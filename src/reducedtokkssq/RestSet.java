/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reducedtokkssq;

import java.util.ArrayList;

/**
 *
 * @author vacuum
 */
public class RestSet {

    //    private ArrayList<Integer> memberList;
//    private ArrayList<Float> distanceList;
    private ArrayList<Boolean> visited;
    private ArrayList<Integer> memberIdList;
    //    private int cnt;
//    private double totalDistance = 0;
//    private double totalDistanceUpTon = 0;
    public int maxMember = 0;

    public RestSet() {
//        cnt = 0;
        maxMember = 0;
//        memberList = new ArrayList<>();
        memberIdList = new ArrayList<>();
//        distanceList = new ArrayList<>();
        visited = new ArrayList<>();
    }

    //    public ArrayList<Float> getDistanceList() {
//        return distanceList;
//    }
    public ArrayList<Boolean> getVisited() {
        return visited;
    }

    public ArrayList<Integer> getMemberList() {
        return memberIdList;
    }

    public RestSet(ArrayList<Integer> memberIdList, ArrayList<Boolean> ivisited, int maxMemberGot) {
//        memberList = new ArrayList<>();
//        distanceList = new ArrayList<>();
        visited = new ArrayList<>();
        this.memberIdList = new ArrayList<>();

        for (Integer m : memberIdList) {
//            this.memberList.add(m);
            this.memberIdList.add(m);
        }
//        for (float f : idistanceList) {
//            this.distanceList.add(f);
//        }

        for (boolean b : ivisited) {
            this.visited.add(b);
        }
        maxMember = maxMemberGot;
//        System.out.println("hm");

//        this.memberList = memberList;
//        this.distanceList = distanceList;
//        this.visited = visited;
//        this.cnt = cnt;
    }

    int getMaxDegree() {
        int max = 0;
        int cnt = memberIdList.size();
        for (int i = 0; i < cnt; i++) {
            Member m = ReducedTokKSSQ.memberSet.getList().get(memberIdList.get(i));
            int c = 0;
//            System.out.print(m.getMemberId() + "  : ");
            for (int friend : m.getFriendList()) {

                if (memberIdList.contains(friend)) {
//                    System.out.print(friend + " ");
                    c++;
                }

            }
//            System.out.println("");
            if (c > max) {
                max = c;
            }
        }
//        System.out.println(max);
        return max;
    }

    int getMinDegree() {
        int min = 1000;
        int cnt = memberIdList.size();
        for (int i = 0; i < cnt; i++) {
            Member m = ReducedTokKSSQ.memberSet.getList().get(memberIdList.get(i));
            int c = 0;
//            System.out.print(m.getMemberId() + "  : ");
            for (int friend : m.getFriendList()) {

                if (memberIdList.contains(friend)) {
//                    System.out.print(friend + " ");
                    c++;
                }

            }
//            System.out.println("");
            if (c < min) {
                min = c;
            }
        }
//        System.out.println(max);
        return min;
    }

    void addMember(Integer memberId) {
//        cnt++;
        maxMember++;
//        memberList.add(member);
//        distanceList.add(distance);
        visited.add(Boolean.FALSE);
//        totalDistance += distance;
        memberIdList.add(memberId);
//        if (cnt <= ReducedTokKSSQ.minimumMember) {
//            totalDistanceUpTon += distance;
//        }
    }

    void popMember(int index) {
        // System.out.println("#########################################");
        // showMemberList();
//        cnt--;

//        System.out.println("index:  " + index);
//        totalDistance -= distanceList.get(index);
//        Member m = memberList.get(index);
        memberIdList.remove(index);
//        Float f = distanceList.get(index);
//        distanceList.remove((Float) f);
        visited.remove(index);
//        memberList.remove(m);

        //System.out.println("after delete ");
        // showMemberList();
    }

    //    public double getTotalDistance() {
//        return totalDistance;
//    }
//    public ArrayList<Member> getMemberList() {
//        return memberList;
//    }
//    double getMemberDistance() {
//        return distanceList.get(0);
//    }
    boolean isEmpty() {
        if (memberIdList.size() == 0) {
            return true;
        }
        return false;
    }

    int getSize() {
        //for(int i)
        return memberIdList.size();
    }

    void showMemberList() {
//        for (int i = 0; i < cnt; i++) {
//            if (!visited.get(i)) {
//                System.out.print(memberList.get(i).getMemberId() + "  ");
//            }
//        }
//        System.out.println("");
        System.out.print("");
        int cnt = memberIdList.size();
        for (int i = 0; i < cnt; i++) {

            System.out.print((char)(memberIdList.get(i)+'a')+"  ");

        }
        System.out.print("\n");
    }

    void markAllUnVisited() {
//        System.out.println(memberIdList.size());
        int cnt = memberIdList.size();

        for (int i = 0; i < cnt; i++) {
            visited.set(i, Boolean.FALSE);
        }
    }

    void markVisited(int index) {
        visited.set(index, Boolean.TRUE);
    }

    void markUnVisited(int index) {
        visited.set(index, Boolean.FALSE);
    }

    boolean foundUnvisitedVertex() {
        for (boolean b : visited) {
            if (b == false) {
                return true;
            }
        }
        return false;
    }

    Integer topMember() {
        int c = 0;
        for (boolean b : visited) {
            if (b == false) {
                return memberIdList.get(c);
            }
            c++;
        }
        System.err.println("ekhane asbe na, ");
        return memberIdList.get(0);

    }

    int getLocalMemberIndex(Integer mId) {
        int index = memberIdList.indexOf(mId);
        return index;
    }

    public Integer getMemberId(int index) {
        return memberIdList.get(index);
    }

    int getUnvisitedMemberCount() {
        int c = 0;
//        System.out.println("visited size " + visited.size());
        for (boolean b : visited) {
            if (b == false) {
                c++;
            }
        }
        return c;
    }

    //    public double getTotalDistanceUpTon() {
//        return totalDistanceUpTon;
//    }
    void pruneUnqualifiedMembers() {
        boolean adjust = false;
        ArrayList<Integer> deletedIndex = new ArrayList<>();
        int cnt = memberIdList.size();

        for (int i = 0; i < cnt; i++) {
            Member m = ReducedTokKSSQ.memberSet.getList().get(memberIdList.get(i));
            int c = 0;
//            System.out.print(m.getMemberId()+"  : ");
            for (int friend : m.getFriendList()) {

                if (memberIdList.contains(friend)) {
//                    System.out.print(friend+" ");
                    c++;
                }

            }
            if (c < ReducedTokKSSQ.minimumConstraint) {
                adjust = true;
                deletedIndex.add(i);
            }
        }
        int d = 0;
        for (Integer i : deletedIndex) {
            //    System.out.print(i + "   ");
            popMember(i - d);
            d++;
        }
//        System.out.println("\n\n");
//        getMaxDegree();
        if (adjust) {
            pruneUnqualifiedMembers();
        }

    }

}