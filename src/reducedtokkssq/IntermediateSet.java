/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reducedtokkssq;

import java.util.ArrayList;

/**
 *
 * @author vacuum
 */
public class IntermediateSet {

    //    private ArrayList<Member> memberList;
//    private ArrayList<Float> distanceList;
    private float totalDistance;

    private ArrayList<Integer> memberIdList;
    //    private int groupSize;
    private int totalConnectivity;
    private ArrayList<Integer> maxFriend;

    public IntermediateSet() {
//        groupSize = 0;
        totalDistance = 0;
        totalConnectivity = 0;
        memberIdList = new ArrayList<>();
//        distanceList = new ArrayList<>();
        maxFriend = new ArrayList<>();
    }

    public int getGroupSize() {
        return memberIdList.size();
    }

    public ArrayList<Integer> getMaxFriend() {
        return maxFriend;
    }

    public IntermediateSet(ArrayList<Integer> imemberIdList, ArrayList<Integer> imaxFriend, float totalDistance, int totalConnectivity) {
        memberIdList = new ArrayList<>();
//        distanceList = new ArrayList<>();
        maxFriend = new ArrayList<>();

        for (Integer m : imemberIdList) {
            this.memberIdList.add(m);
        }
//        for (float f : idistanceList) {
//            this.distanceList.add(f);
//        }

        for (Integer i : imaxFriend) {
            this.maxFriend.add(i);
        }

        this.totalDistance = totalDistance;
//        this.groupSize = groupSize;
        this.totalConnectivity = totalConnectivity;
    }

    public int getTotalConnectivity() {
        return totalConnectivity;
    }

    void popMember(Integer memberId, float distance) {
//        memberList.remove(member);
//        distanceList.remove(distance);
        memberIdList.remove(memberIdList.indexOf(memberId));
//        groupSize--;
        maxFriend.remove(memberIdList.size());
        totalDistance -= distance;
        int cnt = memberIdList.size();
        for (int i = 0; i < cnt; i++) {
            Member m = ReducedTokKSSQ.memberSet.getList().get(memberIdList.get(i));
            Member member = ReducedTokKSSQ.memberSet.getList().get(memberId);
            if (member.getFriendList().contains(m.getMemberId())) {
                totalConnectivity -= 2;
                maxFriend.set(i, maxFriend.get(i) - 1);
                //  System.out.println("kk");
            }
        }
//        for (Member m : memberList) {
//            //  System.out.println(m.getMemberId());
//            if (member.getFriendList().contains(m.getMemberId())) {
//                totalConnectivity -= 2;
//                //  System.out.println("kk");
//            }
//        }
//
//        for (int i = 0; i < cnt; i++) {
//            Member m = ReducedTokKSSQ.memberSet.getList().get(memberIdList.get(i));
//            if (member.getFriendList().contains(m.getMemberId())) {
//                maxFriend.set(i, maxFriend.get(i) - 1);
//            }
//        }

        //  System.out.println("total connection: "+totalConnectivity);
    }

    void addMember(Integer memberId, float distance) {
        //System.out.println("adding in vI");
        // System.out.println("total connection: "+totalConnectivity);
        int con = 0;
        int cnt = memberIdList.size();
        for (int i = 0; i < cnt; i++) {
            Member m = ReducedTokKSSQ.memberSet.getList().get(memberIdList.get(i));
            Member member = ReducedTokKSSQ.memberSet.getList().get(memberId);

            if (member.getFriendList().contains(m.getMemberId())) {
                con++;
                maxFriend.set(i, maxFriend.get(i) + 1);
            }
        }
//        groupSize++;

        totalConnectivity += 2 * con;

//        for (Member m : memberList) {
//            //  System.out.println(m.getMemberId());
//            if (member.getFriendList().contains(m.getMemberId())) {
//                totalConnectivity += 2;
//                //  System.out.println("kk");
//            }
//        }
//        //  System.out.println("total connection: "+totalConnectivity);
        maxFriend.add(con);
        memberIdList.add(memberId);
//        distanceList.add(distance);
        totalDistance += distance;
    }

    int getSize() {
        return memberIdList.size();
    }

    float getTotalDistance() {
        return totalDistance;
    }

    //problematic ,,,,,,, 
    float nextPossibleConnection(Integer memberId) {
        int cnt = memberIdList.size();
        if (cnt == 0) {
            return 0;
        }
        int con = 0;
        // System.out.println("existing member number: " + getSize());
        for (int i = 0; i < cnt; i++) {
            Member m = ReducedTokKSSQ.memberSet.getList().get(memberIdList.get(i));
            Member member = ReducedTokKSSQ.memberSet.getList().get(memberIdList.get(memberId));
            if (member.getFriendList().contains(m.getMemberId())) {
                con += 2;
            }

        }
//        for (Member m : memberList) {
//            if (member.getFriendList().contains(m.getMemberId())) {
//                con += 2;
//            }
//        }
        con += totalConnectivity;

        return (float) con / cnt;
    }

    void showMemberList() {
//        System.out.print("intermediate set member list: ");
        System.out.print("");
        int cnt = memberIdList.size();

        for (int i = 0; i < cnt; i++) {
            System.out.print((char)(memberIdList.get(i)+'a')+"  ");
        }
        System.out.print("\n");

    }

    boolean isPresent(Integer memberId){
        int cnt = memberIdList.size();
//        System.out.println(memberId);
        for (int i = 0; i < cnt; i++) {
//            System.out.print((char)(memberIdList.get(i)+'a')+"  ");
//            System.out.println(memberIdList.get(i));
            if(memberIdList.get(i)==memberId){
                return true;

            }
        }
        return false;
    }

    int connectionIfAdded(Integer memberId) {
//        int con = 0;
        int allCon = 0;
        int cnt = memberIdList.size();

        // System.out.println("existing member number: " + getSize());
        for (int i = 0; i < cnt; i++) {
            Member m = ReducedTokKSSQ.memberSet.getList().get(memberIdList.get(i));
            Member member = ReducedTokKSSQ.memberSet.getList().get(memberId);

            if (member.getFriendList().contains(m.getMemberId())) {
                //if ( member.getFriendList().contains(m.getMemberId())) {
                allCon++;
//                if (maxFriend.get(i) < SSTK.minimumConstraint) {
//                    con++;
//
//                }
            }
        }

//        System.out.println( allCon);
        return allCon;
    }

    public ArrayList<Integer> getMemberList() {
        return memberIdList;
    }

//    public ArrayList<Float> getDistanceList() {
//        return distanceList;
//    }
}