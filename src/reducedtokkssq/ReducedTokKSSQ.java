/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reducedtokkssq;

import gnu.trove.procedure.TIntProcedure;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PriorityQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.jsi.Point;
import net.sf.jsi.Rectangle;
import net.sf.jsi.SpatialIndex;
import net.sf.jsi.rtree.RTree;

import java.util.Random;

/**
 * @author vacuum
 */
public class ReducedTokKSSQ {

    /**
     * @param args the command line arguments
     */
    static ArrayList<ResultGroup> resultLists = new ArrayList<ResultGroup>();
    static int groupFound = 0;
    private static long totalCalled = 0;

    static int timeOut = 2000000;
    static int minimumConstraint;
    static int minimumMember;
    static float maxDistance;
    static int totalMeetingPOint;
    static int c = 0;
    static int rand[];
    static int maxGroupSize;

    static int topK;
    static int queueMem = 11;
    static MemberSet memberSet;
    static MeetingPointSet mPointSet;
    static int presentSampleIndex = -1;

    static int totalTimeBaseline[];
    static long totalitBaseline[];
    static long totalItGreedy[];
    static int totalTimeGreedy[];
    static int retrievedMember[];
    static int retrievedMemberApprox[];
    static int retrievedMemberGreedy[];
    static int retrievedMemberBaseline[];
    static MeetingPointSet sampleMeetingPointSet;
    static int totalTimeBaseline_extended[];
    static long totalitBaseline_extended[];
    static int retrievedMemberBaseline_extended[];

    static int totalTime[];
    static long totalIt[];
    static long totalItApprox[];
    static int totalTimeApprox[];
    boolean verbose = true;
    static int dataset = 3;

    public static void setDefaultParameter() {


        if (!false) {
            totalRetrievedMember = 0;
            minimumConstraint = 3;
            minimumMember = 6;
            maxDistance = (float) .4;
            totalMeetingPOint = 100;
            maxGroupSize = 8;
            topK = 8;

            Score.alpha = .33;
            Score.beta = .33;
            Score.gamma = .33;

        } else {
            totalRetrievedMember = 0;
            minimumConstraint = 1;
            minimumMember = 3;
            maxDistance = (float) 10;
            totalMeetingPOint = 1;
            maxGroupSize = 4;
            topK = 4;

            Score.alpha = .33;
            Score.beta = .33;
            Score.gamma = .33;

        }

    }

    static int sampleNum = 1000;      // total number of samples we are checking
    static boolean mpCOllect =false;  // when "true" random meeting points are generated, when "false" executing is done
    // based on previous selected random meeting poits which are stored in a file,
    // when mpCollect= true, assign totalMeetingPoint ~ 2000,4000 for generating more groups

    public static void main(String[] args) {


        setDefaultParameter();//selecting default parameters


// samplelist contains various "parameter values"

        ArrayList<Float> samplelist = new ArrayList<>();

// supportlist contains different "types of parameter"
// supportlist: 1-> change minimum group size, similarly 2-> minimum acquaintance constraint
// 3-> max distance dm
//4-> groups to be ranked, i.e., top 32 group, top 16 group
//5, k, number of meeting points
// 6,7,8, for changing alpha, beta, gamma,
//plz check if it is correct
        ArrayList<Integer> supportList = new ArrayList<>();

        // minimum member
//        todo change this for actual experiment
        samplelist.add((float) 6);
        supportList.add(1);

        if (!mpCOllect) {
//            todo comment everything


            samplelist.add((float) 4);
            supportList.add(1);
            samplelist.add((float) 6);
            supportList.add(1);
            samplelist.add((float) 7);
            supportList.add(1);

//        }
//            MIN CONTRRAINT

            samplelist.add((float) 2);
            supportList.add(2);
            samplelist.add((float) 4);
            supportList.add(2);
            samplelist.add((float) 5);
            supportList.add(2);


//            distance
            samplelist.add((float) .35);
            supportList.add(3);
            samplelist.add((float) .45);
            supportList.add(3);

            samplelist.add((float) .50);
            supportList.add(3);
//
////            top k
            samplelist.add((float) 4);
            supportList.add(4);
            samplelist.add((float) 16);
            supportList.add(4);
            samplelist.add((float) 32);
            supportList.add(4);
//
////        mp
            samplelist.add((float) 50);
            supportList.add(5);
//        samplelist.add((float)16);
//        supportList.add(5);
            samplelist.add((float) 200);
            supportList.add(5);
            samplelist.add((float) 400);
            supportList.add(5);
//
////        alpha
            samplelist.add((float) .5);
            supportList.add(6);

            samplelist.add((float) .75);
            supportList.add(9);

            samplelist.add((float) 1);
            supportList.add(12);
//
////      beta 19
            samplelist.add((float) .5);
            supportList.add(7);

            samplelist.add((float) .75);
            supportList.add(10);

            samplelist.add((float) 1);
            supportList.add(13);

//
////      gamma
            samplelist.add((float) .5);
            supportList.add(8);

            samplelist.add((float) .75);
            supportList.add(11);

            samplelist.add((float) 1);
            supportList.add(14);
//
//max group size
            samplelist.add((float) 6);
            supportList.add(15);

            samplelist.add((float) 7);
            supportList.add(15);
            samplelist.add((float) 9);
            supportList.add(15);

        }
        MemberSet memberSet1, memberSet2, memberSet3;
        MeetingPointSet mPointSet, mPointSet1, mPointSet2, mPointSet3;
        memberSet = new MemberSet();
        mPointSet = new MeetingPointSet();
        sampleMeetingPointSet = new MeetingPointSet();

        insertMember(memberSet);
        insertGraphConnection(memberSet);
//        computeNoOfTriangles(edges, memberSet);

        if (!mpCOllect) {
            insertMeetingPoints(mPointSet);

        }
        int feasibleRun[][][] = new int[4][samplelist.size()][4];
        double avgContains50[][] = new double[4][samplelist.size()];
        double avgContains75[][] = new double[4][samplelist.size()];
        double avgContains100[][] = new double[4][samplelist.size()];
        double avgContains200[][] = new double[4][samplelist.size()];

        double avgCorrect[][] = new double[4][samplelist.size()];
        double recall[][] = new double[4][samplelist.size()];
        int correctMin[][] = new int[4][samplelist.size()];
        int correctMax[][] = new int[4][samplelist.size()];
        int otherRun[][] = new int[4][samplelist.size()];
        int recallCount[][] = new int[4][samplelist.size()];
        int memberAppearanceCount[][] = new int[4][samplelist.size()];
        double memberAppearance[][] = new double[4][samplelist.size()];
        totalTime = new int[samplelist.size()];
        totalIt = new long[samplelist.size()];
        totalItApprox = new long[samplelist.size()];
        totalTimeApprox = new int[samplelist.size()];

        totalTimeBaseline = new int[samplelist.size()];
        totalitBaseline = new long[samplelist.size()];
        totalItGreedy = new long[samplelist.size()];
        totalTimeGreedy = new int[samplelist.size()];
        retrievedMember = new int[samplelist.size()];
        retrievedMemberApprox = new int[samplelist.size()];
        retrievedMemberGreedy = new int[samplelist.size()];
        retrievedMemberBaseline = new int[samplelist.size()];
        retrievedMemberBaseline_extended = new int[samplelist.size()];

        totalTimeBaseline_extended = new int[samplelist.size()];
        totalitBaseline_extended = new long[samplelist.size()];

        for (int i = 0; i < samplelist.size(); i++) {
            for (int z = 0; z < 4; z++) {
                for (int zz = 0; zz < 4; zz++) {
                    feasibleRun[z][i][zz] = 0;

                }
                memberAppearanceCount[z][i] = 0;
                memberAppearance[z][i] = 0;
                avgContains50[z][i] = 0;
                avgContains75[z][i] = 0;
                avgContains100[z][i] = 0;
                avgContains200[z][i] = 0;
                avgCorrect[z][i] = 0;
                correctMin[z][i] = 10000;
                correctMax[z][i] = -10000;
                otherRun[z][i] = 0;
                recallCount[z][i] = 0;
                recall[z][i] = 0;
            }

            totalTime[i] = 0;
            totalIt[i] = 0;
            totalItApprox[i] = 0;
            totalTimeApprox[i] = 0;

            totalTimeBaseline[i] = 0;
            totalitBaseline[i] = 0;
            totalItGreedy[i] = 0;
            totalTimeGreedy[i] = 0;
            retrievedMember[i] = 0;
            retrievedMemberApprox[i] = 0;
            retrievedMemberGreedy[i] = 0;
            retrievedMemberBaseline[i] = 0;

        }

        for (int j = 1; j <= sampleNum; j++) {


            System.out.println("####################################################################################");
            System.out.println(j + "  ");

            Random rr = new Random();
            if (mpCOllect) {
                float randFactor = rr.nextFloat();
                int meetingDivisionFactor = totalMeetingPOint * 4 + rr.nextInt(totalMeetingPOint * 6);
                System.out.println("randFactor " + randFactor);
                System.out.println("meeting point factor: " + meetingDivisionFactor);
                insertRandomMeetingPoints(mPointSet, meetingDivisionFactor, randFactor);

            }

            int totalMeet = mPointSet.getMeetingPointSetSize();
            rand = new int[totalMeet];

            for (int x = 0; x < totalMeet; x++) {
                rand[x] = x;
            }

            for (int x = 0; x < totalMeet; x++) {
                int val = rr.nextInt(totalMeet);
                int temp = rand[x];
                rand[x] = rand[val];
                rand[val] = temp;
            }
            for (int i = 0; i < samplelist.size(); i++) {


                if (i == 1 || i == 2 || i == 3 || i == 10 || i == 11 || i == 12 || i == 16 || i == 17 || i == 18 || i == 19 || i == 20 || i == 21 || i == 22 || i == 23 || i == 24) {
                    continue;
                }
                setDefaultParameter();

                System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                System.out.println(i + "  ");

                presentSampleIndex = i;


                // this function sets current parameters
                changeParameter(samplelist.get(i), supportList.get(i));

//                maxGroupSize = minimumMember;


                ArrayList<Double> topKVal = new ArrayList<>();
                resultLists = new ArrayList<>();
                groupFound = 0;
//                System.out.println(minimumMember);
//                System.out.println(maxGroupSize);
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

//                todo calling exact, comb=true, baseline, comb=false
                boolean complete = true;

                if (!mpCOllect) {
                    complete = makeResult(memberSet, mPointSet, false, mpCOllect);

                }
                ArrayList<ResultGroup> tempListGroup = new ArrayList<>();

                if (!complete) {
                    continue;
                }

                ArrayList<Integer> foundMember = new ArrayList<>();
                int cc = 0;
                for (ResultGroup rrr : resultLists) {
                    cc++;
                    topKVal.add(rrr.score);
                    tempListGroup.add(rrr);
                    if (cc <= topK) {
                        for (int fMember : rrr.getMemberIdList()) {
                            if (!foundMember.contains(fMember)) {
                                foundMember.add(fMember);
                            }
                        }

                    }
                }
                int topExact = groupFound;


                resultLists.clear();


                // Now we are calling approximate approach with similar parameter set
                resultLists = new ArrayList<>();
                groupFound = 0;

//                System.out.println(minimumMember);
//                System.out.println(maxGroupSize);
                int scorTh = 0, scorOur = 0;

                if (!mpCOllect && complete) {

//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//                    System.out.println("ekhane");
                    makeResultApprox(memberSet, mPointSet, true);

                }
                if (topExact == 0 || groupFound == 0) {
//                    continue;
//                    System.out.println("ans nai");
                } else {

                    int acc = 0;
//                    System.out.println(topKVal.get(0));
//                    System.out.println(resultLists.get(0).score);
                    if (topExact > 0 && groupFound > 0) {
                        for (int xx = 0; xx < topExact; xx++) {
                            if (topKVal.get(xx) <= resultLists.get(groupFound - 1).score) {
                                if (groupFound > xx + 1) {
                                    xx = groupFound - 1;
                                }
                                recall[0][i] += (double) (groupFound) / (xx + 1.0);
                                recallCount[0][i]++;
//                                System.out.println(recall[0][i]);
//                                System.out.println(xx);
//                                System.out.println(topKVal.get(xx));
//                                System.out.println(resultLists.get(groupFound - 1).score);

                                break;
                            }
                        }

                    } else {
                        recall[0][i] += (double) (groupFound) / (1.0);
                        recallCount[0][i]++;
                    }

//                    ArrayList<Integer> foundMember = new ArrayList<>();

                    int foundMemberCount = 0;
                    ArrayList<Integer> currentFoundMemberList = new ArrayList<>();


                    for (ResultGroup rrr : resultLists) {

                        for (int fMember : rrr.getMemberIdList()) {
                            if (!currentFoundMemberList.contains(fMember)) {
                                currentFoundMemberList.add(fMember);
                            }
                        }
                    }
                    for (Integer fMember : currentFoundMemberList) {
                        if (foundMember.contains(fMember)) {
                            foundMemberCount++;

                        }
                    }
                    memberAppearanceCount[0][i]++;
                    memberAppearance[0][i] += (double) foundMemberCount / (foundMember.size() + 0.00);
                    System.out.println("hm");
                    System.out.println(foundMemberCount);
                    System.out.println(foundMember.size());
                    System.out.println(memberAppearance[0][i]);


                    if (topExact >= topK) {

                        int xx = groupFound;


                        double difRes = 0;
                        int contains50 = 0;
                        int contains75 = 0;
                        int contains100 = 0;
                        int contains200 = 0;
                        int contains = 0;
                        for (int x = 0; x < xx; x++) {
                            difRes += resultLists.get(x).score - topKVal.get(x);
                            if (resultLists.get(x).getGroupSize() == tempListGroup.get(x).getGroupSize()) {
                                if (Math.abs(resultLists.get(x).score - topKVal.get(x)) < .00000001) {
                                    acc++;
                                } else {
                                    int cntt = 0;
                                    for (Integer memInteger : resultLists.get(x).getMemberIdList()) {
//                                    System.out.println(memInteger + "  " + tempListGroup.get(x).getMemberIdList().get(cntt));
                                        if (memInteger.equals(tempListGroup.get(x).getMemberIdList().get(cntt))) {
                                            cntt++;
                                        } else {
                                            break;
                                        }

                                    }
                                    if (cntt == resultLists.get(x).getGroupSize()) {
                                        acc++;
                                    } else {
//                                    System.out.println(x);
                                    }

                                }
                            }

                            if (topExact >= groupFound && topKVal.get(groupFound - 1) <= resultLists.get(x).score) {
                                contains50++;

                            }
                            if (topExact >= (groupFound * 3) / 2 && topKVal.get((groupFound * 3) / 2 - 1) <= resultLists.get(x).score) {
                                contains75++;
//                                System.out.println("f");
                            }

                            if (topExact >= groupFound * 2 && topKVal.get(groupFound * 2 - 1) <= resultLists.get(x).score) {
                                contains100++;
//                                System.out.println("g");
                            }
                            if (topExact >= groupFound * 4 && topKVal.get(groupFound * 4 - 1) <= resultLists.get(x).score) {
                                contains200++;
//                                System.out.println("g");
                            }

                        }
                        if (acc != xx) {
//                        System.err.println("me le nai");
//                        for (int k = 0; k < totalMeetingPOint; k++) {
//                            System.out.println("mp: " + mPointSet.getList().get(rand[k]).getPosX() + "   " + mPointSet.getList().get(rand[k]).getPosY());
//                        }
                        }

                        if (acc > correctMax[0][i]) {
                            correctMax[0][i] = acc;
                        }
                        if (acc < correctMin[0][i]) {
                            correctMin[0][i] = acc;
                        }

//                        if (contains50 > 0) {
//                            feasibleRun[0][i][0]++;
//
//                        }
//                        if (contains75 > 0) {
//                            feasibleRun[0][i][1]++;
//
//                        }
//                        if (contains100 > 0) {
//                            feasibleRun[0][i][2]++;
//
//                        }
//
//                        if (contains200 > 0) {
//                            feasibleRun[0][i][3]++;
//
//                        }

                        for (int e = 0; e < 4; e++) {
                            feasibleRun[0][i][e]++;
                        }

                        avgCorrect[0][i] += (double) acc / xx;
                        avgContains50[0][i] += (double) contains50 / xx;
                        avgContains75[0][i] += (double) contains75 / xx;
                        avgContains100[0][i] += (double) contains100 / xx;
                        avgContains200[0][i] += (double) contains200 / xx;

                    } else {
                        for (int e = 0; e < 4; e++) {
                            feasibleRun[0][i][e]++;
                        }

//                        int xx = Math.min(groupFound, topExact);
//                        otherRun[0][i]++;
//
//                        double difRes = 0;
//                        int contains = 0;
//                        for (int x = 0; x < xx; x++) {
//                            difRes += resultLists.get(x).score - topKVal.get(x);
//                            if (Math.abs(resultLists.get(x).score - topKVal.get(x)) < .00000001) {
//                                acc++;
//                            }
//                            if (topKVal.get(topExact - 1) <= resultLists.get(x).score) {
//                                contains++;
//                            }
//
//                        }
//
//                        avgCorrect[0][i] += (double) acc / xx;
//                        avgContains50[0][i] += (double) contains / xx;
                    }

                }

//                System.out.println("score our: " + scorOur);
//                System.out.println("score their:" + scorTh);

                resultLists.clear();
                resultLists = new ArrayList<>();

                groupFound = 0;

                if (!mpCOllect && complete) {
                    makeResultApprox(memberSet, mPointSet, false);
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//                    makeResultApprox(memberSet, mPointSet, false);
                }
                if (topExact == 0 || groupFound == 0) {
//                    continue;
                } else {
                    int acc = 0;

                    if (topExact > 0 && groupFound > 0) {
                        for (int xx = 0; xx < topExact; xx++) {
                            if (topKVal.get(xx) <= resultLists.get(groupFound - 1).score) {
                                if (groupFound > xx + 1) {
                                    xx = groupFound - 1;
                                }
                                recall[2][i] += (double) (groupFound) / (xx + 1.0);
                                recallCount[2][i]++;
//                                System.out.println(recall[2][i]);
//                                System.out.println(xx);
//                                System.out.println(topKVal.get(xx));
//                                System.out.println(resultLists.get(groupFound - 1).score);

                                break;
                            }
                        }

                    } else {
                        recall[2][i] += (double) (groupFound) / (1.0);
                        recallCount[2][i]++;
                    }

                    int foundMemberCount = 0;
                    ArrayList<Integer> currentFoundMemberList = new ArrayList<>();


                    for (ResultGroup rrr : resultLists) {

                        for (int fMember : rrr.getMemberIdList()) {
                            if (!currentFoundMemberList.contains(fMember)) {
                                currentFoundMemberList.add(fMember);
                            }
                        }
                    }
                    for (Integer fMember : currentFoundMemberList) {
                        if (foundMember.contains(fMember)) {
                            foundMemberCount++;

                        }
                    }
                    memberAppearanceCount[2][i]++;
                    memberAppearance[2][i] += (double) foundMemberCount / (foundMember.size() + 0.00);
                    System.out.println("hm");
                    System.out.println(foundMemberCount);
                    System.out.println(foundMember.size());
                    System.out.println(memberAppearance[2][i]);

                    if (topExact >= topK && groupFound >= topK) {

                        int xx = topK;


                        double difRes = 0;
                        int contains50 = 0;
                        int contains75 = 0;
                        int contains100 = 0;
                        int contains200 = 0;
                        int contains = 0;
                        for (int x = 0; x < xx; x++) {
                            difRes += resultLists.get(x).score - topKVal.get(x);
                            if (resultLists.get(x).getGroupSize() == tempListGroup.get(x).getGroupSize()) {
                                if (Math.abs(resultLists.get(x).score - topKVal.get(x)) < .00000001) {
                                    acc++;
                                } else {
                                    int cntt = 0;
                                    for (Integer memInteger : resultLists.get(x).getMemberIdList()) {
//                                    System.out.println(memInteger + "  " + tempListGroup.get(x).getMemberIdList().get(cntt));
                                        if (memInteger.equals(tempListGroup.get(x).getMemberIdList().get(cntt))) {
                                            cntt++;
                                        } else {
                                            break;
                                        }

                                    }
                                    if (cntt == resultLists.get(x).getGroupSize()) {
                                        acc++;
                                    } else {
//                                    System.out.println(x);
                                    }

                                }
                            }

                            if (topExact >= topK && topKVal.get(topK - 1) <= resultLists.get(x).score) {
                                contains50++;

                            }
                            if (topExact >= (topK * 3) / 2 && topKVal.get((topK * 3) / 2 - 1) <= resultLists.get(x).score) {
                                contains75++;
//                                System.out.println("f");
                            }

                            if (topExact >= topK * 2 && topKVal.get(topK * 2 - 1) <= resultLists.get(x).score) {
                                contains100++;
//                                System.out.println("g");
                            }
                            if (topExact >= topK * 4 && topKVal.get(topK * 4 - 1) <= resultLists.get(x).score) {
                                contains200++;
//                                System.out.println("g");
                            }

                        }
                        if (acc != xx) {
//                        System.err.println("me le nai");
//                        for (int k = 0; k < totalMeetingPOint; k++) {
//                            System.out.println("mp: " + mPointSet.getList().get(rand[k]).getPosX() + "   " + mPointSet.getList().get(rand[k]).getPosY());
//                        }
                        }

                        if (acc > correctMax[0][i]) {
                            correctMax[2][i] = acc;
                        }
                        if (acc < correctMin[0][i]) {
                            correctMin[2][i] = acc;
                        }

//                        if (contains50 > 0) {
//                            feasibleRun[2][i][0]++;
//
//                        }
//                        if (contains75 > 0) {
//                            feasibleRun[2][i][1]++;
//
//                        }
//                        if (contains100 > 0) {
//                            feasibleRun[2][i][2]++;
//
//                        }
//
//                        if (contains200 > 0) {
//                            feasibleRun[2][i][3]++;
//
//                        }

                        for (int e = 0; e < 4; e++) {
                            feasibleRun[2][i][e]++;
                        }

                        avgCorrect[2][i] += (double) acc / xx;
                        avgContains50[2][i] += (double) contains50 / xx;
                        avgContains75[2][i] += (double) contains75 / xx;
                        avgContains100[2][i] += (double) contains100 / xx;
                        avgContains200[2][i] += (double) contains200 / xx;

                    } else {

                        for (int e = 0; e < 4; e++) {
                            feasibleRun[2][i][e]++;
                        }


//                        int xx = Math.min(groupFound, topExact);
//                        otherRun[2][i]++;
//
//                        double difRes = 0;
//                        int contains = 0;
//                        for (int x = 0; x < xx; x++) {
//                            difRes += resultLists.get(x).score - topKVal.get(x);
//                            if (Math.abs(resultLists.get(x).score - topKVal.get(x)) < .00000001) {
//                                acc++;
//                            }
//                            if (topKVal.get(topExact - 1) <= resultLists.get(x).score) {
//                                contains++;
//                            }
//
//                        }
//
//                        avgCorrect[2][i] += (double) acc / xx;
//                        avgContains50[2][i] += (double) contains / xx;
                    }

                    resultLists.clear();
                    resultLists = new ArrayList<>();
                    groupFound = 0;

                }

                resultLists.clear();
                resultLists = new ArrayList<>();
                groupFound = 0;

//                if (!mpCOllect) {
//                    makeResult(memberSet, mPointSet, false, mpCOllect);
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

//                    todo calling exact, comb=true
                makeResult(memberSet, mPointSet, true, mpCOllect);

                if (topExact == 0 || groupFound == 0) {

                    recall[1][i] += (double) (groupFound) / (1.0);
                    recallCount[1][i]++;

                } else {
                    int acc = 0;
                    if (topExact > 0 && groupFound > 0 && groupFound >= topK) {
                        int tempVal = groupFound;
                        groupFound = topK;
                        for (int xx = 0; xx < topExact; xx++) {
                            if (topKVal.get(xx) <= resultLists.get(groupFound - 1).score) {
                                if (groupFound > xx + 1) {
                                    xx = groupFound - 1;
                                }
                                recall[1][i] += (double) (groupFound) / (xx + 1.0);
                                recallCount[1][i]++;
//                                System.out.println(recall[1][i]);
//                                System.out.println(xx);
//                                System.out.println(topKVal.get(xx));
//                                System.out.println(resultLists.get(groupFound - 1).score);
                                break;
                            }
                        }
                        groupFound = tempVal;

                    }
                    int foundMemberCount = 0;
                    ArrayList<Integer> currentFoundMemberList = new ArrayList<>();


                    for (ResultGroup rrr : resultLists) {

                        for (int fMember : rrr.getMemberIdList()) {
                            if (!currentFoundMemberList.contains(fMember)) {
                                currentFoundMemberList.add(fMember);
                            }
                        }
                    }
                    for (Integer fMember : currentFoundMemberList) {
                        if (foundMember.contains(fMember)) {
                            foundMemberCount++;

                        }
                    }
                    memberAppearanceCount[1][i]++;
                    memberAppearance[1][i] += (double) foundMemberCount / (foundMember.size() + 0.00);
                    System.out.println("hm");
                    System.out.println(foundMemberCount);
                    System.out.println(foundMember.size());
                    System.out.println(memberAppearance[1][i]);


                    if (topExact >= topK && groupFound >= topK) {

                        int xx = topK;


                        double difRes = 0;
                        int contains50 = 0;
                        int contains75 = 0;
                        int contains100 = 0;
                        int contains200 = 0;
                        int contains = 0;
                        for (int x = 0; x < xx; x++) {
                            difRes += resultLists.get(x).score - topKVal.get(x);
                            if (resultLists.get(x).getGroupSize() == tempListGroup.get(x).getGroupSize()) {
                                if (Math.abs(resultLists.get(x).score - topKVal.get(x)) < .00000001) {
                                    acc++;
                                } else {
                                    int cntt = 0;
                                    for (Integer memInteger : resultLists.get(x).getMemberIdList()) {
//                                    System.out.println(memInteger + "  " + tempListGroup.get(x).getMemberIdList().get(cntt));
                                        if (memInteger.equals(tempListGroup.get(x).getMemberIdList().get(cntt))) {
                                            cntt++;
                                        } else {
                                            break;
                                        }

                                    }
                                    if (cntt == resultLists.get(x).getGroupSize()) {
                                        acc++;
                                    } else {
//                                    System.out.println(x);
                                    }

                                }
                            }

                            if (topExact >= topK && topKVal.get(topK - 1) <= resultLists.get(x).score) {
                                contains50++;

                            }
                            if (topExact >= (topK * 3) / 2 && topKVal.get((topK * 3) / 2 - 1) <= resultLists.get(x).score) {
                                contains75++;
//                                System.out.println("f");
                            }

                            if (topExact >= topK * 2 && topKVal.get(topK * 2 - 1) <= resultLists.get(x).score) {
                                contains100++;
//                                System.out.println("g");
                            }
                            if (topExact >= topK * 4 && topKVal.get(topK * 4 - 1) <= resultLists.get(x).score) {
                                contains200++;
//                                System.out.println("g");
                            }

                        }

                        if (acc != xx) {
//                        System.err.println("me le nai");
//                        for (int k = 0; k < totalMeetingPOint; k++) {
//                            System.out.println("mp: " + mPointSet.getList().get(rand[k]).getPosX() + "   " + mPointSet.getList().get(rand[k]).getPosY());
//                        }
                        }

                        if (acc > correctMax[0][i]) {
                            correctMax[1][i] = acc;
                        }
                        if (acc < correctMin[0][i]) {
                            correctMin[1][i] = acc;
                        }

//                        if (contains50 > 0) {
//                            feasibleRun[1][i][0]++;
//
//                        }
//                        if (contains75 > 0) {
//                            feasibleRun[1][i][1]++;
//
//                        }
//                        if (contains100 > 0) {
//                            feasibleRun[1][i][2]++;
//
//                        }
//
//                        if (contains200 > 0) {
//                            feasibleRun[1][i][3]++;
//
//                        }
                        for (int e = 0; e < 4; e++) {
                            feasibleRun[1][i][e]++;
                        }


                        avgCorrect[1][i] += (double) acc / xx;
                        avgContains50[1][i] += (double) contains50 / xx;
                        avgContains75[1][i] += (double) contains75 / xx;
                        avgContains100[1][i] += (double) contains100 / xx;
                        avgContains200[1][i] += (double) contains200 / xx;

                    } else {
                        for (int e = 0; e < 4; e++) {
                            feasibleRun[1][i][e]++;
                        }

//                        int xx = Math.min(groupFound, topExact);
//                        otherRun[1][i]++;
//
//                        double difRes = 0;
//                        int contains = 0;
//                        for (int x = 0; x < xx; x++) {
//                            difRes += resultLists.get(x).score - topKVal.get(x);
//                            if (Math.abs(resultLists.get(x).score - topKVal.get(x)) < .00000001) {
//                                acc++;
//                            }
//                            if (topKVal.get(topExact - 1) <= resultLists.get(x).score) {
//                                contains++;
//                            }
//
//                        }
//
//                        avgCorrect[1][i] += (double) acc / xx;
//                        avgContains50[1][i] += (double) contains / xx;
                    }


                }

                resultLists.clear();
                resultLists = new ArrayList<>();
                groupFound = 0;

                if (!mpCOllect && minimumConstraint!=2) {
                    makeResult_New_baseline(memberSet, mPointSet, false, mpCOllect);

                }
//                }
                if (topExact == 0 || groupFound == 0) {

                    recall[3][i] += (double) (groupFound) / (1.0);
                    recallCount[3][i]++;

                } else {
                    int acc = 0;
                    if (topExact > 0 && groupFound > 0) {
                        for (int xx = 0; xx < topExact; xx++) {
                            if (topKVal.get(xx) <= resultLists.get(groupFound - 1).score) {
                                if (groupFound > xx + 1) {
                                    xx = groupFound - 1;
                                }
                                recall[3][i] += (double) (groupFound) / (xx + 1.0);
                                recallCount[3][i]++;
//                                System.out.println(recall[1][i]);
//                                System.out.println(xx);
//                                System.out.println(topKVal.get(xx));
//                                System.out.println(resultLists.get(groupFound - 1).score);
                                break;
                            }
                        }

                    }
                    int foundMemberCount = 0;
                    ArrayList<Integer> currentFoundMemberList = new ArrayList<>();


                    for (ResultGroup rrr : resultLists) {

                        for (int fMember : rrr.getMemberIdList()) {
                            if (!currentFoundMemberList.contains(fMember)) {
                                currentFoundMemberList.add(fMember);
                            }
                        }
                    }
                    for (Integer fMember : currentFoundMemberList) {
                        if (foundMember.contains(fMember)) {
                            foundMemberCount++;

                        }
                    }
                    memberAppearanceCount[3][i]++;
                    memberAppearance[3][i] += (double) foundMemberCount / (foundMember.size() + 0.00);
                    System.out.println("hm");
                    System.out.println(foundMemberCount);
                    System.out.println(foundMember.size());
                    System.out.println(memberAppearance[3][i]);


                    if (topExact >= topK && groupFound >= topK) {

                        int xx = topK;


                        double difRes = 0;
                        int contains50 = 0;
                        int contains75 = 0;
                        int contains100 = 0;
                        int contains200 = 0;
                        int contains = 0;
                        for (int x = 0; x < xx; x++) {
                            difRes += resultLists.get(x).score - topKVal.get(x);
                            if (resultLists.get(x).getGroupSize() == tempListGroup.get(x).getGroupSize()) {
                                if (Math.abs(resultLists.get(x).score - topKVal.get(x)) < .00000001) {
                                    acc++;
                                } else {
                                    int cntt = 0;
                                    for (Integer memInteger : resultLists.get(x).getMemberIdList()) {
//                                    System.out.println(memInteger + "  " + tempListGroup.get(x).getMemberIdList().get(cntt));
                                        if (memInteger.equals(tempListGroup.get(x).getMemberIdList().get(cntt))) {
                                            cntt++;
                                        } else {
                                            break;
                                        }

                                    }
                                    if (cntt == resultLists.get(x).getGroupSize()) {
                                        acc++;
                                    } else {
//                                    System.out.println(x);
                                    }

                                }
                            }

                            if (topExact >= topK && topKVal.get(topK - 1) <= resultLists.get(x).score) {
                                contains50++;

                            }
                            if (topExact >= (topK * 3) / 2 && topKVal.get((topK * 3) / 2 - 1) <= resultLists.get(x).score) {
                                contains75++;
//                                System.out.println("f");
                            }

                            if (topExact >= topK * 2 && topKVal.get(topK * 2 - 1) <= resultLists.get(x).score) {
                                contains100++;
//                                System.out.println("g");
                            }
                            if (topExact >= topK * 4 && topKVal.get(topK * 4 - 1) <= resultLists.get(x).score) {
                                contains200++;
//                                System.out.println("g");
                            }

                        }

                        if (acc != xx) {
//                        System.err.println("me le nai");
//                        for (int k = 0; k < totalMeetingPOint; k++) {
//                            System.out.println("mp: " + mPointSet.getList().get(rand[k]).getPosX() + "   " + mPointSet.getList().get(rand[k]).getPosY());
//                        }
                        }

                        if (acc > correctMax[0][i]) {
                            correctMax[3][i] = acc;
                        }
                        if (acc < correctMin[0][i]) {
                            correctMin[3][i] = acc;
                        }

//                        if (contains50 > 0) {
//                            feasibleRun[1][i][0]++;
//
//                        }
//                        if (contains75 > 0) {
//                            feasibleRun[1][i][1]++;
//
//                        }
//                        if (contains100 > 0) {
//                            feasibleRun[1][i][2]++;
//
//                        }
//
//                        if (contains200 > 0) {
//                            feasibleRun[1][i][3]++;
//
//                        }
                        for (int e = 0; e < 4; e++) {
                            feasibleRun[3][i][e]++;
                        }


                        avgCorrect[3][i] += (double) acc / xx;
                        avgContains50[3][i] += (double) contains50 / xx;
                        avgContains75[3][i] += (double) contains75 / xx;
                        avgContains100[3][i] += (double) contains100 / xx;
                        avgContains200[3][i] += (double) contains200 / xx;

                    } else {
                        for (int e = 0; e < 4; e++) {
                            feasibleRun[3][i][e]++;
                        }

//                        int xx = Math.min(groupFound, topExact);
//                        otherRun[1][i]++;
//
//                        double difRes = 0;
//                        int contains = 0;
//                        for (int x = 0; x < xx; x++) {
//                            difRes += resultLists.get(x).score - topKVal.get(x);
//                            if (Math.abs(resultLists.get(x).score - topKVal.get(x)) < .00000001) {
//                                acc++;
//                            }
//                            if (topKVal.get(topExact - 1) <= resultLists.get(x).score) {
//                                contains++;
//                            }
//
//                        }
//
//                        avgCorrect[1][i] += (double) acc / xx;
//                        avgContains50[1][i] += (double) contains / xx;
                    }


                }
            }


            String fileName = "FSSGQ_result.csv";
            BufferedWriter bw = null;
            FileWriter fw = null;

            try {

                File file = new File(fileName);

                // if file doesnt exists, then create it
                if (!file.exists()) {
                    file.createNewFile();
                }
                // true = append file
                fw = new FileWriter(file.getAbsoluteFile());
                bw = new BufferedWriter(fw);

//                for (MeetingPoint meetingPoint : sampleMeetingPointSet.getList()) {
//                    StringBuilder sb = new StringBuilder();
//
//                    sb.append(meetingPoint.getPosX());
//                    sb.append(',');
//                    sb.append(meetingPoint.getPosY());
//
//                    sb.append('\n');
//
//                    bw.write(sb.toString());
//                }

                System.out.println(":::::::::::::::::::::::))))(((((((::::::::::::::::::::::::::::::::::::::::::");
                StringBuilder savedStringBuilder = new StringBuilder();
                bw.write(j + "\n\n");

                for (int i = 0; i < samplelist.size(); i++) {

//                minimumConstraint = 3;
//                minimumMember = 6;
//                maxDistance = (float) 1;
//                totalMeetingPOint = 16;
//                maxGroupSize = 9;
//                topK = 16;
//
                    setDefaultParameter();
                    changeParameter(samplelist.get(i), supportList.get(i));
                    StringBuilder sb = new StringBuilder();
                    if (i == 0) {
                        sb.append("\n\n\n");
                        sb.append("Minimum size," + minimumMember + "\n");
                        sb.append("Maximum size," + maxGroupSize + "\n");
                        sb.append("min Constraint," + minimumConstraint + "\n");
                        sb.append("meeting points," + totalMeetingPOint + "\n");
                        sb.append("max distance," + maxDistance + "\n");
                        sb.append("K," + topK + "\n");
                        sb.append("\n\n\n\n");
                        sb.append(",FFGQ_approx_1,FFGQ_approx_2,FFGQ_exact,FFGQ_greedy,FFGQ_b_extended,");
                        sb.append("FFGQ_approx_1,FFGQ_approx_2,FFGQ_exact,FFGQ_greedy,FFGQ_b_extended,FFGQ_exact & FFGQ_approx_1,,,,,,FFGQ_exact & FFGQ_approx_2,,,,,,FFGQ_exact & FFGQ_greedy,,,,,,FFGQ_exact & FFGQ_b_extended\n");
                        sb.append(",iteration,,,,,time,,,,,precision,1.5k,2k,4k,recall,overlap,precision,1.5k,2k,4k,recall,overlap,precision,1.5k,2k,4k,recall,overlap,precision,1.5k,2k,4k,recall,overlap\n");
                        bw.write(sb.toString());
                    }

                    if (i == 4 || i == 7 || i == 10 || i == 13 || i == 16 || i == 19 || i == 22 || i == 25) {


                        sb.append("\n\n\n\n");
//                        "FFGQ,   FFGQ_approx, FFGQ_baseline,  FFGQ_greedy,FFGQ_b_extended"
                        sb.append(",FFGQ_approx_1,FFGQ_approx_2,FFGQ_exact,FFGQ_greedy,FFGQ_b_extended,");
                        sb.append("FFGQ_approx_1,FFGQ_approx_2,FFGQ_exact,FFGQ_greedy,FFGQ_b_extended,FFGQ_exact & FFGQ_approx_1,,,,,,FFGQ_exact & FFGQ_approx_2,,,,,,FFGQ_exact & FFGQ_greedy,,,,,,FFGQ_exact & FFGQ_b_extended\n");
                        sb.append(",iteration,,,,,time,,,,,precision,1.5k,2k,4k,recall,overlap,precision,1.5k,2k,4k,recall,overlap,precision,1.5k,2k,4k,recall,overlap,precision,1.5k,2k,4k,recall,overlap\n");

//                        sb.append(",iteration,,,,time,,,,precision,recall,,,,,precision,recall\n");
                        bw.write(sb.toString());
                    }

                    if (i == 1 || i == 2 || i == 3 || i == 10 || i == 11 || i == 12 || i == 16 || i == 17 || i == 18 || i == 19 || i == 20 || i == 21 || i == 22 || i == 23 || i == 24) {
                        continue;
                    }

                    sb = new StringBuilder();

                    sb.append(samplelist.get(i) + ",");
                    sb.append(totalIt[i] / j + ",   " + totalItApprox[i] / j + ",  " + totalitBaseline[i] / j + ",   " + totalItGreedy[i] / j + ",    " + totalitBaseline_extended[i] / j + ",    ");
                    sb.append(totalTime[i] / j + ",   " + totalTimeApprox[i] / j + ",   " + totalTimeBaseline[i] / j + ",   " + totalTimeGreedy[i] / j + ",  " + totalTimeBaseline_extended[i] / j + ",   ");

                    sb.append(100.0 * (double) avgContains50[1][i] / (feasibleRun[1][i][0] + otherRun[1][i]) + ",");
                    sb.append(100.0 * (double) avgContains75[1][i] / (feasibleRun[1][i][1] + otherRun[1][i]) + ",");
                    sb.append(100.0 * (double) avgContains100[1][i] / (feasibleRun[1][i][2] + otherRun[1][i]) + ",");
                    sb.append(100.0 * (double) avgContains200[1][i] / (feasibleRun[1][i][3] + otherRun[1][i]) + ",");
                    sb.append(100.0 * (double) recall[1][i] / recallCount[1][i] + ",");
                    sb.append(100.0 * (double) memberAppearance[1][i] / memberAppearanceCount[1][i] + ",");

                    sb.append(100.0 * (double) avgContains50[0][i] / (feasibleRun[0][i][0] + otherRun[0][i]) + ",");
                    sb.append(100.0 * (double) avgContains75[0][i] / (feasibleRun[0][i][1] + otherRun[0][i]) + ",");
                    sb.append(100.0 * (double) avgContains100[0][i] / (feasibleRun[0][i][2] + otherRun[0][i]) + ",");
                    sb.append(100.0 * (double) avgContains200[0][i] / (feasibleRun[0][i][3] + otherRun[0][i]) + ",");
                    sb.append(100.0 * (double) recall[0][i] / recallCount[0][i] + ",");
                    sb.append(100.0 * (double) memberAppearance[0][i] / memberAppearanceCount[0][i] + ",");


                    sb.append(100.0 * (double) avgContains50[2][i] / (feasibleRun[2][i][0] + otherRun[2][i]) + ",");
                    sb.append(100.0 * (double) avgContains75[2][i] / (feasibleRun[2][i][1] + otherRun[2][i]) + ",");
                    sb.append(100.0 * (double) avgContains100[2][i] / (feasibleRun[2][i][2] + otherRun[2][i]) + ",");
                    sb.append(100.0 * (double) avgContains200[2][i] / (feasibleRun[2][i][3] + otherRun[2][i]) + ",");
                    sb.append(100.0 * (double) recall[2][i] / recallCount[2][i] + ",");
                    sb.append(100.0 * (double) memberAppearance[2][i] / memberAppearanceCount[2][i] + ",");

                    sb.append(100.0 * (double) avgContains50[3][i] / (feasibleRun[3][i][0] + otherRun[3][i]) + ",");
                    sb.append(100.0 * (double) avgContains75[3][i] / (feasibleRun[3][i][1] + otherRun[3][i]) + ",");
                    sb.append(100.0 * (double) avgContains100[3][i] / (feasibleRun[3][i][2] + otherRun[3][i]) + ",");
                    sb.append(100.0 * (double) avgContains200[3][i] / (feasibleRun[3][i][3] + otherRun[3][i]) + ",");
                    sb.append(100.0 * (double) recall[3][i] / recallCount[3][i] + ",");
                    sb.append(100.0 * (double) memberAppearance[3][i] / memberAppearanceCount[3][i] + ",");


                    sb.append("\n");

                    if (i == 0) {
                        savedStringBuilder = sb;
                    }
                    if (i == 5 || i == 8 || i == 11 || i == 14 || i == 16 || i == 19 || i == 22 || i == 26) {
                        bw.write(savedStringBuilder.toString());
                    }
                    bw.write(sb.toString());


                    System.out.println("Minimum size   " + minimumMember);
                    System.out.println("Maximum size   " + maxGroupSize);
                    System.out.println("min Constraint " + minimumConstraint);
                    System.out.println("meeting points " + totalMeetingPOint);
                    System.out.println("max distance   " + maxDistance);
                    System.out.println("topK           " + topK);

                    System.out.println("");
                    System.out.println("FFGQ_approx_1,   FFGQ_approx_2, FFGQ_exact,  FFGQ_greedy,FFGQ_b_extended");
                    System.out.println("iteration");

                    System.out.println(totalIt[i] / j + ";   " + totalItApprox[i] / j + ";  " + totalitBaseline[i] / j + ";   " + totalItGreedy[i] / j + ";    " + totalitBaseline_extended[i] / j + ";    ");

                    System.out.println("time");

                    System.out.println(totalTime[i] / j + ";   " + totalTimeApprox[i] / j + ";   " + totalTimeBaseline[i] / j + ";   " + totalTimeGreedy[i] / j + ";   " + totalTimeBaseline_extended[i] / j + ";   ");

//                System.out.println("#####################   finally   " + totalIt[i] / j + "\t" + totalTime[i] / j);
//                System.out.println("#####################   finally   " + totalItApprox[i] / j + "\t" + totalTimeApprox[i] / j + "\t" + "Approx");
//                System.out.println("#####################   finally   " + totalitBaseline[i] / j + "\t" + totalTimeBaseline[i] / j);
//                System.out.println("#####################   finally   " + totalItGreedy[i] / j + "\t" + totalTimeGreedy[i] / j + "\t" + "Approx INcremental");
//                System.out.println("total retrieved");
//
//                System.out.println(retrievedMember[i] / j + "," + retrievedMemberApprox[i] / j + "," + retrievedMemberBaseline[i] / j + "," + retrievedMemberGreedy[i] / j);
//                System.out.println(retrievedMember[i] / j + "\t" + retrievedMemberApprox[i] / j + "\t" + retrievedMemberBaseline[i] / j + "\t" + retrievedMemberGreedy[i] / j);
//
//                System.out.println("total retrieved member                     " + retrievedMember[i] / j);
//                System.out.println("total retrieved member Approx              " + retrievedMemberApprox[i] / j);
//                System.out.println("total retrieved member Incremental         " + retrievedMemberBaseline[i] / j);
//                System.out.println("total retrieved member Approx Incremental  " + retrievedMemberGreedy[i] / j);

//            avgContains50[i] = (double) avgContains50[i] / (feasibleRun[i] + otherRun[i]);
//            avgContains75[i] = (double) avgContains75[i] / feasibleRun[i];
//            avgContains100[i] = (double) avgContains100[i] / feasibleRun[i];
//            avgCorrect[i] = (double) avgCorrect[i] / (feasibleRun[i] + otherRun[i]);


                    System.out.println("Comparison btn FFGQ_exact & FFGQ_approx_2");
//                System.out.println(100.0 * (double) avgCorrect[0][i] / (feasibleRun[0][i] + otherRun[0][i]) + "," + 100.0 * (double) avgContains50[0][i] / (feasibleRun[0][i] + otherRun[0][i]) + "," + 100.0 * (double) avgContains75[0][i] / feasibleRun[0][i] + "," + 100.0 * (double) avgContains100[0][i] / feasibleRun[0][i]);
                    System.out.println("precision    :" + 100.0 * (double) avgContains50[0][i] / (feasibleRun[0][i][0] + otherRun[0][i]));
                    System.out.println("recall       :" + 100.0 * (double) recall[0][i] / recallCount[0][i]);

                    System.out.println("\n\n");
//                System.out.println("avg correct:  " + (double) avgCorrect[i] / (feasibleRun[i] + otherRun[i]));
//
//                // System.out.println("avg contains:   " + avgContains);
//                System.out.println("avg contains50:   " + (double) avgContains50[i] / (feasibleRun[i] + otherRun[i]));
//                System.out.println("avg contains75:   " + (double) avgContains75[i] / feasibleRun[i]);
//                System.out.println("avg contains100:   " + (double) avgContains100[i] / feasibleRun[i]);
//                System.out.println("correct max " + correctMax[0][i]);
//                System.out.println("correct min " + correctMin[0][i]);

//                System.out.println("1");
//                System.out.println(100.0 * (double) avgCorrect[1][i] / (feasibleRun[1][i] + otherRun[1][i]) + "," + 100.0 * (double) avgContains50[1][i] / (feasibleRun[1][i] + otherRun[1][i]) + "," + 100.0 * (double) avgContains75[1][i] / feasibleRun[1][i] + "," + 100.0 * (double) avgContains100[1][i] / feasibleRun[1][i]);
//                System.out.println("recall: " + 100.0 * (double) recall[1][i] / recallCount[1][i]);
////                System.out.println("avg correct:  " + (double) avgCorrect[i] / (feasibleRun[i] + otherRun[i]));
////
////                // System.out.println("avg contains:   " + avgContains);
////                System.out.println("avg contains50:   " + (double) avgContains50[i] / (feasibleRun[i] + otherRun[i]));
////                System.out.println("avg contains75:   " + (double) avgContains75[i] / feasibleRun[i]);
////                System.out.println("avg contains100:   " + (double) avgContains100[i] / feasibleRun[i]);
//                System.out.println("correct max " + correctMax[1][i]);
//                System.out.println("correct min " + correctMin[1][i]);

                    System.out.println("Comparison btn FFGQ_exact & FFGQ_greedy");
//                System.out.println(100.0 * (double) avgCorrect[0][i] / (feasibleRun[0][i] + otherRun[0][i]) + "," + 100.0 * (double) avgContains50[0][i] / (feasibleRun[0][i] + otherRun[0][i]) + "," + 100.0 * (double) avgContains75[0][i] / feasibleRun[0][i] + "," + 100.0 * (double) avgContains100[0][i] / feasibleRun[0][i]);
                    System.out.println("precision    :" + 100.0 * (double) avgContains50[2][i] / (feasibleRun[2][i][0] + otherRun[2][i]));
                    System.out.println("recall       :" + 100.0 * (double) recall[2][i] / recallCount[2][i]);

                    System.out.println("\n\n");
                    System.out.println("Comparison btn FFGQ_exact & FFGQ_approx_1");
//                System.out.println(100.0 * (double) avgCorrect[0][i] / (feasibleRun[0][i] + otherRun[0][i]) + "," + 100.0 * (double) avgContains50[0][i] / (feasibleRun[0][i] + otherRun[0][i]) + "," + 100.0 * (double) avgContains75[0][i] / feasibleRun[0][i] + "," + 100.0 * (double) avgContains100[0][i] / feasibleRun[0][i]);
                    System.out.println("precision    :" + 100.0 * (double) avgContains50[1][i] / (feasibleRun[1][i][0] + otherRun[1][i]));
                    System.out.println("recall       :" + 100.0 * (double) recall[1][i] / recallCount[1][i]);


                    System.out.println("\n\n");
                    System.out.println("Comparison btn FFGQ_exact & FFGQ_extended_baseline");
//                System.out.println(100.0 * (double) avgCorrect[0][i] / (feasibleRun[0][i] + otherRun[0][i]) + "," + 100.0 * (double) avgContains50[0][i] / (feasibleRun[0][i] + otherRun[0][i]) + "," + 100.0 * (double) avgContains75[0][i] / feasibleRun[0][i] + "," + 100.0 * (double) avgContains100[0][i] / feasibleRun[0][i]);
                    System.out.println("precision    :" + 100.0 * (double) avgContains50[3][i] / (feasibleRun[3][i][0] + otherRun[3][i]));
                    System.out.println("recall       :" + 100.0 * (double) recall[3][i] / recallCount[3][i]);

//                System.out.println("2");
//                System.out.println(100.0 * (double) avgCorrect[2][i] / (feasibleRun[2][i] + otherRun[2][i]) + "," + 100.0 * (double) avgContains50[2][i] / (feasibleRun[2][i] + otherRun[2][i]) + "," + 100.0 * (double) avgContains75[2][i] / feasibleRun[2][i] + "," + 100.0 * (double) avgContains100[2][i] / feasibleRun[2][i]);
//                System.out.println("recall: " + 100.0 * (double) recall[2][i] / recallCount[2][i]);
////                System.out.println("avg correct:  " + (double) avgCorrect[i] / (feasibleRun[i] + otherRun[i]));
////
////                // System.out.println("avg contains:   " + avgContains);
////                System.out.println("avg contains50:   " + (double) avgContains50[i] / (feasibleRun[i] + otherRun[i]));
////                System.out.println("avg contains75:   " + (double) avgContains75[i] / feasibleRun[i]);
////                System.out.println("avg contains100:   " + (double) avgContains100[i] / feasibleRun[i]);
//                System.out.println("correct max " + correctMax[2][i]);
//                System.out.println("correct min " + correctMin[0][i]);
                    System.out.println("\n\n\n\n");

//                    System.out.println("Comparison btn FFGQ & FFGQ_approx");
////                System.out.println(100.0 * (double) avgCorrect[0][i] / (feasibleRun[0][i] + otherRun[0][i]) + "," + 100.0 * (double) avgContains50[0][i] / (feasibleRun[0][i] + otherRun[0][i]) + "," + 100.0 * (double) avgContains75[0][i] / feasibleRun[0][i] + "," + 100.0 * (double) avgContains100[0][i] / feasibleRun[0][i]);
//                    System.out.println("precision    :" + 100.0 * (double) avgContains50[0][i] / (feasibleRun[0][i][0] + otherRun[0][i]));
//                    System.out.println("recall       :" + 100.0 * (double) recall[0][i] / recallCount[0][i]);
//
//                    System.out.println("\n\n");
////                System.out.println("avg correct:  " + (double) avgCorrect[i] / (feasibleRun[i] + otherRun[i]));
////
////                // System.out.println("avg contains:   " + avgContains);
////                System.out.println("avg contains50:   " + (double) avgContains50[i] / (feasibleRun[i] + otherRun[i]));
////                System.out.println("avg contains75:   " + (double) avgContains75[i] / feasibleRun[i]);
////                System.out.println("avg contains100:   " + (double) avgContains100[i] / feasibleRun[i]);
////                System.out.println("correct max " + correctMax[0][i]);
////                System.out.println("correct min " + correctMin[0][i]);
//
////                System.out.println("1");
////                System.out.println(100.0 * (double) avgCorrect[1][i] / (feasibleRun[1][i] + otherRun[1][i]) + "," + 100.0 * (double) avgContains50[1][i] / (feasibleRun[1][i] + otherRun[1][i]) + "," + 100.0 * (double) avgContains75[1][i] / feasibleRun[1][i] + "," + 100.0 * (double) avgContains100[1][i] / feasibleRun[1][i]);
////                System.out.println("recall: " + 100.0 * (double) recall[1][i] / recallCount[1][i]);
//////                System.out.println("avg correct:  " + (double) avgCorrect[i] / (feasibleRun[i] + otherRun[i]));
//////
//////                // System.out.println("avg contains:   " + avgContains);
//////                System.out.println("avg contains50:   " + (double) avgContains50[i] / (feasibleRun[i] + otherRun[i]));
//////                System.out.println("avg contains75:   " + (double) avgContains75[i] / feasibleRun[i]);
//////                System.out.println("avg contains100:   " + (double) avgContains100[i] / feasibleRun[i]);
////                System.out.println("correct max " + correctMax[1][i]);
////                System.out.println("correct min " + correctMin[1][i]);
//
//                    System.out.println("Comparison btn FFGQ & FFGQ_greedy");
////                System.out.println(100.0 * (double) avgCorrect[0][i] / (feasibleRun[0][i] + otherRun[0][i]) + "," + 100.0 * (double) avgContains50[0][i] / (feasibleRun[0][i] + otherRun[0][i]) + "," + 100.0 * (double) avgContains75[0][i] / feasibleRun[0][i] + "," + 100.0 * (double) avgContains100[0][i] / feasibleRun[0][i]);
//                    System.out.println("precision    :" + 100.0 * (double) avgContains50[2][i] / (feasibleRun[2][i][0] + otherRun[2][i]));
//                    System.out.println("recall       :" + 100.0 * (double) recall[2][i] / recallCount[2][i]);
//
////                System.out.println("2");
////                System.out.println(100.0 * (double) avgCorrect[2][i] / (feasibleRun[2][i] + otherRun[2][i]) + "," + 100.0 * (double) avgContains50[2][i] / (feasibleRun[2][i] + otherRun[2][i]) + "," + 100.0 * (double) avgContains75[2][i] / feasibleRun[2][i] + "," + 100.0 * (double) avgContains100[2][i] / feasibleRun[2][i]);
////                System.out.println("recall: " + 100.0 * (double) recall[2][i] / recallCount[2][i]);
//////                System.out.println("avg correct:  " + (double) avgCorrect[i] / (feasibleRun[i] + otherRun[i]));
//////
//////                // System.out.println("avg contains:   " + avgContains);
//////                System.out.println("avg contains50:   " + (double) avgContains50[i] / (feasibleRun[i] + otherRun[i]));
//////                System.out.println("avg contains75:   " + (double) avgContains75[i] / feasibleRun[i]);
//////                System.out.println("avg contains100:   " + (double) avgContains100[i] / feasibleRun[i]);
////                System.out.println("correct max " + correctMax[2][i]);
////                System.out.println("correct min " + correctMin[0][i]);
//                    System.out.println("\n\n\n\n");
                }


//                    bw.write(data);
//			System.out.println("Done");
            } catch (IOException e) {

                e.printStackTrace();

            } finally {

                try {

                    if (bw != null) {
                        bw.close();
                    }

                    if (fw != null) {
                        fw.close();
                    }

                } catch (IOException ex) {

                    ex.printStackTrace();

                }
            }


        }
        if (mpCOllect) {
            saveMeetingPOints();

        }

    }

    public static void saveMeetingPOints() {
        String fileName = "producedMeetingPoint.csv";
        BufferedWriter bw = null;
        FileWriter fw = null;

        try {

            File file = new File(fileName);

            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }
            // true = append file
            fw = new FileWriter(file.getAbsoluteFile());
            bw = new BufferedWriter(fw);

            for (MeetingPoint meetingPoint : sampleMeetingPointSet.getList()) {
                StringBuilder sb = new StringBuilder();

                sb.append(meetingPoint.getPosX());
                sb.append(',');
                sb.append(meetingPoint.getPosY());

                sb.append('\n');

                bw.write(sb.toString());
            }

//                    bw.write(data);
//			System.out.println("Done");
        } catch (IOException e) {

            e.printStackTrace();

        } finally {

            try {

                if (bw != null) {
                    bw.close();
                }

                if (fw != null) {
                    fw.close();
                }

            } catch (IOException ex) {

                ex.printStackTrace();

            }
        }
        System.out.println("mp Collect:" + sampleMeetingPointSet.getList().size());

    }

    private static boolean makeResult(MemberSet memberSet, MeetingPointSet mPointSet, boolean comb, boolean mpCollect) {
        int it = totalMeetingPOint;
        long start = System.currentTimeMillis();
        long ta = 0, tb = 0;
        PriorityQueue<AssistantClass> as = new PriorityQueue<>(queueMem, (a, b) -> a.getminDistance().compareTo(b.getminDistance()));

        int totalMemberConsidered = 0;
        MeetingPointSet workingMeetingPointSet = new MeetingPointSet();
        ArrayList<Rectangle> rects = new ArrayList<>();
        SpatialIndex si = new RTree();
        si.init(null);
        int cnt = 0;
        for (Member m : memberSet.getList()) {
            Rectangle r = new Rectangle(m.getPosX(), m.getPosY(), m.getPosX(), m.getPosY());
            rects.add(r);
            si.add(r, cnt);
            cnt++;
        }

        for (int q = 0; q < it; q++) {
            RestSet vRest = new RestSet();
            MeetingPoint mp = new MeetingPoint(mPointSet.getList().get(rand[q]).getPosX(), mPointSet.getList().get(rand[q]).getPosY(), rand[q]);

            final Point p = new Point(mPointSet.getList().get(rand[q]).getPosX(), mPointSet.getList().get(rand[q]).getPosY());


            // the commented part was for demo  illustration purpose


//            if (q == 0) {
//                vRest.addMember(0);
//                vRest.addMember(4);
//                vRest.addMember(1);
//                vRest.addMember(3);
//                vRest.addMember(2);
//                mp.addEntry(0, 1);
//                mp.addEntry(4, 1);
//                mp.addEntry(1, 2);
//                mp.addEntry(3, 14);
//                mp.addEntry(2, 15);
//
//            } else {
//                vRest.addMember(0);
//                vRest.addMember(1);
//                vRest.addMember(3);
//                vRest.addMember(4);
//                vRest.addMember(2);
//                mp.addEntry(0, 11);
//                mp.addEntry(1, 12);
//                mp.addEntry(3, 13);
//                mp.addEntry(4, 13);
//                mp.addEntry(2, 14);
//            }

            si.nearestN(p, new TIntProcedure() {
                public boolean execute(int i) {
                    vRest.addMember(memberSet.getList().get(i).getMemberId());
//                    System.out.println(memberSet.getList().get(i).getMemberId() + "  " + rects.get(i).distance(p));
                    mp.addEntry(memberSet.getList().get(i).getMemberId(), rects.get(i).distance(p));
                    return true;
                }

            }, memberSet.getMemberSetSize(), maxDistance);
//            System.out.print(q + "  ");
//            System.out.println(vRest.getSize());
            ta = System.currentTimeMillis();
//            System.out.print(vRest.getSize()+"        ");
            vRest.pruneUnqualifiedMembers();     /// we discard members not having enough connection in Vr
//            System.out.println(vRest.getSize());
            tb = System.currentTimeMillis();
            totalMemberConsidered += vRest.getSize();

            workingMeetingPointSet.addMeetingPOint(mp);

            if (vRest.getSize() != 0) {
//                as.add(new AssistantClass(vRest, new IntermediateSet(), vRest.getMinimumDistance(0), mPointSet.getList().get(rand[q]),
//                        (double) vRest.getTotalDistance() / vRest.getSize(),
//                        (double) vRest.getTotalDistanceUpTon() / minimumMember, vRest.getMaxDegree(), false));
                RestSet foundRestSet = null;

//        public AssistantClass(RestSet restSet, IntermediateSet intermediateSet, float minDistance, Integer mpId,
//                float avgDistance, float avgDistanceUpToN, int initialMaxDegree, boolean updated, RestSet foundRestSet
//        ) {
                as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
                        vRest.getMaxDegree(), true,
                        foundRestSet));
//                vRest.showMemberList();
                if (mpCollect && vRest.getSize() >= minimumMember) {
                    if (!sampleMeetingPointSet.getList().contains(mPointSet.getList().get(rand[q]))) {
                        sampleMeetingPointSet.addMeetingPOint(mPointSet.getList().get(rand[q]));
                    }

                }

            }
//            si=null;
//            System.out.print(q + "  ");
//            System.out.println(vRest.getSize());
        }

        System.out.println(">>>> " + totalMemberConsidered);
        System.out.println("quque size: " + as.size());
        c = 0;
        totalCalled = 0;
        boolean complete = false;
        if (!mpCollect) {
            if (comb) {
                System.out.println(".......calling  exact.........");
                complete = generateRankList(as, rects, workingMeetingPointSet, si, comb, start);

//                System.out.println(".......calling  exact baseline wokring version.........");
//                complete = re_revised_generateRankListBaseline(as, rects, workingMeetingPointSet, si, comb, start);


            } else {
//                System.out.println("...... calling baseline...........");
////                complete = generateRankListBaseline(as, rects, workingMeetingPointSet, si, comb, start);
//                complete = revised_generateRankListBaseline(as, rects, workingMeetingPointSet, si, comb, start);
////                complete = generateRankList_New_Baseline(as, rects, workingMeetingPointSet, si, comb, start);
                System.out.println(".......calling  exact baseline wokring version.........");
                complete = re_revised_generateRankListBaseline(as, rects, workingMeetingPointSet, si, comb, start);


            }
        }
        as.clear();
        long now = System.currentTimeMillis();
//        System.out.println(".................................... " + totalCalled);
        if (comb) {
            totalIt[presentSampleIndex] += totalCalled;
            totalTime[presentSampleIndex] += tb - ta + now - start;
            retrievedMember[presentSampleIndex] += totalMemberConsidered;

        } else {
            totalitBaseline[presentSampleIndex] += totalCalled;
            totalTimeBaseline[presentSampleIndex] += tb - ta + now - start;
            retrievedMemberBaseline[presentSampleIndex] += totalMemberConsidered;

        }

//        System.out.println(groupFound);

        int ccnt = 0;
//        for (ResultGroup r : resultLists) {
////            System.out.println("ki");
//            ccnt++;
//            System.out.println(ccnt + "\n" + r.score);
//            r.showMembers();
//            System.out.println(r.mpID);
//
//            System.out.println("");
//        }
//        System.out.println("\n\n");


        as.clear();


        System.out.println("group paoa gase: " + groupFound);
        return complete;
    }

    private static void makeResult_New_baseline(MemberSet memberSet, MeetingPointSet mPointSet, boolean comb, boolean mpCollect) {
        System.out.println("makeResult for new baseline");
        int it = totalMeetingPOint;
        long start = System.currentTimeMillis();
        long ta = 0, tb = 0;
        int totalMemberConsidered = 0;
        ArrayList<Rectangle> rects = new ArrayList<>();
        SpatialIndex si = new RTree();
        si.init(null);
        int cnt = 0;
        for (Member m : memberSet.getList()) {
            Rectangle r = new Rectangle(m.getPosX(), m.getPosY(), m.getPosX(), m.getPosY());
            rects.add(r);
            si.add(r, cnt);
            cnt++;
        }
        ArrayList<AssistantClass> save_all_assistentClass = new ArrayList<>();
        MeetingPointSet workingMeetingPointSet = new MeetingPointSet();
        ArrayList<Integer> groupSizeList = new ArrayList<>();
        for (int q = 0; q < it; q++) {
//            System.out.println(q);

            PriorityQueue<AssistantClass> as = new PriorityQueue<>(queueMem, (a, b) -> a.getminDistance().compareTo(b.getminDistance()));

            RestSet vRest[] = new RestSet[maxGroupSize - minimumMember + 1];
            for (int each_val = 0; each_val <= maxGroupSize - minimumMember; each_val++) {
                vRest[each_val] = new RestSet();
            }


            MeetingPoint mp = new MeetingPoint(mPointSet.getList().get(rand[q]).getPosX(), mPointSet.getList().get(rand[q]).getPosY(), rand[q]);

            final Point p = new Point(mPointSet.getList().get(rand[q]).getPosX(), mPointSet.getList().get(rand[q]).getPosY());


            // the commented part was for demo  illustration purpose


            si.nearestN(p, new TIntProcedure() {
                public boolean execute(int i) {

                    for (int each_val = 0; each_val <= maxGroupSize - minimumMember; each_val++) {
                        vRest[each_val].addMember(memberSet.getList().get(i).getMemberId());
                    }
//                    System.out.println(memberSet.getList().get(i).getMemberId() + "  " + rects.get(i).distance(p));
                    mp.addEntry(memberSet.getList().get(i).getMemberId(), rects.get(i).distance(p));
                    return true;
                }

            }, memberSet.getMemberSetSize(), maxDistance);
//            System.out.print(q + "  ");
//            System.out.println(vRest.getSize());
            ta = System.currentTimeMillis();
            for (int each_val = 0; each_val <= maxGroupSize - minimumMember; each_val++) {
                vRest[each_val].pruneUnqualifiedMembers();
                totalMemberConsidered += vRest[each_val].getSize();

            }
            tb = System.currentTimeMillis();

            workingMeetingPointSet.addMeetingPOint(mp);


            for (int each_val = 0; each_val <= maxGroupSize - minimumMember; each_val++) {
                if (vRest[each_val].getSize() != 0) {
//                as.add(new AssistantClass(vRest, new IntermediateSet(), vRest.getMinimumDistance(0), mPointSet.getList().get(rand[q]),
//                        (double) vRest.getTotalDistance() / vRest.getSize(),
//                        (double) vRest.getTotalDistanceUpTon() / minimumMember, vRest.getMaxDegree(), false));
                    RestSet foundRestSet = null;
                    save_all_assistentClass.add(new AssistantClass(vRest[each_val], new IntermediateSet(), mp.getMemberDistance(vRest[each_val].getMemberId(0)), q,
                            vRest[each_val].getMaxDegree(), true,
                            foundRestSet));
                    groupSizeList.add(minimumMember + each_val);
                }

            }
        }


        System.out.println(">>>> " + totalMemberConsidered);

        totalCalled = 0;


//        todo iterate over minimum group size
        int saved_minimumMember = minimumMember;
        int ccnt = 0;
        for (AssistantClass each_instance : save_all_assistentClass) {
            minimumMember = groupSizeList.get(ccnt);
            ccnt++;
            PriorityQueue<AssistantClass> as = new PriorityQueue<>(queueMem, (a, b) -> a.getminDistance().compareTo(b.getminDistance()));


            as.add(each_instance);
            c = 0;

            if (!mpCollect && !comb) {
//                    System.out.println("called baseline");
                generateRankList_New_Baseline(as, rects, workingMeetingPointSet, si, comb, start);
//                System.out.println(totalCalled);
            }
            as.clear();

        }

        minimumMember = saved_minimumMember;
        //        System.out.println("minimum group size   " + minimumMember);


        long now = System.currentTimeMillis();
//        System.out.println(".................................... " + totalCalled);
        if (!comb) {
            totalitBaseline_extended[presentSampleIndex] += totalCalled;
            totalTimeBaseline_extended[presentSampleIndex] += tb - ta + now - start;
            retrievedMemberBaseline_extended[presentSampleIndex] += totalMemberConsidered;

        }

//        System.out.println(groupFound);

//        int ccnt = 0;
//        for (ResultGroup r : resultLists) {
//            System.out.println("ki");
//            ccnt++;
//            System.out.println(ccnt + "\n" + r.score);
//            r.showMembers();
//            System.out.println(r.mpID);
//
//            System.out.println("");
//        }
//        System.out.println("\n\n");
//
//
//        System.out.println("group paoa gase: " + groupFound);
        return;
    }


    private static void makeResultApprox(MemberSet memberSet, MeetingPointSet mPointSet, boolean comb) {


        int it = totalMeetingPOint;
        long start = System.currentTimeMillis();
        long ta = 0, tb = 0;
        PriorityQueue<AssistantClass> as = new PriorityQueue<>(queueMem, (a, b) -> a.getminDistance().compareTo(b.getminDistance()));
        int totalMemberConsidered = 0;
        MeetingPointSet workingMeetingPointSet = new MeetingPointSet();
        ArrayList<Rectangle> rects = new ArrayList<>();
        SpatialIndex si = new RTree();
        si.init(null);
        int cnt = 0;
        for (Member m : memberSet.getList()) {
            Rectangle r = new Rectangle(m.getPosX(), m.getPosY(), m.getPosX(), m.getPosY());
            rects.add(r);
            si.add(r, cnt);
            cnt++;
        }

        for (int q = 0; q < it; q++) {
            RestSet vRest = new RestSet();
            MeetingPoint mp = new MeetingPoint(mPointSet.getList().get(rand[q]).getPosX(), mPointSet.getList().get(rand[q]).getPosY(), rand[q]);

            final Point p = new Point(mPointSet.getList().get(rand[q]).getPosX(), mPointSet.getList().get(rand[q]).getPosY());

            si.nearestN(p, new TIntProcedure() {
                public boolean execute(int i) {
                    vRest.addMember(memberSet.getList().get(i).getMemberId());
//                    System.out.println(memberSet.getList().get(i).getMemberId() + "  " + rects.get(i).distance(p));
                    mp.addEntry(memberSet.getList().get(i).getMemberId(), rects.get(i).distance(p));
                    return true;
                }

            }, memberSet.getMemberSetSize(), maxDistance);

//            System.out.print(q + "  ");
//            System.out.println(vRest.getSize());
            ta = System.currentTimeMillis();

            vRest.pruneUnqualifiedMembers();
            tb = System.currentTimeMillis();
//            System.out.println(vRest.getMinDegree());
            totalMemberConsidered += vRest.getSize();

            workingMeetingPointSet.addMeetingPOint(mp);

            if (vRest.getSize() != 0) {
//                as.add(new AssistantClass(vRest, new IntermediateSet(), vRest.getMinimumDistance(0), mPointSet.getList().get(rand[q]),
//                        (double) vRest.getTotalDistance() / vRest.getSize(),
//                        (double) vRest.getTotalDistanceUpTon() / minimumMember, vRest.getMaxDegree(), false));
                RestSet foundRestSet = null;

//        public AssistantClass(RestSet restSet, IntermediateSet intermediateSet, float minDistance, Integer mpId,
//                float avgDistance, float avgDistanceUpToN, int initialMaxDegree, boolean updated, RestSet foundRestSet
//        ) {
                as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
                        vRest.getMaxDegree(), true,
                        foundRestSet));

            }
//            si=null;
//            System.out.print(q + "  ");
//            System.out.println(vRest.getSize());
        }

        System.out.println(">>>> " + totalMemberConsidered);
        System.out.println("quque size: " + as.size());
        c = 0;
        totalCalled = 0;

        if (comb) {
            System.out.println(".......calling  approx.........");

            generateRankListApprox(as, rects, workingMeetingPointSet, si, comb);

        } else {
            System.out.println(".......calling  greedy.........");

            generateRankListApprox(as, rects, workingMeetingPointSet, si, comb);
        }
//        generateKcore(as, rects, workingMeetingPointSet, si, comb, start);
//        generateKTruss(as, rects, workingMeetingPointSet, si, comb, start);
        long now = System.currentTimeMillis();
//        System.out.println(".................................... " + totalCalled);

        as.clear();
        if (comb) {
            retrievedMemberApprox[presentSampleIndex] += totalMemberConsidered;
            totalItApprox[presentSampleIndex] += totalCalled;
            totalTimeApprox[presentSampleIndex] += tb - ta + now - start;

        } else {
            retrievedMemberGreedy[presentSampleIndex] += totalMemberConsidered;
            totalItGreedy[presentSampleIndex] += totalCalled;
            totalTimeGreedy[presentSampleIndex] += tb - ta + now - start;

        }


        System.out.println("group paoa gase: " + groupFound);


//        int ccnt = 0;
//        for (ResultGroup r : resultLists) {
//            ccnt++;
//            System.out.println(ccnt + "  " + r.score);
//            r.showMembers();
//            System.out.println(r.getMinDegree());
//            System.out.println(r.getMp());
//            System.out.println("");
//        }
//        System.out.println("\n\n");
    }

    static int totalRetrievedMember = 0;


    static int adP = 0;
    static int notFeas = 0;


    private static boolean generateRankList(PriorityQueue<AssistantClass> queue, ArrayList<Rectangle> rects, MeetingPointSet mpSet,
                                            SpatialIndex siList, boolean comb, long start) {

        while (queue.size() != 0) {
//            System.out.println(queue.size());
            long now = System.currentTimeMillis();
            if (now - start > timeOut) {
                return false;
            }
            AssistantClass assistantClass = queue.remove();
            Integer mpID = assistantClass.meetingPointId;
            IntermediateSet vIntermediateSet = assistantClass.intermediateSet;
            RestSet vRestSet = assistantClass.restSet;
            RestSet foundRestSet = assistantClass.foundRestSet;
//            System.out.println("\\\\");
//            System.out.print(mpID - (mpID - 1) * 2);
//            System.out.print(" & ");

//            vIntermediateSet.showMemberList();
//            System.out.print(" & ");

//            vRestSet.showMemberList();
//            System.out.print(" & ");

//            System.out.println("");
//            System.out.println("dist "+ assistantClass.minDistance);
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//            if (vIntermediateSet.getSize() + vRestSet.getSize() < minimumMember || vRestSet.getSize() == 0) {
//                continue;
//            }
            if (vIntermediateSet.getSize() >= maxGroupSize || vIntermediateSet.getSize() + vRestSet.getSize() < minimumMember) {
//                System.out.println("??");
//System.out.println("y");
                continue;

            }
//

//            System.out.println("before");
//            System.out.println(queue.size());
            Score s = new Score(vIntermediateSet);
            boolean resultUpdated = false;

            Integer m = null;
            float dMin = 100;
            int index;
//            vIntermediateSet.showMemberList();
//            vRestSet.showMemberList();

            int size = vRestSet.getSize() + vIntermediateSet.getSize();
//        System.out.println(size);
//        System.out.println(vRestSet.isEmpty());
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//            while (size >= minimumMember && !vRestSet.isEmpty()) {
            while (vIntermediateSet.getSize() < maxGroupSize && !vRestSet.isEmpty()) {

                if (vRestSet.foundUnvisitedVertex()) {
                    m = vRestSet.topMember();
                    index = vRestSet.getLocalMemberIndex(m);
                    vRestSet.markVisited(index);

                    dMin = mpSet.getList().get(mpID).getMemberDistance(m);
//                System.out.println("member id: " + m.getMemberId() + "  distance : " + dMin + "           index:    " + index);

                } else {
//                System.out.println("ar member nai ");
//System.out.println("hm");
                    break;
                }

                if (resultLists.size() == topK) {
                    ResultGroup r = resultLists.get(topK - 1);
                    int ttlGroupSize = vIntermediateSet.getGroupSize()
                            + vRestSet.getUnvisitedMemberCount() + 1;
                    int sz = vIntermediateSet.getSize();
                    boolean adTerminate = true;
                    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    for (int i = minimumMember; i <= maxGroupSize; i++) {

//                        for (int i = minimumMember; i <= ttlGroupSize; i++) {
                        if (sz >= i) {
                            continue;
                        }
                        double pruningDistance = s.advancePruningDistance(r.getTotalConnectivity(),
                                r.getGroupSize(), r.getTotalDistance(), assistantClass.initialMaxDegree,
                                i);
//                        System.out.println(pruningDistance);
//                        System.out.println(dMin * (i - vIntermediateSet.getSize()));

                        if (pruningDistance > (double) dMin * (i - vIntermediateSet.getSize())) {
//                        System.out.println("\n\n\n\ntermination advance");
//                        System.out.println((ttlGroupSize - vIntermediateSet.getSize()));
//                        //   System.out.println(m.getMemberId());
//                        System.out.println(pruningDistance);
//                        System.out.println((double) dMin * (ttlGroupSize - vIntermediateSet.getSize()));

                            adTerminate = false;
                            break;
                        }
                    }
                    if (adTerminate) {
//                    adP++;

//                        System.out.println(" a.t & ");
                        break;

                    }

                }


                ResultGroup currentGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                        vIntermediateSet.getTotalDistance(),
                        vIntermediateSet.getSize(),
                        vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);


                double newCon = minimumConstraint - vIntermediateSet.connectionIfAdded(m);
                double rightSide = (minimumMember - 1 - vIntermediateSet.getSize());


                if ((vIntermediateSet.getSize() < minimumMember && newCon <= rightSide) ||
                        (vIntermediateSet.getSize() >= minimumMember
                                && vIntermediateSet.connectionIfAdded(m) >= minimumConstraint)) {


                    vIntermediateSet.addMember(m, dMin);
                    boolean updated = false;

                    if (vIntermediateSet.getSize() >= minimumMember) {

//                             public ResultGroup(ArrayList<Integer> imemberList,
//             float totalDistance,
//            int groupSize, int totalConnectivity, double score, Integer mpId) {
                        ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                vIntermediateSet.getTotalDistance(),
                                vIntermediateSet.getSize(),
                                vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                        int minCon = resultGroup.getMinDegree();
                        boolean isFeasibleGroup = true;
                        if (minCon < minimumConstraint) {
                            isFeasibleGroup = false;
                        }

                        if (isFeasibleGroup) {
                            if (groupFound < topK) {
                                groupFound++;
                                updated = true;
                                for (ResultGroup r : resultLists) {
                                    if (r.getMemberIdList().equals(currentGroup.getMemberIdList())) {
                                        resultLists.remove(r);
                                        groupFound--;
//                                        System.out.println("hmmmm");
//                                        r.showMembers();
//                                        currentGroup.showMembers();
                                        break;
                                    }
                                }
                                resultLists.add(resultGroup);
                                Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                            resultGroup.getMaxDegree();
//                                System.out.print(" and recorded &");
//                            resultGroup.showConnection();
                            } else {
                                ResultGroup r = resultLists.get(topK - 1);
                                if (r.score < resultGroup.score) {
                                    updated = true;
                                    resultLists.add(resultGroup);
                                    groupFound++;
                                    boolean overlapGroup = false;
                                    for (ResultGroup rG : resultLists) {
                                        if (rG.getMemberIdList().equals(currentGroup.getMemberIdList())) {
                                            resultLists.remove(rG);
                                            groupFound--;
//                                            System.out.println("hmmmm");
//                                            rG.showMembers();
                                            overlapGroup = true;
//                                            currentGroup.showMembers();
                                            break;
                                        }
                                    }
//                                    groupFound++;
                                    if (!overlapGroup) {
                                        resultLists.remove(topK - 1);
                                        groupFound--;
                                    }
                                    Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                                resultGroup.getMaxDegree();
//                                resultGroup.showConnection();
//                                    System.out.print(" and recorded &");
                                }

                            }
                        }


                    }
                    vRestSet.popMember(index);
                    totalCalled++;
                    RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                            vRestSet.getVisited(), vRestSet.maxMember);
                    newRestSet.markAllUnVisited();

                    IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                            vIntermediateSet.getMaxFriend(),
                            vIntermediateSet.getTotalDistance(),
                            vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                    if (newRestSet.getSize() != 0) {
                        queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                assistantClass.initialMaxDegree, false,
                                foundRestSet));

                    }

                    if (comb) {
                        vIntermediateSet.popMember(m, dMin);

                        if (vRestSet.getSize() != 0) {
                            queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                    assistantClass.initialMaxDegree, false,
                                    foundRestSet));

                        }

                    }
//                        System.out.println("after");
//                        vIntermediateSet.showMemberList();

                    break;

                }
            }
        }

        return true;
    }

    private static boolean saved_generateRankList(PriorityQueue<AssistantClass> queue, ArrayList<Rectangle> rects, MeetingPointSet mpSet,
                                                  SpatialIndex siList, boolean comb, long start) {

        while (queue.size() != 0) {
//            System.out.println(queue.size());
            long now = System.currentTimeMillis();
            if (now - start > timeOut) {
                return false;
            }
            AssistantClass assistantClass = queue.remove();
            Integer mpID = assistantClass.meetingPointId;
            IntermediateSet vIntermediateSet = assistantClass.intermediateSet;
            RestSet vRestSet = assistantClass.restSet;
            RestSet foundRestSet = assistantClass.foundRestSet;
//            System.out.println("\\\\");
//            System.out.print(mpID - (mpID - 1) * 2);
//            System.out.print(" & ");

//            vIntermediateSet.showMemberList();
//            System.out.print(" & ");

//            vRestSet.showMemberList();
//            System.out.print(" & ");

//            System.out.println("");
//            System.out.println("dist "+ assistantClass.minDistance);
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//            if (vIntermediateSet.getSize() + vRestSet.getSize() < minimumMember || vRestSet.getSize() == 0) {
//                continue;
//            }
            if (vIntermediateSet.getSize() >= maxGroupSize || vIntermediateSet.getSize() + vRestSet.getSize() < minimumMember) {
//                System.out.println("??");
//System.out.println("y");
                continue;

            }
//

//            System.out.println("before");
//            System.out.println(queue.size());
            Score s = new Score(vIntermediateSet);
            boolean resultUpdated = false;

            Integer m = null;
            float dMin = 100;
            int index;
//            vIntermediateSet.showMemberList();
//            vRestSet.showMemberList();

            int size = vRestSet.getSize() + vIntermediateSet.getSize();
//        System.out.println(size);
//        System.out.println(vRestSet.isEmpty());
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//            while (size >= minimumMember && !vRestSet.isEmpty()) {
            while (vIntermediateSet.getSize() < maxGroupSize && !vRestSet.isEmpty()) {

                if (vRestSet.foundUnvisitedVertex()) {
                    m = vRestSet.topMember();
                    index = vRestSet.getLocalMemberIndex(m);
                    vRestSet.markVisited(index);

                    dMin = mpSet.getList().get(mpID).getMemberDistance(m);
//                System.out.println("member id: " + m.getMemberId() + "  distance : " + dMin + "           index:    " + index);

                } else {
//                System.out.println("ar member nai ");
//System.out.println("hm");
                    break;
                }

                if (resultLists.size() == topK) {
                    ResultGroup r = resultLists.get(topK - 1);
                    int ttlGroupSize = vIntermediateSet.getGroupSize()
                            + vRestSet.getUnvisitedMemberCount() + 1;
                    int sz = vIntermediateSet.getSize();
                    boolean adTerminate = true;
                    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    for (int i = minimumMember; i <= maxGroupSize; i++) {

//                        for (int i = minimumMember; i <= ttlGroupSize; i++) {
                        if (sz >= i) {
                            continue;
                        }
                        double pruningDistance = s.advancePruningDistance(r.getTotalConnectivity(),
                                r.getGroupSize(), r.getTotalDistance(), assistantClass.initialMaxDegree,
                                i);
//                        System.out.println(pruningDistance);
//                        System.out.println(dMin * (i - vIntermediateSet.getSize()));

                        if (pruningDistance > (double) dMin * (i - vIntermediateSet.getSize())) {
//                        System.out.println("\n\n\n\ntermination advance");
//                        System.out.println((ttlGroupSize - vIntermediateSet.getSize()));
//                        //   System.out.println(m.getMemberId());
//                        System.out.println(pruningDistance);
//                        System.out.println((double) dMin * (ttlGroupSize - vIntermediateSet.getSize()));

                            adTerminate = false;
                            break;
                        }
                    }
                    if (adTerminate) {
//                    adP++;

//                        System.out.println(" a.t & ");
                        break;

                    }

                }


                ResultGroup currentGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                        vIntermediateSet.getTotalDistance(),
                        vIntermediateSet.getSize(),
                        vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);


                if (vIntermediateSet.getSize() < minimumMember) {
//                    System.out.print(" $ " + (char) (m + 'a') + " $ &");
//                    vIntermediateSet.showMemberList();
//                    System.out.println(assistantClass.updated);

                    double newCon = minimumConstraint - vIntermediateSet.connectionIfAdded(m);
                    double rightSide = (minimumMember - 1 - vIntermediateSet.getSize());
                    if (newCon <= rightSide) {

                        vIntermediateSet.addMember(m, dMin);
                        boolean updated = false;

                        if (vIntermediateSet.getSize() == minimumMember) {

//                             public ResultGroup(ArrayList<Integer> imemberList,
//             float totalDistance,
//            int groupSize, int totalConnectivity, double score, Integer mpId) {
                            ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getSize(),
                                    vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                            int minCon = resultGroup.getMinDegree();
                            if (minCon < minimumConstraint) {
                                vIntermediateSet.popMember(m, dMin);

//                            System.out.println("group formed but not satisfied group:");
//                            //    resultGroup.getMaxDegree();
//                                System.out.print ("n.f &");
                                notFeas++;
                                continue;
                            }
// System.out.print(" included &");

                            if (groupFound < topK) {
                                groupFound++;
                                updated = true;
                                for (ResultGroup r : resultLists) {
                                    if (r.getMemberIdList().equals(currentGroup.getMemberIdList())) {
                                        resultLists.remove(r);
                                        groupFound--;
//                                        System.out.println("hmmmm");
//                                        r.showMembers();
//                                        currentGroup.showMembers();
                                        break;
                                    }
                                }
                                resultLists.add(resultGroup);
                                Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                            resultGroup.getMaxDegree();
//                                System.out.print(" and recorded &");
//                            resultGroup.showConnection();
                            } else {
                                ResultGroup r = resultLists.get(topK - 1);
                                if (r.score < resultGroup.score) {
                                    updated = true;
                                    resultLists.add(resultGroup);
                                    groupFound++;
                                    boolean overlapGroup = false;
                                    for (ResultGroup rG : resultLists) {
                                        if (rG.getMemberIdList().equals(currentGroup.getMemberIdList())) {
                                            resultLists.remove(rG);
                                            groupFound--;
//                                            System.out.println("hmmmm");
//                                            rG.showMembers();
                                            overlapGroup = true;
//                                            currentGroup.showMembers();
                                            break;
                                        }
                                    }
//                                    groupFound++;
                                    if (!overlapGroup) {
                                        resultLists.remove(topK - 1);
                                        groupFound--;
                                    }
                                    Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                                resultGroup.getMaxDegree();
//                                resultGroup.showConnection();
//                                    System.out.print(" and recorded &");
                                }

                            }

                        } else {
//                             System.out.print(" included  &");

                        }
                        vRestSet.popMember(index);
                        totalCalled++;
                        RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                vRestSet.getVisited(), vRestSet.maxMember);
                        newRestSet.markAllUnVisited();

                        IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                vIntermediateSet.getMaxFriend(),
                                vIntermediateSet.getTotalDistance(),
                                vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                        if (newRestSet.getSize() != 0) {
                            queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                    assistantClass.initialMaxDegree, false,
                                    foundRestSet));

                        }

                        if (comb) {
                            vIntermediateSet.popMember(m, dMin);

                            if (vRestSet.getSize() != 0) {
                                queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                        assistantClass.initialMaxDegree, false,
                                        foundRestSet));

                            }

                        }
//                        System.out.println("after");
//                        vIntermediateSet.showMemberList();

                        break;

                    } else {
//                        System.out.println("familily bad member " + m.getMemberId());

//                    System.out.println("\n bad for familiarity constraint\n ");
//                    System.out.print("f.prun &");
//                    System.out.println(minimumConstraint);
//                    System.out.println(minimumMember - 1);
//                    System.out.println(rightSide);
                        adP++;
                    }
                } else {

//                System.out.println("yeap");
//                    System.out.print(" $ " + (char) (m + 'a') + " $ &");
//                    System.out.print(m + " con ");
//                    vIntermediateSet.showMemberList();
//                    System.out.println(assistantClass.updated);
                    int newCon = vIntermediateSet.connectionIfAdded(m);

                    if (newCon >= minimumConstraint) {
//                    System.out.println("no");
                        if (groupFound < topK) {
                            vIntermediateSet.addMember(m, dMin);

//                        System.out.println("group sob ase ni");
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
                            ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getSize(),
                                    vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);

                            groupFound++;
                            for (ResultGroup r : resultLists) {
                                if (r.getMemberIdList().equals(currentGroup.getMemberIdList())) {
                                    resultLists.remove(r);
                                    groupFound--;
//                                    System.out.println("hmmmm");
//                                    r.showMembers();
//                                    currentGroup.showMembers();
                                    break;
                                }
                            }

//                            System.out.println("included &");
//                            System.out.print(" and recorded  &");
                            resultLists.add(resultGroup);
                            Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                        resultGroup.getMaxDegree();

                            vRestSet.popMember(index);
                            totalCalled++;
                            RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                    vRestSet.getVisited(), vRestSet.maxMember);
                            newRestSet.markAllUnVisited();

                            IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getMaxFriend(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getTotalConnectivity());
//                            System.out.println("before");
//                            vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                            if (newRestSet.getSize() != 0) {
                                queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                        assistantClass.initialMaxDegree, false,
                                        foundRestSet));

                            }

                            if (comb) {
                                vIntermediateSet.popMember(m, dMin);

                                if (vRestSet.getSize() != 0) {
                                    queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                            assistantClass.initialMaxDegree, false,
                                            foundRestSet));

                                }

                            }
//                            System.out.println("after");
//                            vIntermediateSet.showMemberList();
                            break;

//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//                            continue;
                        }

                        vIntermediateSet.addMember(m, dMin);
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();

                        ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                vIntermediateSet.getTotalDistance(),
                                vIntermediateSet.getSize(),
                                vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                        ResultGroup r = resultLists.get(topK - 1);
                        boolean updated = false;

//                        System.out.println("included &");
                        if (r.score < resultGroup.score) {
                            updated = true;
                            resultLists.add(resultGroup);
                            groupFound++;
                            boolean overlapGroup = false;
                            for (ResultGroup rG : resultLists) {
                                if (rG.getMemberIdList().equals(currentGroup.getMemberIdList())) {
                                    resultLists.remove(rG);
                                    groupFound--;
//                                    System.out.println("hmmmm");
//                                    rG.showMembers();
                                    overlapGroup = true;
//                                    currentGroup.showMembers();
                                    break;
                                }
                            }
//                            groupFound++;
                            if (!overlapGroup) {
                                resultLists.remove(topK - 1);
                                groupFound--;

                            }
                            Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                            System.out.print(" and recorded &");
//                            resultGroup.getMaxDegree();
                        }
                        vRestSet.popMember(index);
                        totalCalled++;

                        RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                vRestSet.getVisited(), vRestSet.maxMember);
                        newRestSet.markAllUnVisited();

                        IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                vIntermediateSet.getMaxFriend(),
                                vIntermediateSet.getTotalDistance(),
                                vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                        if (newRestSet.getSize() != 0) {
                            queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                    assistantClass.initialMaxDegree, false,
                                    foundRestSet));

                        }

                        if (comb) {
                            vIntermediateSet.popMember(m, dMin);

                            if (vRestSet.getSize() != 0) {
                                queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                        assistantClass.initialMaxDegree, false,
                                        foundRestSet));

                            }

                        }
//                        System.out.println("after");
//                        vIntermediateSet.showMemberList();

                        break;

                    } else {
//                        System.out.println("familily bad member " + m.getMemberId());
//                        System.out.print("f.prun & ");
                    }

                }
            }
//            assistantClass.restSet.getMemberList().clear();
//            assistantClass.intermediateSet.getDistanceList().clear();
//            assistantClass.intermediateSet.getMemberList().clear();
//            assistantClass.restSet.getDistanceList().clear();
        }
        return true;
    }

    private static boolean generateRankListBaseline(PriorityQueue<AssistantClass> queue, ArrayList<Rectangle> rects, MeetingPointSet mpSet,
                                                    SpatialIndex siList, boolean comb, long start) {
        System.out.println("old baseline");
        while (queue.size() != 0) {
//            System.out.println(queue.size());
            long now = System.currentTimeMillis();
            if (now - start > timeOut) {
                return false;
            }
            AssistantClass assistantClass = queue.remove();
            Integer mpID = assistantClass.meetingPointId;
            IntermediateSet vIntermediateSet = assistantClass.intermediateSet;
            RestSet vRestSet = assistantClass.restSet;
            RestSet foundRestSet = assistantClass.foundRestSet;

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//            if (vIntermediateSet.getSize() + vRestSet.getSize() < minimumMember || vRestSet.getSize() == 0) {
//                continue;
//            }
            if (vIntermediateSet.getSize() >= maxGroupSize || vIntermediateSet.getSize() + vRestSet.getSize() < minimumMember) {
//                System.out.println("??");
//System.out.println("y");
                continue;

            }

//

//            System.out.println("before");
//            System.out.println(queue.size());
            Score s = new Score(vIntermediateSet);
            boolean resultUpdated = false;

            Integer m = null;
            float dMin = 100;
            int index;
//            vIntermediateSet.showMemberList();
//            vRestSet.showMemberList();

            int size = vRestSet.getSize() + vIntermediateSet.getSize();
//        System.out.println(size);
//        System.out.println(vRestSet.isEmpty());
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//            while (size >= minimumMember && !vRestSet.isEmpty()) {
            while (vIntermediateSet.getSize() < maxGroupSize && !vRestSet.isEmpty()) {

                if (vRestSet.foundUnvisitedVertex()) {
                    m = vRestSet.topMember();
                    index = vRestSet.getLocalMemberIndex(m);
                    vRestSet.markVisited(index);

                    dMin = mpSet.getList().get(mpID).getMemberDistance(m);
//                System.out.println("member id: " + m.getMemberId() + "  distance : " + dMin + "           index:    " + index);

                } else {
//                System.out.println("ar member nai ");
                    break;
                }

//                if (resultLists.size() == topK) {
//                    ResultGroup r = resultLists.get(topK - 1);
//                    int ttlGroupSize = vIntermediateSet.getGroupSize()
//                            + vRestSet.getUnvisitedMemberCount() + 1;
//                    int sz = vIntermediateSet.getSize();
//                    boolean adTerminate = true;
//                    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//                    for (int i = minimumMember; i <= maxGroupSize; i++) {
//
////                        for (int i = minimumMember; i <= ttlGroupSize; i++) {
//                        if (sz >= i) {
//                            continue;
//                        }
//                        double pruningDistance = s.advancePruningDistance(r.getTotalConnectivity(),
//                                r.getGroupSize(), r.getTotalDistance(), assistantClass.initialMaxDegree,
//                                i);
////                        System.out.println(pruningDistance);
////                        System.out.println(dMin * (i - vIntermediateSet.getSize()));
//
//                        if (pruningDistance > (double) dMin * (i - vIntermediateSet.getSize())) {
////                        System.out.println("\n\n\n\ntermination advance");
////                        System.out.println((ttlGroupSize - vIntermediateSet.getSize()));
////                        //   System.out.println(m.getMemberId());
////                        System.out.println(pruningDistance);
////                        System.out.println((double) dMin * (ttlGroupSize - vIntermediateSet.getSize()));
//
//                            adTerminate = false;
//                            break;
//                        }
//                    }
//                    if (adTerminate) {
////                    adP++;
//
////                        System.out.println("early termination for member:" + m.getMemberId());
//                        break;
//
//                    }
//
//                }


                ResultGroup currentGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                        vIntermediateSet.getTotalDistance(),
                        vIntermediateSet.getSize(),
                        vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);

                if (vIntermediateSet.getSize() < minimumMember) {
//                    System.out.print(m + " con ");
//                    vIntermediateSet.showMemberList();
//                    System.out.println(assistantClass.updated);

//                    double newCon = minimumConstraint - vIntermediateSet.connectionIfAdded(m);
//                    double rightSide = (minimumMember - 1 - vIntermediateSet.getSize());
//                    if (newCon <= rightSide) {
                    vIntermediateSet.addMember(m, dMin);
                    boolean updated = false;
                    if (vIntermediateSet.getSize() == minimumMember) {

//                             public ResultGroup(ArrayList<Integer> imemberList,
//             float totalDistance,
//            int groupSize, int totalConnectivity, double score, Integer mpId) {
                        ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                vIntermediateSet.getTotalDistance(),
                                vIntermediateSet.getSize(),
                                vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                        int minCon = resultGroup.getMinDegree();
                        if (minCon < minimumConstraint) {
                            vIntermediateSet.popMember(m, dMin);

//                            System.out.println("group formed but not satisfied group:");
//                            //    resultGroup.getMaxDegree();
//                                System.out.println("feasible not found for member: " + m.getMemberId());
                            notFeas++;
                            continue;
                        }
                        if (groupFound < topK) {
                            groupFound++;
                            updated = true;
                            resultLists.add(resultGroup);
                            boolean overlapGroup = false;
                            for (ResultGroup rG : resultLists) {
                                if (rG.getMemberIdList().equals(currentGroup.getMemberIdList())) {
                                    resultLists.remove(rG);
                                    groupFound--;
//                                            System.out.println("hmmmm");
//                                            rG.showMembers();
                                    overlapGroup = true;
//                                            currentGroup.showMembers();
                                    break;
                                }
                            }

                            Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                            resultGroup.getMaxDegree();

//                            resultGroup.showConnection();
                        } else {
                            ResultGroup r = resultLists.get(topK - 1);
                            if (r.score < resultGroup.score) {
                                updated = true;
                                resultLists.add(resultGroup);
                                groupFound++;
                                boolean overlapGroup = false;
                                for (ResultGroup rG : resultLists) {
                                    if (rG.getMemberIdList().equals(currentGroup.getMemberIdList())) {
                                        resultLists.remove(rG);
                                        groupFound--;
//                                            System.out.println("hmmmm");
//                                            rG.showMembers();
                                        overlapGroup = true;
//                                            currentGroup.showMembers();
                                        break;
                                    }
                                }
                                if (!overlapGroup) {
                                    resultLists.remove(topK - 1);
                                    groupFound--;

                                }
                                Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                                resultGroup.getMaxDegree();
//                                resultGroup.showConnection();
                            }

                        }

                    }
                    vRestSet.popMember(index);
                    totalCalled++;
                    RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                            vRestSet.getVisited(), vRestSet.maxMember);
                    newRestSet.markAllUnVisited();

                    IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                            vIntermediateSet.getMaxFriend(),
                            vIntermediateSet.getTotalDistance(),
                            vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                    if (newRestSet.getSize() != 0) {
                        queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                assistantClass.initialMaxDegree, false,
                                foundRestSet));

                    }

//                    if (comb) {
                    vIntermediateSet.popMember(m, dMin);

                    if (vRestSet.getSize() != 0) {
                        queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                assistantClass.initialMaxDegree, false,
                                foundRestSet));

                    }

//                    }
//                        System.out.println("after");
//                        vIntermediateSet.showMemberList();
                    break;

//                    } else {
////                        System.out.println("familily bad member " + m.getMemberId());
//
////                    System.out.println("\n bad for familiarity constraint\n ");
////                    System.out.println(newCon);
////                    System.out.println(minimumConstraint);
////                    System.out.println(minimumMember - 1);
////                    System.out.println(rightSide);
//                        adP++;
//                    }
                } else {

//                System.out.println("yeap");
//                    System.out.print(m + " con ");
//                    vIntermediateSet.showMemberList();
//                    System.out.println(assistantClass.updated);
                    int newCon = vIntermediateSet.connectionIfAdded(m);

                    if (newCon >= minimumConstraint) {
//                    System.out.println("no");
                        if (groupFound < topK) {
                            vIntermediateSet.addMember(m, dMin);

//                        System.out.println("group sob ase ni");
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
                            ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getSize(),
                                    vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);

                            groupFound++;
                            resultLists.add(resultGroup);
                            boolean overlapGroup = false;
                            for (ResultGroup rG : resultLists) {
                                if (rG.getMemberIdList().equals(currentGroup.getMemberIdList())) {
                                    resultLists.remove(rG);
                                    groupFound--;
//                                            System.out.println("hmmmm");
//                                            rG.showMembers();
                                    overlapGroup = true;
//                                            currentGroup.showMembers();
                                    break;
                                }
                            }

                            Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                        resultGroup.getMaxDegree();

                            vRestSet.popMember(index);
                            totalCalled++;
                            RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                    vRestSet.getVisited(), vRestSet.maxMember);
                            newRestSet.markAllUnVisited();

                            IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getMaxFriend(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getTotalConnectivity());
//                            System.out.println("before");
//                            vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                            if (newRestSet.getSize() != 0) {
                                queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                        assistantClass.initialMaxDegree, false,
                                        foundRestSet));

                            }

//                            if (comb) {
                            vIntermediateSet.popMember(m, dMin);

                            if (vRestSet.getSize() != 0) {
                                queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                        assistantClass.initialMaxDegree, false,
                                        foundRestSet));

                            }

//                            }
//                            System.out.println("after");
//                            vIntermediateSet.showMemberList();
                            break;

//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//                            continue;
                        }

                        vIntermediateSet.addMember(m, dMin);
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();

                        ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                vIntermediateSet.getTotalDistance(),
                                vIntermediateSet.getSize(),
                                vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                        ResultGroup r = resultLists.get(topK - 1);
                        boolean updated = false;
                        if (r.score < resultGroup.score) {
                            updated = true;
                            resultLists.add(resultGroup);
                            groupFound++;
                            boolean overlapGroup = false;
                            for (ResultGroup rG : resultLists) {
                                if (rG.getMemberIdList().equals(currentGroup.getMemberIdList())) {
                                    resultLists.remove(rG);
                                    groupFound--;
//                                            System.out.println("hmmmm");
//                                            rG.showMembers();
                                    overlapGroup = true;
//                                            currentGroup.showMembers();
                                    break;
                                }
                            }
                            if (!overlapGroup) {
                                resultLists.remove(topK - 1);
                                groupFound--;

                            }
                            Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                            resultGroup.getMaxDegree();
                        }
                        vRestSet.popMember(index);
                        totalCalled++;
                        RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                vRestSet.getVisited(), vRestSet.maxMember);
                        newRestSet.markAllUnVisited();

                        IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                vIntermediateSet.getMaxFriend(),
                                vIntermediateSet.getTotalDistance(),
                                vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                        if (newRestSet.getSize() != 0) {
                            queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                    assistantClass.initialMaxDegree, false,
                                    foundRestSet));

                        }

//                        if (comb) {
                        vIntermediateSet.popMember(m, dMin);

                        if (vRestSet.getSize() != 0) {
                            queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                    assistantClass.initialMaxDegree, false,
                                    foundRestSet));

                        }

//                        }
//                        System.out.println("after");
//                        vIntermediateSet.showMemberList();
                        break;

                    } else {
//                        System.out.println("familily bad member " + m.getMemberId());

                    }

                }
            }
//            assistantClass.restSet.getMemberList().clear();
//            assistantClass.intermediateSet.getDistanceList().clear();
//            assistantClass.intermediateSet.getMemberList().clear();
//            assistantClass.restSet.getDistanceList().clear();
        }
        return true;
    }

    private static boolean revised_generateRankListBaseline(PriorityQueue<AssistantClass> queue, ArrayList<Rectangle> rects, MeetingPointSet mpSet,
                                                            SpatialIndex siList, boolean comb, long start) {
        System.out.println("old revised baseline");
        while (queue.size() != 0) {
//            System.out.println(queue.size());
            long now = System.currentTimeMillis();
            if (now - start > timeOut) {
                return false;
            }
            AssistantClass assistantClass = queue.remove();
            Integer mpID = assistantClass.meetingPointId;
            IntermediateSet vIntermediateSet = assistantClass.intermediateSet;
            RestSet vRestSet = assistantClass.restSet;
            RestSet foundRestSet = assistantClass.foundRestSet;


            if (vIntermediateSet.getSize() >= maxGroupSize || vIntermediateSet.getSize() + vRestSet.getSize() < minimumMember) {

                continue;

            }


            Score s = new Score(vIntermediateSet);
            boolean resultUpdated = false;

            Integer m = null;
            float dMin = 100;
            int index;

            int size = vRestSet.getSize() + vIntermediateSet.getSize();

            while (vIntermediateSet.getSize() < maxGroupSize && !vRestSet.isEmpty()) {


                if (vRestSet.foundUnvisitedVertex()) {
                    m = vRestSet.topMember();
                    index = vRestSet.getLocalMemberIndex(m);
                    vRestSet.markVisited(index);
                    dMin = mpSet.getList().get(mpID).getMemberDistance(m);

                } else {
                    break;
                }


                ResultGroup currentGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                        vIntermediateSet.getTotalDistance(),
                        vIntermediateSet.getSize(),
                        vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);


                vIntermediateSet.addMember(m, dMin);
//                vIntermediateSet.showMemberList();

                boolean updated = false;
                if (vIntermediateSet.getSize() >= minimumMember) {


                    ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                            vIntermediateSet.getTotalDistance(),
                            vIntermediateSet.getSize(),
                            vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                    int minCon = resultGroup.getMinDegree();

                    boolean isFeasibleGroup = true;
                    if (minCon < minimumConstraint) {
                        isFeasibleGroup = false;
                        notFeas++;
                    }
                    if (isFeasibleGroup) {
                        if (groupFound < topK) {
                            groupFound++;
                            updated = true;
                            resultLists.add(resultGroup);
                            boolean overlapGroup = false;
                            for (ResultGroup rG : resultLists) {
                                if (rG.getMemberIdList().equals(currentGroup.getMemberIdList())) {
                                    resultLists.remove(rG);
                                    groupFound--;
                                    overlapGroup = true;
                                    break;
                                }
                            }

                            Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
                        } else {
                            ResultGroup r = resultLists.get(topK - 1);
                            if (r.score < resultGroup.score) {
                                updated = true;
                                resultLists.add(resultGroup);
                                boolean overlapGroup = false;
                                for (ResultGroup rG : resultLists) {
                                    if (rG.getMemberIdList().equals(currentGroup.getMemberIdList())) {
                                        resultLists.remove(rG);
                                        groupFound--;
                                        overlapGroup = true;
                                        break;
                                    }
                                }
                                groupFound++;
                                if (!overlapGroup) {
//                                    resultLists.remove(topK - 1);

                                }
                                Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
                            }

                        }
                    }


                }
                vRestSet.popMember(index);
                totalCalled++;
                RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                        vRestSet.getVisited(), vRestSet.maxMember);


                newRestSet.markAllUnVisited();

                IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                        vIntermediateSet.getMaxFriend(),
                        vIntermediateSet.getTotalDistance(),
                        vIntermediateSet.getTotalConnectivity());

                if (newRestSet.getSize() != 0) {
                    queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                            assistantClass.initialMaxDegree, false,
                            foundRestSet));

                }

                vIntermediateSet.popMember(m, dMin);

                if (vRestSet.getSize() != 0) {
                    queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                            assistantClass.initialMaxDegree, false,
                            foundRestSet));

                }

                break;

            }
        }
        return true;
    }

    private static boolean re_revised_generateRankListBaseline(PriorityQueue<AssistantClass> queue, ArrayList<Rectangle> rects, MeetingPointSet mpSet,
                                                               SpatialIndex siList, boolean comb, long start) {
        System.out.println("Re revised baseline");
        while (queue.size() != 0) {
//            System.out.println(queue.size());
            long now = System.currentTimeMillis();
            if (now - start > timeOut) {
                return false;
            }
            AssistantClass assistantClass = queue.remove();
            Integer mpID = assistantClass.meetingPointId;
            IntermediateSet vIntermediateSet = assistantClass.intermediateSet;
            RestSet vRestSet = assistantClass.restSet;
            RestSet foundRestSet = assistantClass.foundRestSet;


            if (vIntermediateSet.getSize() >= maxGroupSize || vIntermediateSet.getSize() + vRestSet.getSize() < minimumMember) {

                continue;

            }


            Score s = new Score(vIntermediateSet);
            boolean resultUpdated = false;

            Integer m = null;
            float dMin = 100;
            int index;

            int size = vRestSet.getSize() + vIntermediateSet.getSize();

            while (vIntermediateSet.getSize() < maxGroupSize && !vRestSet.isEmpty()) {


                if (vRestSet.foundUnvisitedVertex()) {
                    m = vRestSet.topMember();
                    index = vRestSet.getLocalMemberIndex(m);
                    vRestSet.markVisited(index);
                    dMin = mpSet.getList().get(mpID).getMemberDistance(m);

                } else {
                    break;
                }

                if (resultLists.size() >= topK) {
                    ResultGroup r = resultLists.get(topK - 1);
                    int ttlGroupSize = vIntermediateSet.getGroupSize()
                            + vRestSet.getUnvisitedMemberCount() + 1;
                    int sz = vIntermediateSet.getSize();
                    boolean adTerminate = true;
                    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    for (int i = minimumMember; i <= maxGroupSize; i++) {

//                        for (int i = minimumMember; i <= ttlGroupSize; i++) {
                        if (sz >= i) {
                            continue;
                        }
                        double pruningDistance = s.advancePruningDistance(r.getTotalConnectivity(),
                                r.getGroupSize(), r.getTotalDistance(), assistantClass.initialMaxDegree,
                                i);
//                        System.out.println(pruningDistance);
//                        System.out.println(dMin * (i - vIntermediateSet.getSize()));

                        if (pruningDistance > (double) dMin * (i - vIntermediateSet.getSize())) {
//                        System.out.println("\n\n\n\ntermination advance");
//                        System.out.println((ttlGroupSize - vIntermediateSet.getSize()));
//                        //   System.out.println(m.getMemberId());
//                        System.out.println(pruningDistance);
//                        System.out.println((double) dMin * (ttlGroupSize - vIntermediateSet.getSize()));

                            adTerminate = false;
                            break;
                        }
                    }
                    if (adTerminate) {
//                    adP++;

//                        System.out.println(" a.t & ");
                        break;

                    }

                }


                ResultGroup currentGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                        vIntermediateSet.getTotalDistance(),
                        vIntermediateSet.getSize(),
                        vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);


                vIntermediateSet.addMember(m, dMin);
//                vIntermediateSet.showMemberList();

                boolean updated = false;
                if (vIntermediateSet.getSize() >= minimumMember) {


                    ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                            vIntermediateSet.getTotalDistance(),
                            vIntermediateSet.getSize(),
                            vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                    int minCon = resultGroup.getMinDegree();

                    boolean isFeasibleGroup = true;
                    if (minCon < minimumConstraint) {
                        isFeasibleGroup = false;
                        notFeas++;
                    }
                    if (isFeasibleGroup) {
                        if (groupFound < topK) {
                            groupFound++;
                            updated = true;
                            resultLists.add(resultGroup);
                            boolean overlapGroup = false;
                            for (ResultGroup rG : resultLists) {
                                if (rG.getMemberIdList().equals(currentGroup.getMemberIdList())) {
                                    resultLists.remove(rG);
                                    groupFound--;
                                    overlapGroup = true;
                                    break;
                                }
                            }

                            Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
                        } else {
                            ResultGroup r = resultLists.get(topK - 1);
                            if (r.score < resultGroup.score) {
                                updated = true;
                                resultLists.add(resultGroup);
                                groupFound++;
                                boolean overlapGroup = false;
                                for (ResultGroup rG : resultLists) {
                                    if (rG.getMemberIdList().equals(currentGroup.getMemberIdList())) {
                                        resultLists.remove(rG);
                                        groupFound--;
                                        overlapGroup = true;
                                        break;
                                    }
                                }

                                if (!overlapGroup) {
//                                    resultLists.remove(topK - 1);
//                                    groupFound--;
                                }
                                Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
                            }

                        }
                    }


                }
                vRestSet.popMember(index);
                totalCalled++;
                RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                        vRestSet.getVisited(), vRestSet.maxMember);


                newRestSet.markAllUnVisited();

                IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                        vIntermediateSet.getMaxFriend(),
                        vIntermediateSet.getTotalDistance(),
                        vIntermediateSet.getTotalConnectivity());

                if (newRestSet.getSize() != 0) {
                    queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                            assistantClass.initialMaxDegree, false,
                            foundRestSet));

                }

                vIntermediateSet.popMember(m, dMin);

                if (vRestSet.getSize() != 0) {
                    queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                            assistantClass.initialMaxDegree, false,
                            foundRestSet));

                }

                break;

            }
        }
        return true;
    }

    private static void generateRankList_New_Baseline(PriorityQueue<AssistantClass> queue, ArrayList<Rectangle> rects, MeetingPointSet mpSet,
                                                      SpatialIndex siList, boolean comb, long start) {
//        System.out.println("baseline");
//        System.out.println(minimumMember);
        while (queue.size() != 0) {
//            System.out.println(queue.size());
            long now = System.currentTimeMillis();
            if (now - start > timeOut) {
                return;
            }
            AssistantClass assistantClass = queue.remove();
            Integer mpID = assistantClass.meetingPointId;
            IntermediateSet vIntermediateSet = assistantClass.intermediateSet;
            RestSet vRestSet = assistantClass.restSet;
            RestSet foundRestSet = assistantClass.foundRestSet;

            if (vIntermediateSet.getSize() >= minimumMember || vIntermediateSet.getSize() + vRestSet.getSize() < minimumMember) {

                continue;

            }


            Score s = new Score(vIntermediateSet);
            boolean resultUpdated = false;

            Integer m = null;
            float dMin = 100;
            int index;

            int size = vRestSet.getSize() + vIntermediateSet.getSize();

//            todo maxGroupSize=minimumMember

            while (vIntermediateSet.getSize() < minimumMember && !vRestSet.isEmpty()) {

                if (vRestSet.foundUnvisitedVertex()) {
                    m = vRestSet.topMember();
                    index = vRestSet.getLocalMemberIndex(m);
                    vRestSet.markVisited(index);

                    dMin = mpSet.getList().get(mpID).getMemberDistance(m);
//                System.out.println("member id: " + m.getMemberId() + "  distance : " + dMin + "           index:    " + index);

                } else {
//                System.out.println("ar member nai ");
                    break;
                }


                ResultGroup currentGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                        vIntermediateSet.getTotalDistance(),
                        vIntermediateSet.getSize(),
                        vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);

                double newCon = minimumConstraint - vIntermediateSet.connectionIfAdded(m);
                double rightSide = (minimumMember - 1 - vIntermediateSet.getSize());


                if (vIntermediateSet.getSize() < minimumMember && newCon <= rightSide) {
//                if (vIntermediateSet.getSize() < minimumMember ) {

                    vIntermediateSet.addMember(m, dMin);
                    boolean updated = false;
                    if (vIntermediateSet.getSize() == minimumMember) {

                        ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                vIntermediateSet.getTotalDistance(),
                                vIntermediateSet.getSize(),
                                vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                        int minCon = resultGroup.getMinDegree();
                        boolean isFeasible = true;
                        if (minCon < minimumConstraint) {
//                            vIntermediateSet.popMember(m, dMin);
                            isFeasible = false;
                        }
                        if (isFeasible) {
                            if (groupFound < topK) {
                                groupFound++;
                                updated = true;
                                resultLists.add(resultGroup);
                                boolean overlapGroup = false;
                                for (ResultGroup rG : resultLists) {
                                    if (rG.getMemberIdList().equals(currentGroup.getMemberIdList())) {
                                        resultLists.remove(rG);
                                        groupFound--;
//                                            System.out.println("hmmmm");
//                                            rG.showMembers();
                                        overlapGroup = true;
//                                            currentGroup.showMembers();
                                        break;
                                    }
                                }

                                Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                            resultGroup.getMaxDegree();

//                            resultGroup.showConnection();
                            } else {
                                ResultGroup r = resultLists.get(topK - 1);
                                if (r.score < resultGroup.score) {
                                    updated = true;
                                    resultLists.add(resultGroup);
                                    groupFound++;
                                    boolean overlapGroup = false;
                                    for (ResultGroup rG : resultLists) {
                                        if (rG.getMemberIdList().equals(currentGroup.getMemberIdList())) {
                                            resultLists.remove(rG);
                                            groupFound--;
//                                            System.out.println("hmmmm");
//                                            rG.showMembers();
                                            overlapGroup = true;
//                                            currentGroup.showMembers();
                                            break;
                                        }
                                    }
                                    if (!overlapGroup) {
                                        resultLists.remove(topK - 1);
                                        groupFound--;

                                    }
                                    Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                                resultGroup.getMaxDegree();
//                                resultGroup.showConnection();
                                }

                            }

                        }

                    }
                    vRestSet.popMember(index);
                    totalCalled++;
                    RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                            vRestSet.getVisited(), vRestSet.maxMember);
                    newRestSet.markAllUnVisited();

                    IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                            vIntermediateSet.getMaxFriend(),
                            vIntermediateSet.getTotalDistance(),
                            vIntermediateSet.getTotalConnectivity());

                    if (newRestSet.getSize() != 0) {
                        queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                assistantClass.initialMaxDegree, false,
                                foundRestSet));

                    }

                    vIntermediateSet.popMember(m, dMin);

                    if (vRestSet.getSize() != 0) {
                        queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                assistantClass.initialMaxDegree, false,
                                foundRestSet));

                    }

                    break;
                }
            }
        }
        return;
    }


    private static void generateRankListApprox(PriorityQueue<AssistantClass> queue, ArrayList<Rectangle> rects, MeetingPointSet mpSet,
                                               SpatialIndex siList, boolean comb) {
        while (queue.size() != 0) {
            AssistantClass assistantClass = queue.remove();
            Integer mpID = assistantClass.meetingPointId;
            IntermediateSet vIntermediateSet = assistantClass.intermediateSet;
            RestSet vRestSet = assistantClass.restSet;
            RestSet foundRestSet = assistantClass.foundRestSet;

            //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//            if (vIntermediateSet.getSize() + vRestSet.getSize() < minimumMember || vRestSet.getSize() == 0) {
//                continue;
//            }
            if (vIntermediateSet.getSize() >= maxGroupSize || vIntermediateSet.getSize() + vRestSet.getSize() < minimumMember) {
//                System.out.println("??");
//System.out.println("y");
                continue;

            }


//            System.out.println(queue.size());
            Score s = new Score(vIntermediateSet);
            boolean resultUpdated = false;

            Integer m = null;
            float dMin = 100;
            int index;
//            vIntermediateSet.showMemberList();
//            vRestSet.showMemberList();

            int size = vRestSet.getSize() + vIntermediateSet.getSize();
//        System.out.println(size);
//        System.out.println(vRestSet.isEmpty());
            //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//            while (size >= minimumMember && !vRestSet.isEmpty()) {
            while (vIntermediateSet.getSize() < maxGroupSize && !vRestSet.isEmpty()) {

                if (vRestSet.foundUnvisitedVertex()) {
                    m = vRestSet.topMember();
                    index = vRestSet.getLocalMemberIndex(m);
                    vRestSet.markVisited(index);
//                    System.out.println(mpID);

                    dMin = mpSet.getList().get(mpID).getMemberDistance(m);
//                System.out.println("member id: " + m.getMemberId() + "  distance : " + dMin + "           index:    " + index);

                } else {
//                System.out.println("ar member nai ");
                    break;
                }

                if (resultLists.size() == topK) {
                    ResultGroup r = resultLists.get(topK - 1);
                    int ttlGroupSize = vIntermediateSet.getGroupSize()
                            + vRestSet.getUnvisitedMemberCount() + 1;
                    int sz = vIntermediateSet.getSize();
                    boolean adTerminate = true;
                    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    for (int i = minimumMember; i <= maxGroupSize; i++) {

//                    for (int i = minimumMember; i <= ttlGroupSize; i++) {
                        if (sz >= i) {
                            continue;
                        }
                        double pruningDistance = s.advancePruningDistance(r.getTotalConnectivity(),
                                r.getGroupSize(), r.getTotalDistance(), assistantClass.initialMaxDegree,
                                i);
//                        System.out.println(pruningDistance);
//                        System.out.println(dMin * (i - vIntermediateSet.getSize()));

                        if (pruningDistance > (double) dMin * (i - vIntermediateSet.getSize())) {
//                        System.out.println("\n\n\n\ntermination advance");
//                        System.out.println((ttlGroupSize - vIntermediateSet.getSize()));
//                        //   System.out.println(m.getMemberId());
//                        System.out.println(pruningDistance);
//                        System.out.println((double) dMin * (ttlGroupSize - vIntermediateSet.getSize()));

                            adTerminate = false;
                            break;
                        }
                    }
                    if (adTerminate) {
//                    adP++;

//                        System.out.println("early termination for member:" + m.getMemberId());
                        break;

                    }

                }
                ResultGroup currentGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                        vIntermediateSet.getTotalDistance(),
                        vIntermediateSet.getSize(),
                        vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);


                if (vIntermediateSet.getSize() < minimumMember) {
//                    System.out.println("ki");
//                    System.out.print(m + " con " + dMin + "  ");
//                    vIntermediateSet.showMemberList();
//                    System.out.println(assistantClass.updated);
//                    System.out.println(mpID);

                    double newCon = vIntermediateSet.connectionIfAdded(m);

//                    double rightSide = minimumConstraint - minimumMember + 1 + vIntermediateSet.getSize();
                    double rightSide = (double) (minimumConstraint * vIntermediateSet.getSize()) / (minimumMember - 1.0);
//
////                    double rightSide = Math.floor((double) (minimumConstraint * vIntermediateSet.getSize()) / (minimumMember - 1.0));
                    if (newCon >= rightSide) {
                        vIntermediateSet.addMember(m, dMin);
//                    System.out.println("before");
//                    vIntermediateSet.showMemberList();
                        boolean updated = false;
                        if (vIntermediateSet.getSize() == minimumMember) {
                            ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getSize(),
                                    vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                            int minCon = resultGroup.getMinDegree();
                            boolean isFeasibleGroup = true;
                            if (minCon < minimumConstraint) {
                                isFeasibleGroup = false;
//                                vIntermediateSet.popMember(m, dMin);
//
////                            System.out.println("\n\n\n\n\n ekhane\n\n\n\n");
//                                //    resultGroup.getMaxDegree();
////                            System.out.println("famili bad member "+m.getMemberId());
//                                notFeas++;
//                                continue;
                            }
                            if (isFeasibleGroup) {
                                if (groupFound < topK) {
                                    groupFound++;
                                    updated = true;
                                    for (ResultGroup rG : resultLists) {
                                        if (rG.getMemberIdList().equals(currentGroup.getMemberIdList())) {
                                            resultLists.remove(rG);
                                            groupFound--;
//                                        System.out.println("hmmmm");
//                                        rG.showMembers();
//                                        currentGroup.showMembers();
                                            break;
                                        }
                                    }

                                    resultLists.add(resultGroup);
                                    Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                            resultGroup.getMaxDegree();

//                            resultGroup.showConnection();
                                } else {
                                    ResultGroup r = resultLists.get(topK - 1);
                                    if (r.score < resultGroup.score) {
                                        updated = true;
                                        resultLists.add(resultGroup);
                                        groupFound++;
                                        boolean overlapGroup = false;
                                        for (ResultGroup rG : resultLists) {
                                            if (rG.getMemberIdList().equals(currentGroup.getMemberIdList())) {
                                                resultLists.remove(rG);
                                                groupFound--;
//                                            System.out.println("hmmmm");
//                                            rG.showMembers();
                                                overlapGroup = true;
//                                            currentGroup.showMembers();
                                                break;
                                            }
                                        }
//                                    groupFound++;
                                        if (!overlapGroup) {
                                            resultLists.remove(topK - 1);
                                            groupFound--;
                                        }
                                        Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                                resultGroup.getMaxDegree();
//                                resultGroup.showConnection();
                                    }

                                }

                            }

                        }
                        vRestSet.popMember(index);
                        totalCalled++;

                        RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                vRestSet.getVisited(), vRestSet.maxMember);
                        newRestSet.markAllUnVisited();

                        IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                vIntermediateSet.getMaxFriend(),
                                vIntermediateSet.getTotalDistance(),
                                vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                        if (newRestSet.getSize() != 0) {
                            queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                    assistantClass.initialMaxDegree, updated,
                                    foundRestSet));

                        }

                        if (comb) {
                            vIntermediateSet.popMember(m, dMin);

                            if (vRestSet.getSize() != 0) {
                                queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                        assistantClass.initialMaxDegree, assistantClass.updated,
                                        foundRestSet));

                            }

                        }

                        break;
                    } else {
//                    System.out.println("bad");
                    }
                } else {
//                    System.out.print(m + " con " + dMin + "  ");
//                    vIntermediateSet.showMemberList();
//                    System.out.println(assistantClass.updated);
//                    System.out.println(mpID);

//                    System.out.println("hmmmmmmmmmmmmmmmmmmmmmmmm");
//                    System.out.print(m+" con ");
//                    vIntermediateSet.showMemberList();
//FOR PREVIOUS GROUP
//                System.out.println("over");
                    int newCon = vIntermediateSet.connectionIfAdded(m);
//                System.out.println(newCon);
//                System.out.println(minimumConstraint);

                    if (newCon >= minimumConstraint) {

                        //NO CHECKING FOR TERMINATION......
                        if (groupFound < topK) {
//                        System.out.println("\nmember kom\n");
                            vIntermediateSet.addMember(m, dMin);
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();

                            ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getSize(),
                                    vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                            int minCon = resultGroup.getMinDegree();
                            boolean isFeasibleGroup = true;
                            if (minCon < minimumConstraint) {
                                isFeasibleGroup = false;
//                                vIntermediateSet.popMember(m, dMin);
//
////                            System.out.println("\n\n\n\n\n ekhane\n\n\n\n");
//                                //    resultGroup.getMaxDegree();
////                            System.out.println("famili bad member "+m.getMemberId());
//                                notFeas++;
//                                continue;
                            }
                            if (isFeasibleGroup) {
                                groupFound++;

                                resultLists.add(resultGroup);
                                boolean overlapGroup = false;
                                for (ResultGroup rG : resultLists) {
                                    if (rG.getMemberIdList().equals(currentGroup.getMemberIdList())) {
                                        resultLists.remove(rG);
                                        groupFound--;
//                                    System.out.println("hmmmm");
//                                    rG.showMembers();
//                                    overlapGroup = true;
//                                    currentGroup.showMembers();
                                        break;
                                    }
                                }
                                Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
                            }

//                        resultGroup.getMaxDegree();
                            vRestSet.popMember(index);
                            totalCalled++;

                            RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                    vRestSet.getVisited(), vRestSet.maxMember);
                            newRestSet.markAllUnVisited();

                            IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getMaxFriend(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                            if (newRestSet.getSize() != 0) {
                                queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                        assistantClass.initialMaxDegree, true,
                                        foundRestSet));

                            }

                            if (comb) {
                                vIntermediateSet.popMember(m, dMin);

                                if (vRestSet.getSize() != 0) {
                                    queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                            assistantClass.initialMaxDegree, assistantClass.updated,
                                            foundRestSet));

                                }

                            }

                            break;
//                            continue;
                        }

                        if (assistantClass.updated) {
//                        System.out.println("\n baap included hoise\n");
                            vIntermediateSet.addMember(m, dMin);
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();

                            ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getSize(),
                                    vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                            int minCon = resultGroup.getMinDegree();
                            boolean isFeasibleGroup = true;
                            if (minCon < minimumConstraint) {
                                isFeasibleGroup = false;
//                                vIntermediateSet.popMember(m, dMin);
//
////                            System.out.println("\n\n\n\n\n ekhane\n\n\n\n");
//                                //    resultGroup.getMaxDegree();
////                            System.out.println("famili bad member "+m.getMemberId());
//                                notFeas++;
//                                continue;
                            }
                            boolean updated = false;

                            if (isFeasibleGroup) {
                                ResultGroup r = resultLists.get(topK - 1);
                                if (r.score < resultGroup.score) {
                                    updated = true;
                                    resultLists.add(resultGroup);
                                    groupFound++;
                                    boolean overlapGroup = false;
                                    for (ResultGroup rG : resultLists) {
                                        if (rG.getMemberIdList().equals(currentGroup.getMemberIdList())) {
                                            resultLists.remove(rG);
                                            groupFound--;
//                                        System.out.println("hmmmm");
//                                        rG.showMembers();
                                            overlapGroup = true;
//                                        currentGroup.showMembers();
                                            break;
                                        }
                                    }
//                                groupFound++;
                                    if (!overlapGroup) {
                                        resultLists.remove(topK - 1);
                                        groupFound--;

                                    }
                                    Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                            resultGroup.getMaxDegree();
                                }

                            }
                            vRestSet.popMember(index);
                            totalCalled++;

                            RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                    vRestSet.getVisited(), vRestSet.maxMember);
                            newRestSet.markAllUnVisited();

                            IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getMaxFriend(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                            if (newRestSet.getSize() != 0) {
                                queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                        assistantClass.initialMaxDegree, updated,
                                        foundRestSet));

                            }

                            if (comb) {
                                vIntermediateSet.popMember(m, dMin);

                                if (vRestSet.getSize() != 0) {
                                    queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                            assistantClass.initialMaxDegree, assistantClass.updated,
                                            foundRestSet));

                                }

                            }

                            break;
                        } else {

//                        System.out.println("\n baap included hoini **************\n");
                            double terminatingDistance = s.getDistanceTermination(assistantClass.initialMaxDegree);
//                        System.out.println("terminating distance: " + terminatingDistance);

                            if (dMin > terminatingDistance) {
//                            System.out.println("termination due to distance upper bound");
//                            adP++;

                                break;
                            }
//                        System.out.println("upper distance for member: " + s.getUpperDistanceForMember());
                            if (dMin < s.getUpperDistanceForMember()) {
//                            System.out.println("distance lemma");

                                vIntermediateSet.addMember(m, dMin);
//                            System.out.println("before");
//                            vIntermediateSet.showMemberList();

                                ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                        vIntermediateSet.getTotalDistance(),
                                        vIntermediateSet.getSize(),
                                        vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                                int minCon = resultGroup.getMinDegree();
                                boolean isFeasibleGroup = true;
                                if (minCon < minimumConstraint) {
                                    isFeasibleGroup = false;
//                                vIntermediateSet.popMember(m, dMin);
//
////                            System.out.println("\n\n\n\n\n ekhane\n\n\n\n");
//                                //    resultGroup.getMaxDegree();
////                            System.out.println("famili bad member "+m.getMemberId());
//                                notFeas++;
//                                continue;
                                }
                                boolean updated = false;

                                if (isFeasibleGroup) {
                                    ResultGroup r = resultLists.get(topK - 1);
                                    if (r.score < resultGroup.score) {
                                        updated = true;
                                        resultLists.add(resultGroup);
                                        groupFound++;
                                        boolean overlapGroup = false;
                                        for (ResultGroup rG : resultLists) {
                                            if (rG.getMemberIdList().equals(currentGroup.getMemberIdList())) {
                                                resultLists.remove(rG);
                                                groupFound--;
//                                            System.out.println("hmmmm");
//                                            rG.showMembers();
                                                overlapGroup = true;
//                                            currentGroup.showMembers();
                                                break;
                                            }
                                        }
//                                    groupFound++;
                                        if (!overlapGroup) {
                                            resultLists.remove(topK - 1);
                                            groupFound--;
                                        }
                                        Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                                resultGroup.getMaxDegree();
                                    }
                                }


                                vRestSet.popMember(index);
                                totalCalled++;

                                RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                        vRestSet.getVisited(), vRestSet.maxMember);
                                newRestSet.markAllUnVisited();

                                IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                        vIntermediateSet.getMaxFriend(),
                                        vIntermediateSet.getTotalDistance(),
                                        vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                                if (newRestSet.getSize() != 0) {
                                    queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                            assistantClass.initialMaxDegree, updated,
                                            foundRestSet));

                                }

                                if (comb) {
                                    vIntermediateSet.popMember(m, dMin);

                                    if (vRestSet.getSize() != 0) {
                                        queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                                assistantClass.initialMaxDegree, assistantClass.updated,
                                                foundRestSet));

                                    }

                                }

                                break;
//                                continue;
                            }
                            double conLower = s.getLowerBoundOnConnection(dMin);

                            if ((float) 2 * newCon >= conLower) {
//                            System.out.println("connection Lemma");
//                            totalCalled++;

                                vIntermediateSet.addMember(m, dMin);
//                            System.out.println("before");
//                            vIntermediateSet.showMemberList();

                                ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                        vIntermediateSet.getTotalDistance(),
                                        vIntermediateSet.getSize(),
                                        vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                                int minCon = resultGroup.getMinDegree();
                                boolean isFeasibleGroup = true;
                                if (minCon < minimumConstraint) {
                                    isFeasibleGroup = false;
//                                vIntermediateSet.popMember(m, dMin);
//
////                            System.out.println("\n\n\n\n\n ekhane\n\n\n\n");
//                                //    resultGroup.getMaxDegree();
////                            System.out.println("famili bad member "+m.getMemberId());
//                                notFeas++;
//                                continue;
                                }
                                boolean updated = false;

                                if (isFeasibleGroup) {
                                    ResultGroup r = resultLists.get(topK - 1);
                                    if (r.score < resultGroup.score) {
                                        updated = true;
                                        resultLists.add(resultGroup);
                                        groupFound++;
                                        boolean overlapGroup = false;
                                        for (ResultGroup rG : resultLists) {
                                            if (rG.getMemberIdList().equals(currentGroup.getMemberIdList())) {
                                                resultLists.remove(rG);
                                                groupFound--;
//                                            System.out.println("hmmmm");
//                                            rG.showMembers();
                                                overlapGroup = true;
//                                            currentGroup.showMembers();
                                                break;
                                            }
                                        }
                                        if (!overlapGroup) {
                                            resultLists.remove(topK - 1);
                                            groupFound--;
                                        }
//                                    groupFound++;
                                        Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
                                        //                  resultGroup.getMaxDegree();
                                    }
                                }

                                vRestSet.popMember(index);
                                totalCalled++;

                                RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                        vRestSet.getVisited(), vRestSet.maxMember);
                                newRestSet.markAllUnVisited();

                                IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                        vIntermediateSet.getMaxFriend(),
                                        vIntermediateSet.getTotalDistance(),
                                        vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                                if (newRestSet.getSize() != 0) {
                                    queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                            assistantClass.initialMaxDegree, updated,
                                            foundRestSet));

                                }

                                if (comb) {
                                    vIntermediateSet.popMember(m, dMin);

                                    if (vRestSet.getSize() != 0) {
                                        queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                                assistantClass.initialMaxDegree, assistantClass.updated,
                                                foundRestSet));

                                    }

                                }

                                break;
                            } else {
//                                System.out.println("ekhane");
                            }
                        }

                    } else {
//                        System.out.println("familily bad member " + m.getMemberId());

                    }

                }
            }
        }

    }

    private static void saved_generateRankListApprox(PriorityQueue<AssistantClass> queue, ArrayList<Rectangle> rects, MeetingPointSet mpSet,
                                                     SpatialIndex siList, boolean comb) {
        while (queue.size() != 0) {
            AssistantClass assistantClass = queue.remove();
            Integer mpID = assistantClass.meetingPointId;
            IntermediateSet vIntermediateSet = assistantClass.intermediateSet;
            RestSet vRestSet = assistantClass.restSet;
            RestSet foundRestSet = assistantClass.foundRestSet;

            //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//            if (vIntermediateSet.getSize() + vRestSet.getSize() < minimumMember || vRestSet.getSize() == 0) {
//                continue;
//            }
            if (vIntermediateSet.getSize() >= maxGroupSize || vIntermediateSet.getSize() + vRestSet.getSize() < minimumMember) {
//                System.out.println("??");
//System.out.println("y");
                continue;

            }


//            System.out.println(queue.size());
            Score s = new Score(vIntermediateSet);
            boolean resultUpdated = false;

            Integer m = null;
            float dMin = 100;
            int index;
//            vIntermediateSet.showMemberList();
//            vRestSet.showMemberList();

            int size = vRestSet.getSize() + vIntermediateSet.getSize();
//        System.out.println(size);
//        System.out.println(vRestSet.isEmpty());
            //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//            while (size >= minimumMember && !vRestSet.isEmpty()) {
            while (vIntermediateSet.getSize() < maxGroupSize && !vRestSet.isEmpty()) {

                if (vRestSet.foundUnvisitedVertex()) {
                    m = vRestSet.topMember();
                    index = vRestSet.getLocalMemberIndex(m);
                    vRestSet.markVisited(index);
//                    System.out.println(mpID);

                    dMin = mpSet.getList().get(mpID).getMemberDistance(m);
//                System.out.println("member id: " + m.getMemberId() + "  distance : " + dMin + "           index:    " + index);

                } else {
//                System.out.println("ar member nai ");
                    break;
                }

                if (resultLists.size() == topK) {
                    ResultGroup r = resultLists.get(topK - 1);
                    int ttlGroupSize = vIntermediateSet.getGroupSize()
                            + vRestSet.getUnvisitedMemberCount() + 1;
                    int sz = vIntermediateSet.getSize();
                    boolean adTerminate = true;
                    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    for (int i = minimumMember; i <= maxGroupSize; i++) {

//                    for (int i = minimumMember; i <= ttlGroupSize; i++) {
                        if (sz >= i) {
                            continue;
                        }
                        double pruningDistance = s.advancePruningDistance(r.getTotalConnectivity(),
                                r.getGroupSize(), r.getTotalDistance(), assistantClass.initialMaxDegree,
                                i);
//                        System.out.println(pruningDistance);
//                        System.out.println(dMin * (i - vIntermediateSet.getSize()));

                        if (pruningDistance > (double) dMin * (i - vIntermediateSet.getSize())) {
//                        System.out.println("\n\n\n\ntermination advance");
//                        System.out.println((ttlGroupSize - vIntermediateSet.getSize()));
//                        //   System.out.println(m.getMemberId());
//                        System.out.println(pruningDistance);
//                        System.out.println((double) dMin * (ttlGroupSize - vIntermediateSet.getSize()));

                            adTerminate = false;
                            break;
                        }
                    }
                    if (adTerminate) {
//                    adP++;

//                        System.out.println("early termination for member:" + m.getMemberId());
                        break;

                    }

                }
                ResultGroup currentGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                        vIntermediateSet.getTotalDistance(),
                        vIntermediateSet.getSize(),
                        vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);


                if (vIntermediateSet.getSize() < minimumMember) {
//                    System.out.println("ki");
//                    System.out.print(m + " con " + dMin + "  ");
//                    vIntermediateSet.showMemberList();
//                    System.out.println(assistantClass.updated);
//                    System.out.println(mpID);

                    double newCon = vIntermediateSet.connectionIfAdded(m);

//                    double rightSide = minimumConstraint - minimumMember + 1 + vIntermediateSet.getSize();
                    double rightSide = (double) (minimumConstraint * vIntermediateSet.getSize()) / (minimumMember - 1.0);
//
////                    double rightSide = Math.floor((double) (minimumConstraint * vIntermediateSet.getSize()) / (minimumMember - 1.0));
                    if (newCon >= rightSide) {
                        vIntermediateSet.addMember(m, dMin);
//                    System.out.println("before");
//                    vIntermediateSet.showMemberList();
                        boolean updated = false;
                        if (vIntermediateSet.getSize() == minimumMember) {
                            ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getSize(),
                                    vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                            int minCon = resultGroup.getMinDegree();
                            if (minCon < minimumConstraint) {
                                vIntermediateSet.popMember(m, dMin);

//                            System.out.println("\n\n\n\n\n ekhane\n\n\n\n");
                                //    resultGroup.getMaxDegree();
//                            System.out.println("famili bad member "+m.getMemberId());
                                notFeas++;
                                continue;
                            }
                            if (groupFound < topK) {
                                groupFound++;
                                updated = true;
                                for (ResultGroup rG : resultLists) {
                                    if (rG.getMemberIdList().equals(currentGroup.getMemberIdList())) {
                                        resultLists.remove(rG);
                                        groupFound--;
//                                        System.out.println("hmmmm");
//                                        rG.showMembers();
//                                        currentGroup.showMembers();
                                        break;
                                    }
                                }

                                resultLists.add(resultGroup);
                                Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                            resultGroup.getMaxDegree();

//                            resultGroup.showConnection();
                            } else {
                                ResultGroup r = resultLists.get(topK - 1);
                                if (r.score < resultGroup.score) {
                                    updated = true;
                                    resultLists.add(resultGroup);
                                    groupFound++;
                                    boolean overlapGroup = false;
                                    for (ResultGroup rG : resultLists) {
                                        if (rG.getMemberIdList().equals(currentGroup.getMemberIdList())) {
                                            resultLists.remove(rG);
                                            groupFound--;
//                                            System.out.println("hmmmm");
//                                            rG.showMembers();
                                            overlapGroup = true;
//                                            currentGroup.showMembers();
                                            break;
                                        }
                                    }
//                                    groupFound++;
                                    if (!overlapGroup) {
                                        resultLists.remove(topK - 1);
                                        groupFound--;
                                    }
                                    Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                                resultGroup.getMaxDegree();
//                                resultGroup.showConnection();
                                }

                            }

                        }
                        vRestSet.popMember(index);
                        totalCalled++;

                        RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                vRestSet.getVisited(), vRestSet.maxMember);
                        newRestSet.markAllUnVisited();

                        IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                vIntermediateSet.getMaxFriend(),
                                vIntermediateSet.getTotalDistance(),
                                vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                        if (newRestSet.getSize() != 0) {
                            queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                    assistantClass.initialMaxDegree, updated,
                                    foundRestSet));

                        }

                        if (comb) {
                            vIntermediateSet.popMember(m, dMin);

                            if (vRestSet.getSize() != 0) {
                                queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                        assistantClass.initialMaxDegree, assistantClass.updated,
                                        foundRestSet));

                            }

                        }

                        break;
                    } else {
//                    System.out.println("bad");
                    }
                } else {
//                    System.out.print(m + " con " + dMin + "  ");
//                    vIntermediateSet.showMemberList();
//                    System.out.println(assistantClass.updated);
//                    System.out.println(mpID);

//                    System.out.println("hmmmmmmmmmmmmmmmmmmmmmmmm");
//                    System.out.print(m+" con ");
//                    vIntermediateSet.showMemberList();
//FOR PREVIOUS GROUP
//                System.out.println("over");
                    int newCon = vIntermediateSet.connectionIfAdded(m);
//                System.out.println(newCon);
//                System.out.println(minimumConstraint);

                    if (newCon >= minimumConstraint) {

                        //NO CHECKING FOR TERMINATION......
                        if (groupFound < topK) {
//                        System.out.println("\nmember kom\n");
                            vIntermediateSet.addMember(m, dMin);
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();

                            ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getSize(),
                                    vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);

                            groupFound++;

                            resultLists.add(resultGroup);
                            boolean overlapGroup = false;
                            for (ResultGroup rG : resultLists) {
                                if (rG.getMemberIdList().equals(currentGroup.getMemberIdList())) {
                                    resultLists.remove(rG);
                                    groupFound--;
//                                    System.out.println("hmmmm");
//                                    rG.showMembers();
//                                    overlapGroup = true;
//                                    currentGroup.showMembers();
                                    break;
                                }
                            }
                            Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                        resultGroup.getMaxDegree();
                            vRestSet.popMember(index);
                            totalCalled++;

                            RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                    vRestSet.getVisited(), vRestSet.maxMember);
                            newRestSet.markAllUnVisited();

                            IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getMaxFriend(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                            if (newRestSet.getSize() != 0) {
                                queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                        assistantClass.initialMaxDegree, true,
                                        foundRestSet));

                            }

                            if (comb) {
                                vIntermediateSet.popMember(m, dMin);

                                if (vRestSet.getSize() != 0) {
                                    queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                            assistantClass.initialMaxDegree, assistantClass.updated,
                                            foundRestSet));

                                }

                            }

                            break;
//                            continue;
                        }

                        if (assistantClass.updated) {
//                        System.out.println("\n baap included hoise\n");
                            vIntermediateSet.addMember(m, dMin);
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();

                            ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getSize(),
                                    vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                            ResultGroup r = resultLists.get(topK - 1);
                            boolean updated = false;
                            if (r.score < resultGroup.score) {
                                updated = true;
                                resultLists.add(resultGroup);
                                groupFound++;
                                boolean overlapGroup = false;
                                for (ResultGroup rG : resultLists) {
                                    if (rG.getMemberIdList().equals(currentGroup.getMemberIdList())) {
                                        resultLists.remove(rG);
                                        groupFound--;
//                                        System.out.println("hmmmm");
//                                        rG.showMembers();
                                        overlapGroup = true;
//                                        currentGroup.showMembers();
                                        break;
                                    }
                                }
//                                groupFound++;
                                if (!overlapGroup) {
                                    resultLists.remove(topK - 1);
                                    groupFound--;

                                }
                                Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                            resultGroup.getMaxDegree();
                            }
                            vRestSet.popMember(index);
                            totalCalled++;

                            RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                    vRestSet.getVisited(), vRestSet.maxMember);
                            newRestSet.markAllUnVisited();

                            IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getMaxFriend(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                            if (newRestSet.getSize() != 0) {
                                queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                        assistantClass.initialMaxDegree, updated,
                                        foundRestSet));

                            }

                            if (comb) {
                                vIntermediateSet.popMember(m, dMin);

                                if (vRestSet.getSize() != 0) {
                                    queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                            assistantClass.initialMaxDegree, assistantClass.updated,
                                            foundRestSet));

                                }

                            }

                            break;
                        } else {

//                        System.out.println("\n baap included hoini **************\n");
                            double terminatingDistance = s.getDistanceTermination(assistantClass.initialMaxDegree);
//                        System.out.println("terminating distance: " + terminatingDistance);

                            if (dMin > terminatingDistance) {
//                            System.out.println("termination due to distance upper bound");
//                            adP++;

                                break;
                            }
//                        System.out.println("upper distance for member: " + s.getUpperDistanceForMember());
                            if (dMin < s.getUpperDistanceForMember()) {
//                            System.out.println("distance lemma");

                                vIntermediateSet.addMember(m, dMin);
//                            System.out.println("before");
//                            vIntermediateSet.showMemberList();

                                ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                        vIntermediateSet.getTotalDistance(),
                                        vIntermediateSet.getSize(),
                                        vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                                ResultGroup r = resultLists.get(topK - 1);
                                boolean updated = false;
                                if (r.score < resultGroup.score) {
                                    updated = true;
                                    resultLists.add(resultGroup);
                                    groupFound++;
                                    boolean overlapGroup = false;
                                    for (ResultGroup rG : resultLists) {
                                        if (rG.getMemberIdList().equals(currentGroup.getMemberIdList())) {
                                            resultLists.remove(rG);
                                            groupFound--;
//                                            System.out.println("hmmmm");
//                                            rG.showMembers();
                                            overlapGroup = true;
//                                            currentGroup.showMembers();
                                            break;
                                        }
                                    }
//                                    groupFound++;
                                    if (!overlapGroup) {
                                        resultLists.remove(topK - 1);
                                        groupFound--;
                                    }
                                    Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                                resultGroup.getMaxDegree();
                                }
                                vRestSet.popMember(index);
                                totalCalled++;

                                RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                        vRestSet.getVisited(), vRestSet.maxMember);
                                newRestSet.markAllUnVisited();

                                IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                        vIntermediateSet.getMaxFriend(),
                                        vIntermediateSet.getTotalDistance(),
                                        vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                                if (newRestSet.getSize() != 0) {
                                    queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                            assistantClass.initialMaxDegree, updated,
                                            foundRestSet));

                                }

                                if (comb) {
                                    vIntermediateSet.popMember(m, dMin);

                                    if (vRestSet.getSize() != 0) {
                                        queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                                assistantClass.initialMaxDegree, assistantClass.updated,
                                                foundRestSet));

                                    }

                                }

                                break;
//                                continue;
                            }
                            double conLower = s.getLowerBoundOnConnection(dMin);

                            if ((float) 2 * newCon >= conLower) {
//                            System.out.println("connection Lemma");
//                            totalCalled++;

                                vIntermediateSet.addMember(m, dMin);
//                            System.out.println("before");
//                            vIntermediateSet.showMemberList();

                                ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                        vIntermediateSet.getTotalDistance(),
                                        vIntermediateSet.getSize(),
                                        vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                                ResultGroup r = resultLists.get(topK - 1);
                                boolean updated = false;
                                if (r.score < resultGroup.score) {
                                    updated = true;
                                    resultLists.add(resultGroup);
                                    groupFound++;
                                    boolean overlapGroup = false;
                                    for (ResultGroup rG : resultLists) {
                                        if (rG.getMemberIdList().equals(currentGroup.getMemberIdList())) {
                                            resultLists.remove(rG);
                                            groupFound--;
//                                            System.out.println("hmmmm");
//                                            rG.showMembers();
                                            overlapGroup = true;
//                                            currentGroup.showMembers();
                                            break;
                                        }
                                    }
                                    if (!overlapGroup) {
                                        resultLists.remove(topK - 1);
                                        groupFound--;
                                    }
//                                    groupFound++;
                                    Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
                                    //                  resultGroup.getMaxDegree();
                                }
                                vRestSet.popMember(index);
                                totalCalled++;

                                RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                        vRestSet.getVisited(), vRestSet.maxMember);
                                newRestSet.markAllUnVisited();

                                IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                        vIntermediateSet.getMaxFriend(),
                                        vIntermediateSet.getTotalDistance(),
                                        vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                                if (newRestSet.getSize() != 0) {
                                    queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                            assistantClass.initialMaxDegree, updated,
                                            foundRestSet));

                                }

                                if (comb) {
                                    vIntermediateSet.popMember(m, dMin);

                                    if (vRestSet.getSize() != 0) {
                                        queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                                assistantClass.initialMaxDegree, assistantClass.updated,
                                                foundRestSet));

                                    }

                                }

                                break;
                            } else {
//                                System.out.println("ekhane");
                            }
                        }

                    } else {
//                        System.out.println("familily bad member " + m.getMemberId());

                    }

                }
            }
        }

    }

    private static boolean retrieveNextMember(RestSet vRest, RestSet foundRestSet, MeetingPoint mp, MemberSet memberSet, ArrayList<Rectangle> rects,
                                              SpatialIndex siFast
    ) {
        final Point p = new Point(mp.getPosX(), mp.getPosY());

        int l = vRest.getSize();
//        int cnt = 0;
        ArrayList<Integer> memberId = new ArrayList<>();
        ArrayList<Integer> iContainliList = new ArrayList<>();
        siFast.nearestN(p, new TIntProcedure() {
            public boolean execute(int i) {
                //                  System.out.println("Rectangle " + i + " " + rects.get(i) + ", distance=" + rects.get(i).distance(p));
//                    if (vRest.getSize() > l) {
//                        return false;
//                    }
//                    siFast.delete(rects.get(i), i);

//                    System.out.println("retrievnig:::    " + memberSet.getList().get(i).getMemberId());
                memberId.add(memberSet.getList().get(i).getMemberId());
                iContainliList.add(i);
//                if (cnt > totalRetrievedMember) {
//                    foundRestSet.addMember(memberSet.getList().get(i).getMemberId());
//                    vRest.addMember(memberSet.getList().get(i).getMemberId());
//
//                    mp.addEntry(memberSet.getList().get(i).getMemberId(), rects.get(i).distance(p));
//
//
//                }
//                    System.out.println("called");
                return true;
            }

        }, foundRestSet.getSize() + 1, (float) maxDistance);
//        System.out.println(memberId.size() + "   " + foundRestSet.getSize());
        if (memberId.size() > foundRestSet.getSize()) {
            while (memberId.size() > foundRestSet.getSize()) {
                vRest.addMember(memberId.get(foundRestSet.getSize()));

                mp.addEntry(memberId.get(foundRestSet.getSize()), rects.get(iContainliList.get(foundRestSet.getSize())).distance(p));
                foundRestSet.addMember(memberId.get(foundRestSet.getSize()));

                totalRetrievedMember++;

            }

//            vRest.addMember(memberId.get(foundRestSet.getSize()));
//
//            mp.addEntry(memberId.get(foundRestSet.getSize()), rects.get(iContainliList.get(foundRestSet.getSize())).distance(p));
//            foundRestSet.addMember(memberId.get(foundRestSet.getSize()));
//
//            totalRetrievedMember++;
//            System.out.println(totalRetrievedMember);
            return true;
        }

//        if (vRest.getSize() > l) {
////            System.out.println("ota: " + totalRetrievedMember);
//            totalRetrievedMember += vRest.getSize() - l;
//            return true;
//        }
//        else {
//            if (groupFound < topK) {
////                System.out.println("kahini hoise");
//                maxDistance =maxDistance+maxDistance;
//                if(retrieveNextMember(vRest, foundRestSet, p, memberSet, rects, siFast)){
//                    return true;
//                }
//            }
//            return false;
//
//        }
        return false;

    }

    static void insertMeetingPoints(MeetingPointSet mps) {

        String csvFile = "";
        if (dataset == 0) {
            csvFile = "meetingPoints.csv";
        } else {
            csvFile = "producedMeetingPoint.csv";
        }
//        String csvFile = "meetingPoints.csv";
//        String csvFile = "modifiedMeetingPOints.csv";
//        String csvFile = "GowallaMeetingPoint.csv";
//        String csvFile = "checkMeet.csv";
//        String csvFile = "producedMeetingPoint.csv";

        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        int cnt = 0;
        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                // use comma as separator
//                System.out.println(line);
                String[] strings = line.split(cvsSplitBy);

                mps.addMeetingPOint(new MeetingPoint(Float.parseFloat(strings[0]), Float.parseFloat(strings[1]), cnt));

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

//        mps.showMeetingPoints();
    }


    private static void insertMember(MemberSet memberSet) {
        String csvFile = "";
        if (dataset == 0) {
            csvFile = "member - Copy.csv";
        }
        if (dataset == 1) {
            csvFile = "Brightkite_location.csv";
        }
        if (dataset == 2) {
            csvFile = "Gowalla_location.csv";
        }
        if (dataset == 3) {
            csvFile = "10M_location.txt";
        }

//        String csvFile = "Brightkite_location.csv";
//          String csvFile = "Gowalla_location.csv";
//        String csvFile = "10M_location.txt";
//
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        int cnt = 0;
        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] strings = line.split(cvsSplitBy);
                memberSet.addMember(Float.parseFloat(strings[0]), Float.parseFloat(strings[1]), cnt);
                cnt++;
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

//        memberSet.showMembers();
//        System.out.println(memberSet.getMemberSetSize());
    }

    private static void insertGraphConnection(MemberSet memberSet) {
//        String csvFile = "connection - Copy.csv";
//        String csvFile = "brightKiteCOnnection.csv";
//        String csvFile = "GowallaCOnnection.csv";
//        String csvFile = "10M_user.txt";

        String csvFile = "";
        if (dataset == 0) {
            csvFile = "connection - Copy.csv";
        }
        if (dataset == 1) {
            csvFile = "brightKiteCOnnection.csv";
        }
        if (dataset == 2) {
            csvFile = "GowallaCOnnection.csv";
        }
        if (dataset == 3) {
            csvFile = "10M_user.txt";
        }
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        int cnt = 0;
        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] strings = line.split(cvsSplitBy);
                for (String s : strings) {
                    int cnt2 = Integer.parseInt(s.trim()) - 1;
                    memberSet.addConnection(cnt, cnt2);
//                    edges.add(new Edge(cnt, cnt2));
                }
                cnt++;
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

//        memberSet.showGraphConnection();
    }

    public static <T> T mostCommon(List<T> list) {
        Map<T, Integer> map = new HashMap<>();

        for (T t : list) {
            Integer val = map.get(t);
            map.put(t, val == null ? 1 : val + 1);
        }

        Entry<T, Integer> max = null;

        for (Entry<T, Integer> e : map.entrySet()) {
            if (max == null || e.getValue() > max.getValue()) {
                max = e;
            }
        }

        return max.getKey();

    }

    private static void changeParameter(float get, int fuk) {


        switch (fuk) {
            case 1:
                minimumMember = (int) get;
//                minimumConstraint = (int) Math.ceil(get / 2.0);
                System.out.println("changing min member to: " + minimumMember);
//                System.out.println("set min  constraint to: " + minimumConstraint);

                break;
            case 2:
                minimumConstraint = (int) get;
                System.out.println("changing min constraint to: " + get);
                break;
            case 3:
                maxDistance = get;
                System.out.println("changing max distance to: " + get);
                break;
            case 4:
                topK = (int) get;
                System.out.println("changing  top k to: " + get);
                break;
            case 5:
                totalMeetingPOint = (int) get;
                System.out.println("changing mp total to: " + get);
                break;

            case 6:
//                totalMeetingPOint = (int) get;
                Score.alpha = .5;
                Score.beta = .25;
                Score.gamma = .25;
                System.out.println("changing alpha to: .5");
                break;
            case 7:
//                totalMeetingPOint = (int) get;
                Score.alpha = .25;
                Score.beta = .5;
                Score.gamma = .25;

                System.out.println("changing beta to: .5");
                break;
            case 8:

                Score.alpha = .25;
                Score.beta = .25;
                Score.gamma = .5;

//                totalMeetingPOint = (int) get;
                System.out.println("changing gamma to: .5");
                break;
            case 9:
//                totalMeetingPOint = (int) get;
                Score.alpha = .75;
                Score.beta = .125;
                Score.gamma = .125;
                System.out.println("changing alpha to: .75");
                break;
            case 10:
//                totalMeetingPOint = (int) get;
                Score.alpha = .125;
                Score.beta = .75;
                Score.gamma = .125;

                System.out.println("changing beta to: .75");
                break;
            case 11:

                Score.alpha = .125;
                Score.beta = .125;
                Score.gamma = .75;

//                totalMeetingPOint = (int) get;
                System.out.println("changing gamma to: .75");
                break;
            case 12:
//                totalMeetingPOint = (int) get;
                Score.alpha = 1;
                Score.beta = .0;
                Score.gamma = .0;
                System.out.println("changing alpha to: 1");
                break;
            case 13:
//                totalMeetingPOint = (int) get;
                Score.alpha = .0;
                Score.beta = 1;
                Score.gamma = .0;

                System.out.println("changing beta to: 1");
                break;
            case 14:

                Score.alpha = .0;
                Score.beta = .0;
                Score.gamma = 1;

//                totalMeetingPOint = (int) get;
                System.out.println("changing gamma to: 1");
                break;
            case 15:
                maxGroupSize = (int) get;
                System.out.println("changing max group size to: " + get);
                break;


        }
        if (fuk != 15) {
            maxGroupSize = 3 * minimumMember;
            maxGroupSize = (int) Math.floor((1.0 * maxGroupSize) / 2.0)-1;   // we normally assume that maxgroup size is 1.5 times of minimum group szie
            System.out.println(maxGroupSize);
        }

    }

    private static void computeNoOfTriangles(ArrayList<Integer> firstMember, ArrayList<Integer> secondMember, ArrayList<Integer> noOfTringles, IntermediateSet vIntermediateSet) {
        int l = firstMember.size();
        for (int i = 0; i < l; i++) {
            Member m1 = memberSet.getList().get(firstMember.get(i));
            Member m2 = memberSet.getList().get(secondMember.get(i));
            for (int friend : m1.getFriendList()) {
                if (vIntermediateSet.getMemberList().contains(friend) && m2.getFriendList().contains(friend)) {
                    noOfTringles.set(i, noOfTringles.get(i) + 1);
                }
            }

            for (int friend : m1.getFriendList()) {
                System.out.print(" " + friend);
            }
            System.out.println("");

            for (int friend : m2.getFriendList()) {
                System.out.print(" " + friend);
            }
            System.out.println("");
            System.out.println(firstMember.get(i) + "  " + secondMember.get(i) + "  " + noOfTringles.get(i));

        }
    }

    private static void preprocessKTruss(ArrayList<Integer> firstMember, ArrayList<Integer> secondMember, ArrayList<Integer> noOfTringles, IntermediateSet vIntermediateSet) {
        System.out.println("hm");
        vIntermediateSet.showMemberList();

        for (int member : vIntermediateSet.getMemberList()) {
            System.out.println("member: " + member);
            for (int friend : memberSet.getList().get(member).getFriendList()) {
                System.out.print(" " + friend);
                if (firstMember.contains(friend) && secondMember.contains(member)) {
                    continue;
                }
                if (vIntermediateSet.getMemberList().contains(friend)) {
                    firstMember.add(member);
                    secondMember.add(friend);
                    noOfTringles.add(0);
                }
            }
            System.out.println("");
        }

        for (int i = 0; i < firstMember.size(); i++) {
            System.out.println(firstMember.get(i) + "  " + secondMember.get(i));
        }

    }

    private static class Location {

        float longitude;
        float latitude;

        public Location(float longitude, float latitude) {
            this.longitude = longitude;
            this.latitude = latitude;
        }

        public void show() {
            System.out.println(longitude + "  " + latitude);
        }

        public float getLongitude() {
            return longitude;
        }

        public float getLatitude() {
            return latitude;
        }

    }

    static ArrayList<Integer> vertexNoLocation = new ArrayList<>();

    private static void insertBrightKiteMember(MemberSet memberSet) {

        List<Location> list = new ArrayList<>();

        String csvFile = "Gowalla_totalCheckins.txt";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = "\\s+";
        int cnt = 0;
        boolean newMember = false;
        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] strings = line.split(cvsSplitBy);
//                memberSet.addMember(Float.parseFloat(strings[0]), Float.parseFloat(strings[1]), cnt);
//                if (cnt > 2) {
//                    break;
//                }
                int l = Integer.parseInt(strings[0]);
                if (l > cnt) {
//                        mostCommon(list).show();
                    Location location = mostCommon(list);

                    list.clear();

                    for (int i = 0; i < l - cnt; i++) {
                        memberSet.addMember(location.getLongitude(), location.getLatitude(), cnt + i);

                    }
                    cnt = l;

                }

                try {
                    if (strings.length >= 3) {
                        list.add(new Location(Float.parseFloat(strings[2]), Float.parseFloat(strings[3])));

                    } else {
                        System.out.println(cnt);
                        for (String s : strings) {
                            System.out.println(s);
                        }
                        System.out.println("");
                    }

                } catch (Exception e) {
                    for (String s : strings) {
                        System.err.println(s);
                    }
                }

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        Location location = mostCommon(list);

        memberSet.addMember(location.getLongitude(), location.getLatitude(), cnt);

        System.out.println(vertexNoLocation.size());
        System.out.println(memberSet.getMemberSetSize());

        PrintWriter pw;
        try {
            pw = new PrintWriter(new File("Gowalla_location.csv"));
            for (Member m : memberSet.getList()) {
                StringBuilder sb = new StringBuilder();

                sb.append(m.getPosX());
                sb.append(',');
                sb.append(m.getPosY());
                sb.append(',');
                sb.append(m.getMemberId());

                sb.append('\n');

                pw.write(sb.toString());

            }
            pw.close();

        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReducedTokKSSQ.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        try {
            pw = new PrintWriter(new File("vertexNoLocation.csv"));
            for (Integer i : vertexNoLocation) {
                StringBuilder sb = new StringBuilder();

                sb.append(i);
                sb.append('\n');

                pw.write(sb.toString());

            }
            pw.close();

        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReducedTokKSSQ.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("done!");
    }

    private static void insertBrightKiteGraphConnection(MemberSet memberSet) {
        String csvFile = "Gowalla_edges.txt";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = "\\s+";
        int cnt = 0;
        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                String[] strings = line.split(cvsSplitBy);
                if (Integer.parseInt(strings[0]) < memberSet.getMemberSetSize() && Integer.parseInt(strings[1]) < memberSet.getMemberSetSize()) {
                    memberSet.addConnection(Integer.parseInt(strings[0]), Integer.parseInt(strings[1]));
                    cnt++;

                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        PrintWriter pw;
//        try {
//            pw = new PrintWriter(new File("E:/_thesis/Data/Brightkite/Brightkite_location.csv"));
//            for (Member m : memberSet.getList()) {
//                StringBuilder sb = new StringBuilder();
//
//                sb.append(m.getPosX());
//                sb.append(',');
//                sb.append(m.getPosY());
//                sb.append(',');
//                sb.append(m.getMemberId());
//
//                sb.append('\n');
//
//                pw.write(sb.toString());
//
//            }
//            pw.close();
//
//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(SSTKQueueModified.class
//                    .getName()).log(Level.SEVERE, null, ex);
//        }
        try {
            pw = new PrintWriter(new File("GowallaCOnnection.csv"));
            for (Member m : memberSet.getList()) {
                StringBuilder sb = new StringBuilder();

                int l = m.getFriendList().size();
                for (int i = 0; i < l; i++) {
                    sb.append(m.getFriendList().get(i));
                    if (i == l - 1) {
                        break;
                    }
                    sb.append(',');
                }

                sb.append("\n");
                pw.write(sb.toString());

            }

            pw.close();

        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReducedTokKSSQ.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    static void insertRandomMeetingPoints(MeetingPointSet mps, int div, float randFactor) {

        float maxLongitude;
        float minLongitude;
        float maxLatitude;
        float minLatitude;


        if (dataset == 3) {
//        twitter
            maxLatitude = 90;
            minLatitude = -90;
            maxLongitude = 180;
            minLongitude = -180;

            for (int i = 0; i < div; i++) {
                float posX = minLatitude + i * ((float) (maxLatitude - minLatitude) / div);
                float posY = minLongitude + i * ((float) (maxLongitude - minLongitude) / div);
//            System.out.println("\n");
//            System.out.println(posX);
//            System.out.println(posY);
                mps.addMeetingPOint(new MeetingPoint(posX, posY, i));

            }
        }

//        gowalla

        if (dataset == 2) {
            div = div / 7;
            maxLatitude = 55;
            minLatitude = 15;
            maxLongitude = -50;
            minLongitude = -120;

            for (int i = 0; i < div; i++) {
                float posX = minLatitude + i * ((float) (maxLatitude - minLatitude) / div);
                float posY = minLongitude + i * ((float) (maxLongitude - minLongitude) / div);
//            System.out.println("\n");
//            System.out.println(posX);
//            System.out.println(posY);
                mps.addMeetingPOint(new MeetingPoint(posX, posY, i));

            }

//        maxLatitude = 75;
//        minLatitude = 35;
//        maxLongitude = 30;
//        minLongitude = -20;
//
//        for (int i = 0; i < div; i++) {
//            float posX = minLatitude + i * ((float) (maxLatitude - minLatitude) / div);
//            float posY = minLongitude + i * ((float) (maxLongitude - minLongitude) / div);
////            System.out.println("\n");
////            System.out.println(posX);
////            System.out.println(posY);
//            mps.addMeetingPOint(new MeetingPoint(posX, posY, i));
//
//        }
//      problem ase eitate
            maxLatitude = 20;
            minLatitude = -8;
            maxLongitude = 110;
            minLongitude = 90;

            for (int i = 0; i < div; i++) {
                float posX = minLatitude + i * ((float) (maxLatitude - minLatitude) / div);
                float posY = minLongitude + i * ((float) (maxLongitude - minLongitude) / div);
//            System.out.println("\n");
//            System.out.println(posX);
//            System.out.println(posY);
                mps.addMeetingPOint(new MeetingPoint(posX, posY, i));

            }

//        not so good
            maxLatitude = 50;
            minLatitude = 20;
            maxLongitude = 130;
            minLongitude = 90;

            for (int i = 0; i < div; i++) {
                float posX = minLatitude + i * ((float) (maxLatitude - minLatitude) / div);
                float posY = minLongitude + i * ((float) (maxLongitude - minLongitude) / div);
//            System.out.println("\n");
//            System.out.println(posX);
//            System.out.println(posY);
                mps.addMeetingPOint(new MeetingPoint(posX, posY, i));

            }

            maxLatitude = -15;
            minLatitude = -45;
            maxLongitude = 155;
            minLongitude = 135;

            for (int i = 0; i < div; i++) {
                float posX = minLatitude + i * ((float) (maxLatitude - minLatitude) / div);
                float posY = minLongitude + i * ((float) (maxLongitude - minLongitude) / div);
//            System.out.println("\n");
//            System.out.println(posX);
//            System.out.println(posY);
                mps.addMeetingPOint(new MeetingPoint(posX, posY, i));

            }

//eita valo......................
            maxLatitude = -10;
            minLatitude = -40;
            maxLongitude = -40;
            minLongitude = -80;

            for (int i = 0; i < div; i++) {
                float posX = minLatitude + i * ((float) (maxLatitude - minLatitude) / div);
                float posY = minLongitude + i * ((float) (maxLongitude - minLongitude) / div);
//            System.out.println("\n");
//            System.out.println(posX);
//            System.out.println(posY);
                mps.addMeetingPOint(new MeetingPoint(posX, posY, i));

            }

            maxLatitude = 25;
            minLatitude = 2;
            maxLongitude = -60;
            minLongitude = -120;

            for (int i = 0; i < div; i++) {
                float posX = minLatitude + i * ((float) (maxLatitude - minLatitude) / div);
                float posY = minLongitude + i * ((float) (maxLongitude - minLongitude) / div);
//            System.out.println("\n");
//            System.out.println(posX);
//            System.out.println(posY);
                mps.addMeetingPOint(new MeetingPoint(posX, posY, i));

            }

//      kharap
            maxLatitude = 30;
            minLatitude = 10;
            maxLongitude = 60;
            minLongitude = 35;

            for (int i = 0; i < div; i++) {
                float posX = minLatitude + i * ((float) (maxLatitude - minLatitude) / div);
                float posY = minLongitude + i * ((float) (maxLongitude - minLongitude) / div);
//            System.out.println("\n");
//            System.out.println(posX);
//            System.out.println(posY);
                mps.addMeetingPOint(new MeetingPoint(posX, posY, i));


            }
        }

//        }


//////  brighitkite
        if (dataset == 1) {
            div = div / 5;

            maxLongitude = -50;
            minLongitude = -160;
            maxLatitude = 60;
            minLatitude = 5;
            for (int i = 0; i < div; i++) {
                float posX = minLatitude + i * ((float) (maxLatitude - minLatitude) / div);
                float posY = minLongitude + i * ((float) (maxLongitude - minLongitude) / div);
//            System.out.println("\n");
//            System.out.println(posX);
//            System.out.println(posY);
                mps.addMeetingPOint(new MeetingPoint(posX, posY, i));

            }

            maxLatitude = -15;
            minLatitude = -55;
            maxLongitude = 180;
            minLongitude = 90;
            for (int i = 0; i < div; i++) {
                float posX = minLatitude + i * ((float) (maxLatitude - minLatitude) / div);
                float posY = minLongitude + i * ((float) (maxLongitude - minLongitude) / div);
//            System.out.println("\n");
//            System.out.println(posX);
//            System.out.println(posY);
                mps.addMeetingPOint(new MeetingPoint(posX, posY, i));

            }
            maxLatitude = 50;
            minLatitude = -15;
            maxLongitude = 150;
            minLongitude = 60;

            for (int i = 0; i < div; i++) {
                float posX = minLatitude + i * ((float) (maxLatitude - minLatitude) / div);
                float posY = minLongitude + i * ((float) (maxLongitude - minLongitude) / div);
//            System.out.println("\n");
//            System.out.println(posX);
//            System.out.println(posY);
                mps.addMeetingPOint(new MeetingPoint(posX, posY, i));

            }
            maxLatitude = 40;
            minLatitude = -5;
            maxLongitude = 80;
            minLongitude = -20;

            for (int i = 0; i < div; i++) {
                float posX = minLatitude + i * ((float) (maxLatitude - minLatitude) / div);
                float posY = minLongitude + i * ((float) (maxLongitude - minLongitude) / div);
//            System.out.println("\n");
//            System.out.println(posX);
//            System.out.println(posY);
                mps.addMeetingPOint(new MeetingPoint(posX, posY, i));

            }
            maxLongitude = 60;
            minLongitude = -10;
            maxLatitude = 80;
            minLatitude = 5;
            for (int i = 0; i < div; i++) {
                float posX = minLatitude + i * ((float) (maxLatitude - minLatitude) / div);
                float posY = minLongitude + i * ((float) (maxLongitude - minLongitude) / div);
//            System.out.println("\n");
//            System.out.println(posX);
//            System.out.println(posY);
                mps.addMeetingPOint(new MeetingPoint(posX, posY, i));

            }

        }


////normal
////        maxLongitude = 10;
////        minLongitude = 0;
////        maxLatitude = 10;
////        minLatitude = 0;
////
////        for (int i = 0; i < div; i++) {
////            float posX = minLatitude + i * ((float) (maxLatitude - minLatitude) / div);
////            float posY = minLongitude + i * ((float) (maxLongitude - minLongitude) / div);
//////            System.out.println("\n");
//////            System.out.println(posX);
//////            System.out.println(posY);
////            mps.addMeetingPOint(new MeetingPoint(posX, posY, i));
////        }
    }

    private static class AssistantClass {

        RestSet restSet;
        IntermediateSet intermediateSet;
        float minDistance;
        Integer meetingPointId;
        int initialMaxDegree;
        boolean updated;
        RestSet foundRestSet;

        public AssistantClass(RestSet restSet, IntermediateSet intermediateSet, float minDistance, Integer mpId,
                              int initialMaxDegree, boolean updated, RestSet foundRestSet
        ) {
            this.restSet = restSet;
            this.intermediateSet = intermediateSet;
            this.minDistance = minDistance;
            this.meetingPointId = mpId;
            this.initialMaxDegree = initialMaxDegree;
            this.updated = updated;
            this.foundRestSet = foundRestSet;
        }

        Integer getminDistance() {
            return (int) (minDistance * 10000000);
        }

        Integer getSize() {
            return (int) restSet.getSize();
        }

    }


    private static void makeResultIncremental(MemberSet memberSet, MeetingPointSet mPointSet, boolean comb) {

        System.out.println("make result incremental");

        totalRetrievedMember = 0;
        int it = totalMeetingPOint;
        long start = System.currentTimeMillis();
        long ta = 0, tb = 0;
        PriorityQueue<AssistantClass> as = new PriorityQueue<>(queueMem, (a, b) -> a.getminDistance().compareTo(b.getminDistance()));
        int totalMemberConsidered = 0;
        MeetingPointSet workingMeetingPointSet = new MeetingPointSet();
        ArrayList<Rectangle> rects = new ArrayList<>();

        SpatialIndex si = new RTree();
        si.init(null);
        int cnt = 0;
        for (Member m : memberSet.getList()) {
            Rectangle r = new Rectangle(m.getPosX(), m.getPosY(), m.getPosX(), m.getPosY());
            rects.add(r);
            si.add(r, cnt);
            cnt++;
        }
//        SpatialIndex siList[] = new SpatialIndex[it];

        for (int q = 0; q < it; q++) {

            RestSet vRest = new RestSet();
            MeetingPoint mp = new MeetingPoint(mPointSet.getList().get(rand[q]).getPosX(), mPointSet.getList().get(rand[q]).getPosY(), rand[q]);

            final Point p = new Point(mPointSet.getList().get(rand[q]).getPosX(), mPointSet.getList().get(rand[q]).getPosY());

            IntermediateSet vIntermediateSet = new IntermediateSet();
            RestSet foundRestSet = new RestSet();

            retrieveNextMember(vRest, foundRestSet, mp, memberSet, rects, si);
//

//            System.out.print(q + "  ");
//            System.out.println(vRest.getSize());
            ta = System.currentTimeMillis();

            tb = System.currentTimeMillis();

            workingMeetingPointSet.addMeetingPOint(mp);

            if (vRest.getSize() != 0) {

                as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
                        -1, true,
                        foundRestSet));

            }
//            si=null;
//            System.out.print(q + "  ");
//            System.out.println(vRest.getSize());
        }

        totalCalled = 0;
//        totalRetrievedMember = 0;
        generateRankListIncremental(as, rects, workingMeetingPointSet, si, comb);

        as.clear();
        totalitBaseline[presentSampleIndex] += totalCalled;
        retrievedMemberBaseline[presentSampleIndex] += totalRetrievedMember;

        long now = System.currentTimeMillis();
        int ccnt = 0;
//        for (ResultGroup r : resultLists) {
//            ccnt++;
//            System.out.println("\n" + ccnt + "  \n" + r.score);
//            r.showMembers();
////            System.out.println(r.getMp().posX + "  " + r.getMp().posY);
//        }
        totalTimeBaseline[presentSampleIndex] += tb - ta + now - start;
        System.out.print(c + "\t");
        System.out.println(now - start);
        System.out.println("totalRetrieved   " + totalRetrievedMember);

    }

    private static void makeResultApproxIncremental(MemberSet memberSet, MeetingPointSet mPointSet, boolean comb) {

        System.out.println("make result approx incremental");
        totalRetrievedMember = 0;
        int it = totalMeetingPOint;
        long start = System.currentTimeMillis();
        long ta = 0, tb = 0;
        PriorityQueue<AssistantClass> as = new PriorityQueue<>(queueMem, (a, b) -> a.getminDistance().compareTo(b.getminDistance()));
        int totalMemberConsidered = 0;
        MeetingPointSet workingMeetingPointSet = new MeetingPointSet();
        ArrayList<Rectangle> rects = new ArrayList<>();

        SpatialIndex si = new RTree();
        si.init(null);
        int cnt = 0;
        for (Member m : memberSet.getList()) {
            Rectangle r = new Rectangle(m.getPosX(), m.getPosY(), m.getPosX(), m.getPosY());
            rects.add(r);
            si.add(r, cnt);
            cnt++;
        }
//        SpatialIndex siList[] = new SpatialIndex[it];

        for (int q = 0; q < it; q++) {

            RestSet vRest = new RestSet();
            MeetingPoint mp = new MeetingPoint(mPointSet.getList().get(rand[q]).getPosX(), mPointSet.getList().get(rand[q]).getPosY(), rand[q]);

            final Point p = new Point(mPointSet.getList().get(rand[q]).getPosX(), mPointSet.getList().get(rand[q]).getPosY());

            IntermediateSet vIntermediateSet = new IntermediateSet();
            RestSet foundRestSet = new RestSet();

            retrieveNextMember(vRest, foundRestSet, mp, memberSet, rects, si);
//

//            System.out.print(q + "  ");
//            System.out.println(vRest.getSize());
            ta = System.currentTimeMillis();

            tb = System.currentTimeMillis();

            workingMeetingPointSet.addMeetingPOint(mp);

            if (vRest.getSize() != 0) {

                as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
                        -1, true,
                        foundRestSet));

            }
//            si=null;
//            System.out.print(q + "  ");
//            System.out.println(vRest.getSize());
        }

        totalCalled = 0;
//        totalRetrievedMember = 0;
        generateRankListApproxIncremental(as, rects, workingMeetingPointSet, si, comb);

        as.clear();
        retrievedMemberGreedy[presentSampleIndex] += totalRetrievedMember;
        totalItGreedy[presentSampleIndex] += totalCalled;

        long now = System.currentTimeMillis();
        int ccnt = 0;
//        for (ResultGroup r : resultLists) {
//            ccnt++;
//            System.out.println("\n" + ccnt + "  \n" + r.score);
//            r.showMembers();
//            System.out.println(r.getMp());
//        }
        totalTimeGreedy[presentSampleIndex] += tb - ta + now - start;
        System.out.print(c + "\t");
        System.out.println(now - start);
        System.out.println("totalRetrieved   " + totalRetrievedMember);

    }


    private static boolean generateKcore
            (PriorityQueue<AssistantClass> queue, ArrayList<Rectangle> rects, MeetingPointSet mpSet,
             SpatialIndex siList, boolean comb, long start) {
        System.out.println(queue.size());
        System.out.println("problem");
        while (queue.size() != 0) {
//            System.out.println(queue.size());
            long now = System.currentTimeMillis();
            if (now - start > 1000000) {
                System.out.println("hm");
                return false;
            }
            AssistantClass assistantClass = queue.remove();
            Integer mpID = assistantClass.meetingPointId;
            IntermediateSet vIntermediateSet = assistantClass.intermediateSet;
            RestSet vRestSet = assistantClass.restSet;
            RestSet foundRestSet = assistantClass.foundRestSet;
            if (vIntermediateSet.getSize() >= maxGroupSize) {
                continue;

            }
            Score s = new Score(vIntermediateSet);
            boolean resultUpdated = false;

            Integer m = null;
            float dMin = 100;
            int index = 0;
            int size = vRestSet.getSize() + vIntermediateSet.getSize();
            while (vIntermediateSet.getSize() < maxGroupSize && !vRestSet.isEmpty()) {

                if (vRestSet.foundUnvisitedVertex()) {
                    m = vRestSet.topMember();
                    index = vRestSet.getLocalMemberIndex(m);
                    vRestSet.markVisited(index);

                    dMin = mpSet.getList().get(mpID).getMemberDistance(m);
                } else {
                    break;
                }
//                System.out.println(m);
                vIntermediateSet.addMember(m, dMin);

                boolean updated = false;
                ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                        vIntermediateSet.getTotalDistance(),
                        vIntermediateSet.getSize(),
                        vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                int minCon = resultGroup.getMinDegree();

                if (vIntermediateSet.getSize() == minimumMember && minCon < minimumConstraint) {

                    vIntermediateSet.popMember(m, dMin);
                    continue;
                }

                if (minCon >= minimumConstraint) {
//                    vIntermediateSet.popMember(m, dMin);
                    if (groupFound < topK) {
                        groupFound++;
                        updated = true;
                        resultLists.add(resultGroup);
                        Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
                    } else {
                        ResultGroup r = resultLists.get(topK - 1);
                        if (r.score < resultGroup.score) {
                            updated = true;
                            resultLists.remove(topK - 1);
                            resultLists.add(resultGroup);
                            Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
                        }

                    }

                }

                vRestSet.popMember(index);
                totalCalled++;
                RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                        vRestSet.getVisited(), vRestSet.maxMember);
                newRestSet.markAllUnVisited();

                IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                        vIntermediateSet.getMaxFriend(),
                        vIntermediateSet.getTotalDistance(),
                        vIntermediateSet.getTotalConnectivity());

                if (newRestSet.getSize() != 0) {
                    queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                            assistantClass.initialMaxDegree, false,
                            foundRestSet));

                }

                if (comb) {
                    vIntermediateSet.popMember(m, dMin);

                    if (vRestSet.getSize() != 0) {
                        queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                assistantClass.initialMaxDegree, false,
                                foundRestSet));

                    }

                }

                break;

            }

        }

        return true;
    }

    private static boolean generateKTruss
            (PriorityQueue<AssistantClass> queue, ArrayList<Rectangle> rects, MeetingPointSet mpSet,
             SpatialIndex siList, boolean comb, long start) {
        System.out.println(queue.size());
        while (queue.size() != 0) {
//            System.out.println(queue.size());
            long now = System.currentTimeMillis();
            if (now - start > 1000000) {
                System.out.println("hm");
                return false;
            }
            AssistantClass assistantClass = queue.remove();
            Integer mpID = assistantClass.meetingPointId;
            IntermediateSet vIntermediateSet = assistantClass.intermediateSet;
            RestSet vRestSet = assistantClass.restSet;
            RestSet foundRestSet = assistantClass.foundRestSet;
            if (vIntermediateSet.getSize() >= maxGroupSize) {
                continue;

            }
            Score s = new Score(vIntermediateSet);
            boolean resultUpdated = false;

            Integer m = null;
            float dMin = 100;
            int index = 0;
            int size = vRestSet.getSize() + vIntermediateSet.getSize();
            while (vIntermediateSet.getSize() < maxGroupSize && !vRestSet.isEmpty()) {

                if (vRestSet.foundUnvisitedVertex()) {
                    m = vRestSet.topMember();
                    index = vRestSet.getLocalMemberIndex(m);
                    vRestSet.markVisited(index);

                    dMin = mpSet.getList().get(mpID).getMemberDistance(m);
                } else {
                    break;
                }
//                System.out.println(m);
                vIntermediateSet.addMember(m, dMin);
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//
//                ArrayList<Integer> firstMember = new ArrayList<>();
//                ArrayList<Integer> secondMember = new ArrayList<>();
//                ArrayList<Integer> noOfTriangles = new ArrayList<>();
//                preprocessKTruss(firstMember, secondMember, noOfTriangles, vIntermediateSet);
//                while (true) {
//                    computeNoOfTriangles(firstMember, secondMember, noOfTriangles, vIntermediateSet);
//                    int totalEdges = firstMember.size();
//                    ArrayList<Integer> disIntegers = new ArrayList<>();
//                    for (int i = 0; i < totalEdges; i++) {
//                        if (noOfTriangles.get(i) < minimumConstraint) {
//                            disIntegers.add(firstMember.get(i));
//                            disIntegers.add(secondMember.get(i));
//                            firstMember.remove(i);
//                            secondMember.remove(i);
//                            noOfTriangles.remove(i);
//                            totalEdges--;
//                            i--;
//
//                        }
//                    }
//                    if (disIntegers.isEmpty()) {
//                        break;
//                    }
//                    for (Integer dInteger : disIntegers) {
//                        totalEdges = firstMember.size();
//                        for (int i = 0; i < totalEdges; i++) {
//                            if (firstMember.get(i) == dInteger || secondMember.get(i) == dInteger) {
//                                firstMember.remove(i);
//                                secondMember.remove(i);
//                                noOfTriangles.remove(i);
//                                 totalEdges--;
//                                i--;
//
//                            }
//                        }
//                    }
//
//                }
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                boolean updated = false;
                ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                        vIntermediateSet.getTotalDistance(),
                        vIntermediateSet.getSize(),
                        vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                int minCon = resultGroup.getMinDegree();

                if (minCon >= minimumConstraint) {
//                    vIntermediateSet.popMember(m, dMin);
                    if (groupFound < topK) {
                        groupFound++;
                        updated = true;
                        resultLists.add(resultGroup);
                        Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
                    } else {
                        ResultGroup r = resultLists.get(topK - 1);
                        if (r.score < resultGroup.score) {
                            updated = true;
                            resultLists.remove(topK - 1);
                            resultLists.add(resultGroup);
                            Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
                        }

                    }

                }

                vRestSet.popMember(index);
                totalCalled++;
                RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                        vRestSet.getVisited(), vRestSet.maxMember);
                newRestSet.markAllUnVisited();

                IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                        vIntermediateSet.getMaxFriend(),
                        vIntermediateSet.getTotalDistance(),
                        vIntermediateSet.getTotalConnectivity());

                if (newRestSet.getSize() != 0) {
                    queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                            assistantClass.initialMaxDegree, false,
                            foundRestSet));

                }

                if (comb) {
                    vIntermediateSet.popMember(m, dMin);

                    if (vRestSet.getSize() != 0) {
                        queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                assistantClass.initialMaxDegree, false,
                                foundRestSet));

                    }

                }

                break;

            }

        }

        return true;
    }

    private static void generateRankListApproxIncremental
            (PriorityQueue<AssistantClass> queue, ArrayList<Rectangle> rects, MeetingPointSet mpSet,
             SpatialIndex siList, boolean comb) {
        while (queue.size() != 0) {
            AssistantClass assistantClass = queue.remove();
            Integer mpID = assistantClass.meetingPointId;
            IntermediateSet vIntermediateSet = assistantClass.intermediateSet;
            RestSet vRestSet = assistantClass.restSet;
            RestSet foundRestSet = assistantClass.foundRestSet;

            //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//            if (vIntermediateSet.getSize() + vRestSet.getSize() < minimumMember || vRestSet.getSize() == 0) {
//                continue;
//            }
            if (vIntermediateSet.getSize() >= maxGroupSize) {
//                System.out.println("??");
                continue;

            }

//            System.out.println(queue.size());
            Score s = new Score(vIntermediateSet);
            boolean resultUpdated = false;

            Integer m = null;
            float dMin = 100;
            int index;
//            vIntermediateSet.showMemberList();
//            vRestSet.showMemberList();

            int size = vRestSet.getSize() + vIntermediateSet.getSize();
//        System.out.println(size);
//        System.out.println(vRestSet.isEmpty());
            //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//            while (size >= minimumMember && !vRestSet.isEmpty()) {
            while (vIntermediateSet.getSize() < maxGroupSize && !vRestSet.isEmpty()) {

                if (vRestSet.foundUnvisitedVertex()) {
                    m = vRestSet.topMember();
                    index = vRestSet.getLocalMemberIndex(m);
                    vRestSet.markVisited(index);
                    dMin = mpSet.getList().get(mpID).getMemberDistance(m);
//                System.out.println("member id: " + m + "  distance : " + dMin + "           index:    " + index);

                } else {
//                    System.out.println("including new member in restset");
//System.out.println("ekhane ki ase kokhono ?");
                    boolean found = false;
                    if (vRestSet.maxMember < foundRestSet.getSize()) {
                        for (int q = vRestSet.maxMember; q < foundRestSet.getSize(); q++) {
                            vRestSet.addMember(foundRestSet.getMemberList().get(q));
                            found = true;

                        }
                    }
                    if (found) {
                        continue;
                    }
//                    System.out.println("agei hoi ni");
//                System.out.println("before");
//                System.out.println(vIntermediateSet.getSize());
//                    System.out.println(vRestSet.getSize());
//                    System.out.println(foundRestSet.getSize());
                    found = retrieveNextMember(vRestSet, foundRestSet, mpSet.getList().get(mpID), memberSet, rects, siList);
//                System.out.println(vRestSet.getSize());
                    if (!found) {
//                    System.out.println("################### no member available");
                        break;
                    }
                    continue;
                }

                if (resultLists.size() == topK) {
                    ResultGroup r = resultLists.get(topK - 1);
                    int ttlGroupSize = vIntermediateSet.getGroupSize()
                            + vRestSet.getUnvisitedMemberCount() + 1;
                    int sz = vIntermediateSet.getSize();
                    boolean adTerminate = true;
                    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    for (int i = minimumMember; i <= maxGroupSize; i++) {

//                    for (int i = minimumMember; i <= ttlGroupSize; i++) {
                        if (sz >= i) {
                            continue;
                        }
                        double pruningDistance = s.advancePruningDistance(r.getTotalConnectivity(),
                                r.getGroupSize(), r.getTotalDistance(), assistantClass.initialMaxDegree,
                                i);
//                        System.out.println(pruningDistance);
//                        System.out.println(dMin * (i - vIntermediateSet.getSize()));

                        if (pruningDistance > (double) dMin * (i - vIntermediateSet.getSize())) {
//                        System.out.println("\n\n\n\ntermination advance");
//                        System.out.println((ttlGroupSize - vIntermediateSet.getSize()));
//                        //   System.out.println(m.getMemberId());
//                        System.out.println(pruningDistance);
//                        System.out.println((double) dMin * (ttlGroupSize - vIntermediateSet.getSize()));

                            adTerminate = false;
                            break;
                        }
                    }
                    if (adTerminate) {
//                    adP++;

//                        System.out.println("early termination for member:" + m);
                        break;

                    }

                }

                if (vIntermediateSet.getSize() < minimumMember) {
//                    System.out.print(m + " con " + dMin + "  ");
//                    vIntermediateSet.showMemberList();
//                    System.out.println(assistantClass.updated);
//                    System.out.println(mpID);

                    double newCon = vIntermediateSet.connectionIfAdded(m);

//                    double rightSide = minimumConstraint - minimumMember + 1 + vIntermediateSet.getSize();
                    double rightSide = (double) (minimumConstraint * vIntermediateSet.getSize()) / (minimumMember - 1.0);
//
////                    double rightSide = Math.floor((double) (minimumConstraint * vIntermediateSet.getSize()) / (minimumMember - 1.0));
                    if (newCon >= rightSide) {
                        vIntermediateSet.addMember(m, dMin);
//                    System.out.println("before");
//                    vIntermediateSet.showMemberList();
                        boolean updated = false;
                        if (vIntermediateSet.getSize() == minimumMember) {
                            ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getSize(),
                                    vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                            int minCon = resultGroup.getMinDegree();
                            if (minCon < minimumConstraint) {
                                vIntermediateSet.popMember(m, dMin);

//                            System.out.println("\n\n\n\n\n ekhane\n\n\n\n");
                                //    resultGroup.getMaxDegree();
//                                System.out.println("famili bad member " + m);
                                notFeas++;
                                continue;
                            }
                            if (groupFound < topK) {
                                groupFound++;
                                updated = true;
                                resultLists.add(resultGroup);
                                Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                            resultGroup.getMaxDegree();

//                            resultGroup.showConnection();
                            } else {
                                ResultGroup r = resultLists.get(topK - 1);
                                if (r.score < resultGroup.score) {
                                    updated = true;
                                    resultLists.remove(topK - 1);
                                    resultLists.add(resultGroup);
                                    Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                                resultGroup.getMaxDegree();
//                                resultGroup.showConnection();
                                }

                            }

                        }
                        vRestSet.popMember(index);
                        totalCalled++;

                        if (vRestSet.getSize() == 0) {
//                            System.out.println("including new member in restset");
                            boolean found = false;
                            if (vRestSet.maxMember < foundRestSet.getSize()) {
//                                System.out.println("ekhane");
                                for (int q = vRestSet.maxMember; q < foundRestSet.getSize(); q++) {
                                    vRestSet.addMember(foundRestSet.getMemberList().get(q));
                                    found = true;

                                }
                            }
                            if (!found) {
//                                System.out.println("notun");
                                retrieveNextMember(vRestSet, foundRestSet, mpSet.getList().get(mpID), memberSet, rects, siList);
                            }
                        }

                        RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                vRestSet.getVisited(), vRestSet.maxMember);
                        newRestSet.markAllUnVisited();

                        IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                vIntermediateSet.getMaxFriend(),
                                vIntermediateSet.getTotalDistance(),
                                vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                        System.out.println(updated);
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                        if (newRestSet.getSize() != 0) {
//                            System.out.println(updated);
                            queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                    assistantClass.initialMaxDegree, updated,
                                    foundRestSet));

                        }

                        if (comb) {
                            vIntermediateSet.popMember(m, dMin);

                            if (vRestSet.getSize() != 0) {
                                queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                        assistantClass.initialMaxDegree, false,
                                        foundRestSet));

                            }

                        }

                        break;
                    } else {
//                    System.out.println("bad "+m);
                    }
                } else {

//FOR PREVIOUS GROUP
//                System.out.println("over");
                    int newCon = vIntermediateSet.connectionIfAdded(m);
//                System.out.println(newCon);
//                System.out.println(minimumConstraint);
//                    System.out.println("hmmmmmmmmmmmmmmmmmmmmmmmm");
//                    System.out.print(m + " con " + dMin + "  ");
//                    vIntermediateSet.showMemberList();
//                    System.out.println(assistantClass.updated);
//                    System.out.println(mpID);

                    if (newCon >= minimumConstraint) {

                        //NO CHECKING FOR TERMINATION......
                        if (groupFound < topK) {
//                        System.out.println("\nmember kom\n");
                            vIntermediateSet.addMember(m, dMin);
//                            System.out.println("before");
//                            vIntermediateSet.showMemberList();

                            ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getSize(),
                                    vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);

                            groupFound++;
                            resultLists.add(resultGroup);
//                            System.out.println("added");
                            Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                        resultGroup.getMaxDegree();
                            vRestSet.popMember(index);
                            totalCalled++;
                            if (vRestSet.getSize() == 0) {
//                            System.out.println("including new member in restset");
                                boolean found = false;
                                if (vRestSet.maxMember < foundRestSet.getSize()) {
//                                System.out.println("ekhane");
                                    for (int q = vRestSet.maxMember; q < foundRestSet.getSize(); q++) {
                                        vRestSet.addMember(foundRestSet.getMemberList().get(q));
                                        found = true;

                                    }
                                }
                                if (!found) {
//                                System.out.println("notun");
                                    retrieveNextMember(vRestSet, foundRestSet, mpSet.getList().get(mpID), memberSet, rects, siList);
                                }
                            }

                            RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                    vRestSet.getVisited(), vRestSet.maxMember);
                            newRestSet.markAllUnVisited();

                            IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getMaxFriend(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                            if (newRestSet.getSize() != 0) {
                                queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                        assistantClass.initialMaxDegree, true,
                                        foundRestSet));

                            }

                            if (comb) {
                                vIntermediateSet.popMember(m, dMin);

                                if (vRestSet.getSize() != 0) {
                                    queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                            assistantClass.initialMaxDegree, false,
                                            foundRestSet));

                                }

                            }

                            break;
//                            continue;
                        }

                        if (assistantClass.updated) {
//                        System.out.println("\n baap included hoise\n");
                            vIntermediateSet.addMember(m, dMin);
//                            System.out.println("before");
//                            vIntermediateSet.showMemberList();

                            ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getSize(),
                                    vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                            ResultGroup r = resultLists.get(topK - 1);
                            boolean updated = false;
                            if (r.score < resultGroup.score) {
//                                System.out.println("added");
                                resultLists.remove(topK - 1);
                                updated = true;
                                resultLists.add(resultGroup);
                                Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                            resultGroup.getMaxDegree();
                            } else {
//                                System.out.println("not added");
                            }
                            vRestSet.popMember(index);
                            totalCalled++;

                            if (vRestSet.getSize() == 0) {
//                            System.out.println("including new member in restset");
                                boolean found = false;
                                if (vRestSet.maxMember < foundRestSet.getSize()) {
//                                System.out.println("ekhane");
                                    for (int q = vRestSet.maxMember; q < foundRestSet.getSize(); q++) {
                                        vRestSet.addMember(foundRestSet.getMemberList().get(q));
                                        found = true;

                                    }
                                }
                                if (!found) {
//                                System.out.println("notun");
                                    retrieveNextMember(vRestSet, foundRestSet, mpSet.getList().get(mpID), memberSet, rects, siList);
                                }
                            }

                            RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                    vRestSet.getVisited(), vRestSet.maxMember);
                            newRestSet.markAllUnVisited();

                            IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getMaxFriend(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                            if (newRestSet.getSize() != 0) {
                                queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                        assistantClass.initialMaxDegree, updated,
                                        foundRestSet));

                            }

                            if (comb) {
                                vIntermediateSet.popMember(m, dMin);

                                if (vRestSet.getSize() != 0) {
                                    queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                            assistantClass.initialMaxDegree, false,
                                            foundRestSet));

                                }

                            }

                            break;
                        } else {

//                            System.out.println("\n baap included hoini **************\n");
                            double terminatingDistance = s.getDistanceTermination(assistantClass.initialMaxDegree);
//                        System.out.println("terminating distance: " + terminatingDistance);

                            if (dMin > terminatingDistance) {
//                                System.out.println("termination due to distance upper bound");
//                            adP++;

                                break;
                            }
//                        System.out.println("upper distance for member: " + s.getUpperDistanceForMember());
                            if (dMin < s.getUpperDistanceForMember()) {
//                                System.out.println("distance lemma");

                                vIntermediateSet.addMember(m, dMin);
//                                System.out.println("before");
//                                vIntermediateSet.showMemberList();

                                ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                        vIntermediateSet.getTotalDistance(),
                                        vIntermediateSet.getSize(),
                                        vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                                ResultGroup r = resultLists.get(topK - 1);
                                boolean updated = false;
                                if (r.score < resultGroup.score) {
                                    resultLists.remove(topK - 1);
                                    updated = true;
                                    resultLists.add(resultGroup);
//                                    System.out.println("added");
                                    Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                                resultGroup.getMaxDegree();
                                } else {
//                                    System.out.println("not added");
                                }
                                vRestSet.popMember(index);
                                totalCalled++;

                                if (vRestSet.getSize() == 0) {
//                            System.out.println("including new member in restset");
                                    boolean found = false;
                                    if (vRestSet.maxMember < foundRestSet.getSize()) {
//                                System.out.println("ekhane");
                                        for (int q = vRestSet.maxMember; q < foundRestSet.getSize(); q++) {
                                            vRestSet.addMember(foundRestSet.getMemberList().get(q));
                                            found = true;

                                        }
                                    }
                                    if (!found) {
//                                System.out.println("notun");
                                        retrieveNextMember(vRestSet, foundRestSet, mpSet.getList().get(mpID), memberSet, rects, siList);
                                    }
                                }

                                RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                        vRestSet.getVisited(), vRestSet.maxMember);
                                newRestSet.markAllUnVisited();

                                IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                        vIntermediateSet.getMaxFriend(),
                                        vIntermediateSet.getTotalDistance(),
                                        vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                                if (newRestSet.getSize() != 0) {
                                    queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                            assistantClass.initialMaxDegree, updated,
                                            foundRestSet));

                                }

                                if (comb) {
                                    vIntermediateSet.popMember(m, dMin);

                                    if (vRestSet.getSize() != 0) {
                                        queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                                assistantClass.initialMaxDegree, false,
                                                foundRestSet));

                                    }

                                }

                                break;
//                                continue;
                            }
                            double conLower = s.getLowerBoundOnConnection(dMin);

                            if ((float) 2 * newCon >= conLower) {
//                                System.out.println("connection Lemma");
//                            totalCalled++;

                                vIntermediateSet.addMember(m, dMin);
//                                System.out.println("before");
//                                vIntermediateSet.showMemberList();

                                ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                        vIntermediateSet.getTotalDistance(),
                                        vIntermediateSet.getSize(),
                                        vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                                ResultGroup r = resultLists.get(topK - 1);
                                boolean updated = false;
                                if (r.score < resultGroup.score) {
                                    resultLists.remove(topK - 1);
                                    updated = true;
                                    resultLists.add(resultGroup);
                                    Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                                    System.out.println("added");
                                    //                  resultGroup.getMaxDegree();
                                } else {
//                                    System.out.println("not added");
                                }
                                vRestSet.popMember(index);
                                totalCalled++;

                                if (vRestSet.getSize() == 0) {
//                            System.out.println("including new member in restset");
                                    boolean found = false;
                                    if (vRestSet.maxMember < foundRestSet.getSize()) {
//                                System.out.println("ekhane");
                                        for (int q = vRestSet.maxMember; q < foundRestSet.getSize(); q++) {
                                            vRestSet.addMember(foundRestSet.getMemberList().get(q));
                                            found = true;

                                        }
                                    }
                                    if (!found) {
//                                System.out.println("notun");
                                        retrieveNextMember(vRestSet, foundRestSet, mpSet.getList().get(mpID), memberSet, rects, siList);
                                    }
                                }

                                RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                        vRestSet.getVisited(), vRestSet.maxMember);
                                newRestSet.markAllUnVisited();

                                IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                        vIntermediateSet.getMaxFriend(),
                                        vIntermediateSet.getTotalDistance(),
                                        vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                                if (newRestSet.getSize() != 0) {
                                    queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                            assistantClass.initialMaxDegree, updated,
                                            foundRestSet));

                                }

                                if (comb) {
                                    vIntermediateSet.popMember(m, dMin);

                                    if (vRestSet.getSize() != 0) {
                                        queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                                assistantClass.initialMaxDegree, false,
                                                foundRestSet));

                                    }

                                }

                                break;
                            } else {

//                                System.out.println("ekhane");
                            }
                        }

                    } else {
//                        System.out.println("familily bad member " + m);

                    }

                }
            }
            assistantClass = null;
        }

    }

    private static void generateRankListIncremental
            (PriorityQueue<AssistantClass> queue, ArrayList<Rectangle> rects, MeetingPointSet mpSet,
             SpatialIndex siList, boolean comb) {

        while (queue.size() != 0) {
//            System.out.println(queue.size());
            AssistantClass assistantClass = queue.remove();
            Integer mpID = assistantClass.meetingPointId;
            IntermediateSet vIntermediateSet = assistantClass.intermediateSet;
            RestSet vRestSet = assistantClass.restSet;
            RestSet foundRestSet = assistantClass.foundRestSet;
            if (vIntermediateSet.getSize() >= maxGroupSize) {
//                System.out.println("??");
                continue;

            }
//            System.out.println("meeting point " + mp.id);
            Score s = new Score(vIntermediateSet);
            boolean resultUpdated = false;

            Integer m = null;
            float dMin = 100;
            int index;
//            vIntermediateSet.showMemberList();
//            vRestSet.showMemberList();

            while (vIntermediateSet.getSize() < maxGroupSize) {
//                System.out.println("hm");

                if (vRestSet.foundUnvisitedVertex()) {
                    m = vRestSet.topMember();
                    index = vRestSet.getLocalMemberIndex(m);
                    vRestSet.markVisited(index);
                    dMin = mpSet.getList().get(mpID).getMemberDistance(m);
//                    System.out.println("member id: " + m.getMemberId() + "  distance : " + dMin + "           index:    " + index);

                } else {
//                    System.out.println("including new member in restset");
//System.out.println("ekhane ki ase kokhono ?");
                    boolean found = false;
                    if (vRestSet.maxMember < foundRestSet.getSize()) {
                        for (int q = vRestSet.maxMember; q < foundRestSet.getSize(); q++) {
                            vRestSet.addMember(foundRestSet.getMemberList().get(q));
//                              vRest.addMember(memberSet.getList().get(i).getMemberId());
//
                            found = true;

                        }
                    }
                    if (found) {
                        continue;
                    }
//                    System.out.println("agei hoi ni");
//                System.out.println("before");
//                System.out.println(vIntermediateSet.getSize());
//                    System.out.println(vRestSet.getSize());
//                    System.out.println(foundRestSet.getSize());
                    found = retrieveNextMember(vRestSet, foundRestSet, mpSet.getList().get(mpID), memberSet, rects, siList);
//                System.out.println(vRestSet.getSize());
                    if (!found) {
//                    System.out.println("################### no member available");
                        break;
                    }
                    continue;
                }

                if (resultLists.size() == topK) {
                    ResultGroup r = resultLists.get(topK - 1);
                    int ttlGroupSize = vIntermediateSet.getGroupSize()
                            + vRestSet.getUnvisitedMemberCount() + 1;
                    int sz = vIntermediateSet.getSize();
                    boolean adTerminate = true;
                    for (int i = minimumMember; i <= maxGroupSize; i++) {
                        if (sz >= i) {
                            continue;
                        }
                        double pruningDistance = s.advancePruningDistance(r.getTotalConnectivity(),
                                r.getGroupSize(), r.getTotalDistance(), -1,
                                i);
//                         double pruningDistance = s.advancePruningDistance(r.getTotalConnectivity(),
//                                r.getGroupSize(), r.getTotalDistance(), assistantClass.initialMaxDegree,
//                                i);
//                        System.out.println(pruningDistance);
//                        System.out.println(dMin * (i - vIntermediateSet.getSize()));

                        if (pruningDistance > (double) dMin * (i - vIntermediateSet.getSize())) {
//                        System.out.println("\n\n\n\ntermination advance");
//                        System.out.println((ttlGroupSize - vIntermediateSet.getSize()));
//                        //   System.out.println(m.getMemberId());
//                        System.out.println(pruningDistance);
//                        System.out.println((double) dMin * (ttlGroupSize - vIntermediateSet.getSize()));

                            adTerminate = false;
                            break;
                        }
                    }
                    if (adTerminate) {
//                    adP++;

//                        System.out.println("$$$$$$$$$$$$$$$$$early termination for member:" + m.getMemberId());
                        break;

                    }

                }

                if (vIntermediateSet.getSize() < minimumMember) {

//                double newCon=vIntermediateSet.connectionIfAdded(m);
                    double newCon = minimumConstraint - vIntermediateSet.connectionIfAdded(m);
//                double rightSide = (double) (minimumConstraint * vIntermediateSet.getSize()) / (minimumMember - 1.0);

//                double rightSide = Math.floor((double) (minimumConstraint * vIntermediateSet.getSize()) / (minimumMember - 1.0));
                    double rightSide = (minimumMember - 1 - vIntermediateSet.getSize());
                    if (newCon <= rightSide) {
//                        System.out.println("dgukse");

                        vIntermediateSet.addMember(m, dMin);
                        boolean updated = false;
                        if (vIntermediateSet.getSize() == minimumMember) {
                            ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getSize(),
                                    vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                            int minCon = resultGroup.getMinDegree();
                            if (minCon < minimumConstraint) {
                                vIntermediateSet.popMember(m, dMin);

//                            System.out.println("group formed but not satisfied group:");
//                            //    resultGroup.getMaxDegree();
//                                System.out.println("feasible not found for member: " + m.getMemberId());
                                notFeas++;
                                continue;
                            }
                            if (groupFound < topK) {
                                groupFound++;
                                updated = true;
                                resultLists.add(resultGroup);
                                Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                            resultGroup.getMaxDegree();

//                            resultGroup.showConnection();
                            } else {
                                ResultGroup r = resultLists.get(topK - 1);
                                if (r.score < resultGroup.score) {
                                    updated = true;
                                    resultLists.remove(topK - 1);
                                    resultLists.add(resultGroup);
                                    Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                                resultGroup.getMaxDegree();
//                                resultGroup.showConnection();
                                }

                            }

                        }
                        vRestSet.popMember(index);
                        totalCalled++;

                        if (vRestSet.getSize() == 0) {
//                            System.out.println("including new member in restset");
                            boolean found = false;
                            if (vRestSet.maxMember < foundRestSet.getSize()) {
//                                System.out.println("ekhane");
                                for (int q = vRestSet.maxMember; q < foundRestSet.getSize(); q++) {
                                    vRestSet.addMember(foundRestSet.getMemberList().get(q));
                                    found = true;

                                }
                            }
                            if (!found) {
//                                System.out.println("notun");
                                retrieveNextMember(vRestSet, foundRestSet, mpSet.getList().get(mpID), memberSet, rects, siList);
                            }
                        }

                        RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                vRestSet.getVisited(), vRestSet.maxMember);
                        newRestSet.markAllUnVisited();

                        IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                vIntermediateSet.getMaxFriend(),
                                vIntermediateSet.getTotalDistance(),
                                vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                        if (newRestSet.getSize() != 0) {
                            queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                    assistantClass.initialMaxDegree, false,
                                    foundRestSet));

                        }

                        if (comb) {
                            vIntermediateSet.popMember(m, dMin);

                            if (vRestSet.getSize() != 0) {
                                queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                        assistantClass.initialMaxDegree, false,
                                        foundRestSet));

                            }

                        }

//                        System.out.println("after");
//                        vIntermediateSet.showMemberList();
                        break;

                    } else {
//                        System.out.println("familily bad member " + m.getMemberId());

//                    System.out.println("\n bad for familiarity constraint\n ");
//                    System.out.println(newCon);
//                    System.out.println(minimumConstraint);
//                    System.out.println(minimumMember - 1);
//                    System.out.println(rightSide);
                        adP++;
                    }
                } else {

//                System.out.println("yeap");
                    int newCon = vIntermediateSet.connectionIfAdded(m);

                    if (newCon >= minimumConstraint) {
//                    System.out.println("no");
                        if (groupFound < topK) {
                            vIntermediateSet.addMember(m, dMin);

//                        System.out.println("group sob ase ni");
//                            System.out.println("before");
//                            vIntermediateSet.showMemberList();
                            ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getSize(),
                                    vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);

                            groupFound++;
                            resultLists.add(resultGroup);
                            Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                        resultGroup.getMaxDegree();

                            vRestSet.popMember(index);
                            totalCalled++;

                            if (vRestSet.getSize() == 0) {
//                            System.out.println("including new member in restset");
                                boolean found = false;
                                if (vRestSet.maxMember < foundRestSet.getSize()) {
//                                System.out.println("ekhane");
                                    for (int q = vRestSet.maxMember; q < foundRestSet.getSize(); q++) {
                                        vRestSet.addMember(foundRestSet.getMemberList().get(q));
                                        found = true;

                                    }
                                }
                                if (!found) {
//                                System.out.println("notun");
                                    retrieveNextMember(vRestSet, foundRestSet, mpSet.getList().get(mpID), memberSet, rects, siList);
                                }
                            }

                            RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                    vRestSet.getVisited(), vRestSet.maxMember);
                            newRestSet.markAllUnVisited();

                            IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getMaxFriend(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                            if (newRestSet.getSize() != 0) {
                                queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                        assistantClass.initialMaxDegree, false,
                                        foundRestSet));

                            }

                            if (comb) {
                                vIntermediateSet.popMember(m, dMin);

                                if (vRestSet.getSize() != 0) {
                                    queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                            assistantClass.initialMaxDegree, false,
                                            foundRestSet));

                                }

                            }

//                            System.out.println("after");
//                            vIntermediateSet.showMemberList();
                            break;
                        }

                        vIntermediateSet.addMember(m, dMin);
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();

                        ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                vIntermediateSet.getTotalDistance(),
                                vIntermediateSet.getSize(),
                                vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                        ResultGroup r = resultLists.get(topK - 1);
                        boolean updated = false;
                        if (r.score < resultGroup.score) {
                            resultLists.remove(topK - 1);
                            updated = true;
                            resultLists.add(resultGroup);
                            Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                            resultGroup.getMaxDegree();
                        }
                        vRestSet.popMember(index);
                        totalCalled++;

                        if (vRestSet.getSize() == 0) {
//                            System.out.println("including new member in restset");
                            boolean found = false;
                            if (vRestSet.maxMember < foundRestSet.getSize()) {
//                                System.out.println("ekhane");
                                for (int q = vRestSet.maxMember; q < foundRestSet.getSize(); q++) {
                                    vRestSet.addMember(foundRestSet.getMemberList().get(q));
                                    found = true;

                                }
                            }
                            if (!found) {
//                                System.out.println("notun");
                                retrieveNextMember(vRestSet, foundRestSet, mpSet.getList().get(mpID), memberSet, rects, siList);
                            }
                        }

                        RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                vRestSet.getVisited(), vRestSet.maxMember);
                        newRestSet.markAllUnVisited();

                        IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                vIntermediateSet.getMaxFriend(),
                                vIntermediateSet.getTotalDistance(),
                                vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                        if (newRestSet.getSize() != 0) {
                            queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                    assistantClass.initialMaxDegree, false,
                                    foundRestSet));

                        }

                        if (comb) {
                            vIntermediateSet.popMember(m, dMin);

                            if (vRestSet.getSize() != 0) {
                                queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                        assistantClass.initialMaxDegree, false,
                                        foundRestSet));

                            }

                        }
//                        System.out.println("after");
//                        vIntermediateSet.showMemberList();
                        break;

                    } else {
//                        System.out.println("familily bad member " + m.getMemberId());

                    }

                }
            }

//            assistantClass = null;
        }

    }

}